//
//  CancelOrderViewController.swift
//  SWADESH
//
//  Created by Bhasker on 01/02/21.
//

import UIKit

class CancelOrderViewController: UIViewController {
    
    //MARK:-  IB Outlets
    
    @IBOutlet weak var cancelOrderOptionsTblVw: UITableView!
    @IBOutlet weak var cancelOrder: UIButton!
    @IBOutlet weak var reasonTF: UITextView!
    @IBOutlet weak var refundDeliverylbl: UILabel!
    
    //Model
    var cancelItemListModel: CancelItemListModel?
    var cancelorderModel:CancelorderModel?
    
    var categoryNamesArr = [String]()
    var categoryImgArr = [String]()
    var isCheckBoxSelected: Int?
    
    var orderID = String()
    
    //MARK:-  View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.orderCancel)
        
        tableViewNibs()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userCancelList()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        reasonTF.layer.cornerRadius = 5
        reasonTF.layer.borderWidth = 1
        reasonTF.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
    }
    
    func tableViewNibs(){
        
        cancelOrderOptionsTblVw.estimatedRowHeight = cancelOrderOptionsTblVw.rowHeight
        cancelOrderOptionsTblVw.rowHeight = UITableView.automaticDimension
        cancelOrderOptionsTblVw.separatorStyle = .none
        
        self.cancelOrderOptionsTblVw.delegate = self
        self.cancelOrderOptionsTblVw.dataSource = self
        
        cancelOrderOptionsTblVw.register(UINib(nibName: CancelOrderTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CancelOrderTableViewCell.identifier)
        
    }
    
    //MARK:-  IB Actions
    
    @IBAction func onClickCancelOrderBtn(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
            
            if isCheckBoxSelected != nil {
                print(cancelItemListModel?.data?[isCheckBoxSelected!].id, reasonTF.text, "cancel order selected id")
                
                let selectedID  = cancelItemListModel?.data?[isCheckBoxSelected!].id ?? ""
                let enteredText = reasonTF.text ?? ""
                
                if cancelItemListModel?.data?[isCheckBoxSelected!].reason == "Others" {
                    
                    if !reasonTF.text.isEmpty {
                        print("success textview data")
                        userOrderCancellation(reasonID: selectedID, reasonText: enteredText)
                        
                    }else{
                        showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.cancelReasonText)
                    }
                }else{
                    print("Success check box selected")
                    userOrderCancellation(reasonID: selectedID, reasonText: enteredText)
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.cancelReasonSelection)
            }
            
        }else {
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    //MARK:-  API
    //cancel Static list
    func userCancelList() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: orderCancelList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(CancelItemListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.cancelItemListModel = responseJson
                DispatchQueue.main.async {
                    self.cancelOrderOptionsTblVw.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Order Cancellation
    func userOrderCancellation(reasonID:String, reasonText: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.order_id: orderID,
            APIParameters.reason_id: reasonID,
            APIParameters.reason_text: reasonText
        ]
        
        print(params, "cancel order parans")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: orderCancel_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(CancelorderModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.navigationController?.backToViewController(vc: MyOrdersVC.self)
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
   
}

//MARK:-  Extension tableview
extension CancelOrderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cancelItemListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = cancelOrderOptionsTblVw.dequeueReusableCell(withIdentifier: CancelOrderTableViewCell.identifier, for: indexPath) as? CancelOrderTableViewCell else{return UITableViewCell()}
        cell.selectionStyle = .none
        
        cell.cancelOrderOptionLbl.text = cancelItemListModel?.data?[indexPath.row].reason
        cell.selectOptionBtn.tag = indexPath.row
        cell.selectOptionBtn.addTarget(self, action: #selector(onClickOptionsBtn), for: .touchUpInside)
        
        //bottom label
        self.refundDeliverylbl.text = cancelItemListModel?.data?[0].text
        
        if isCheckBoxSelected == indexPath.row {
            cell.optionSelcteImgVw.image = UIImage(named: "check_roundRadio_blue")
        }else{
            cell.optionSelcteImgVw.image = UIImage(named: "unCheck_roundRadio_blue")
        }
        
        return cell
    }
    
    @objc func onClickOptionsBtn(_ sender: UIButton) {
        
        isCheckBoxSelected = sender.tag
        
        DispatchQueue.main.async {
            self.cancelOrderOptionsTblVw.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
