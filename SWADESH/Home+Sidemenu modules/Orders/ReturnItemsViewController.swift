//
//  ReturnItemsViewController.swift
//  SWADESH
//
//  Created by Bhasker on 03/02/21.
//

import UIKit

class ReturnItemsViewController: UIViewController {

    @IBOutlet weak var retutnItemsTblVw: UITableView!
    @IBOutlet weak var returnOrderItemBtn: UIButton!
    
    var particularProductDetail: [ParticularProductDetail]?
    
    var selectedIndex = [Int]()
    var selectedID = [String]()
    var orderID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.orderreturn)
        tableViewNibs()
        retutnItemsTblVw.estimatedRowHeight = retutnItemsTblVw.rowHeight
        retutnItemsTblVw.rowHeight = UITableView.automaticDimension
        retutnItemsTblVw.separatorStyle = .none

        self.retutnItemsTblVw.delegate = self
        self.retutnItemsTblVw.dataSource = self
        self.retutnItemsTblVw.reloadData()
        
        // Do any additional setup after loading the view.
    }

    func tableViewNibs(){
        
        retutnItemsTblVw.register(UINib(nibName: ReturnItemTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ReturnItemTableViewCell.identifier)
    }

    @IBAction func onClickReturnItemBtn(_ sender: UIButton) {
        
        print(selectedID, "ID")
        
        if selectedIndex.count > 0 {
            if selectedIndex.count < 3 {
                let vc = storyboard?.instantiateViewController(identifier: "ReturnItemFormViewController") as!  ReturnItemFormViewController
                vc.orderID = orderID
                vc.selectedID = selectedID
                navigationController?.pushViewController(vc, animated: true)
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: "You can only return 2 items")
            }
        }else{
            showAlertMessage(vc: self, titleStr: "", messageStr: "Select items to return")
        }
        
    }
}

//MARK:-  Extension Tableview
extension ReturnItemsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return particularProductDetail?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = retutnItemsTblVw.dequeueReusableCell(withIdentifier: ReturnItemTableViewCell.identifier) as! ReturnItemTableViewCell
        
        cell.productImage.sd_setImage(with: URL(string: particularProductDetail?[indexPath.row].image ?? ""), placeholderImage: UIImage(named: ""))
        cell.productNameLbl.text = particularProductDetail?[indexPath.row].name ?? ""
        cell.quantityLbl.text = "Quantity - \(particularProductDetail?[indexPath.row].quantity ?? "")"
        cell.productWeightLbl.text = particularProductDetail?[indexPath.row].weight ?? ""
        cell.priceLbl.isHighlited(textConvert: particularProductDetail?[indexPath.row].price ?? "")
        cell.selectedProBtn.tag = indexPath.row
        cell.selectedProBtn.addTarget(self, action: #selector(onTapSelectingReturnProduct(_:)), for: .touchUpInside)
        
        cell.selectedProImage.image = #imageLiteral(resourceName: "uncheck_rect")
        
        if selectedIndex.contains(indexPath.row) {
            cell.selectedProImage.image = #imageLiteral(resourceName: "check_rect")
        }else{
            cell.selectedProImage.image = #imageLiteral(resourceName: "uncheck_rect")
        }
        
        return cell
    }
    
    @objc func onTapSelectingReturnProduct(_ sender: UIButton) {
        print(sender.tag, "selected index")
        
        if selectedIndex.contains(sender.tag) {
            selectedIndex = selectedIndex.filter{$0 != sender.tag}
            selectedID = selectedID.filter{!(particularProductDetail?[sender.tag].productID ?? "").contains($0)}
        }else{
            selectedIndex.append(sender.tag)
            selectedID.append(particularProductDetail?[sender.tag].productID ?? "")
        }
        retutnItemsTblVw.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
