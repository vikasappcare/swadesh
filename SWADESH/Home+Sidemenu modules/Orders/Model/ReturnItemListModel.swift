//
//  ReturnItemListModel.swift
//  SWADESH
//
//  Created by apple on 09/03/21.
//

import Foundation

struct ReturnItemListModel: Codable {
    let status: Int
    let message: String
    let data: [DataReturnList]?
}

// MARK: - Datum
struct DataReturnList: Codable {
    let id, reason, text, cd: String?
}
