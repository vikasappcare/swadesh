//
//  PastOrdersModel.swift
//  SWADESH
//
//  Created by apple on 10/03/21.
//

import Foundation

struct PastOrdersModel: Codable {
    let status: Int
    let message: String
    let data: [DataPastOrders]?
}

// MARK: - Datum
struct DataPastOrders: Codable {
    let orderID, paymentStatus, deliveryType, orderStatus: String?
    let orderAmount, deliveryCharge, deliveryDate, deliveryTime: String?
    let productids: String?
    let productDetails: [PastProductDetail]?

    enum CodingKeys: String, CodingKey {
        case orderID = "order_id"
        case paymentStatus = "payment_status"
        case deliveryType = "delivery_type"
        case orderStatus = "order_status"
        case orderAmount = "order_amount"
        case deliveryCharge = "delivery_charge"
        case deliveryDate = "delivery_date"
        case deliveryTime = "delivery_time"
        case productids
        case productDetails = "ProductDetails"
    }
}

// MARK: - ProductDetail
struct PastProductDetail: Codable {
    let productID, price, weight, quantity: String?
    let name: String?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case price, weight, quantity, name, image
    }
}
