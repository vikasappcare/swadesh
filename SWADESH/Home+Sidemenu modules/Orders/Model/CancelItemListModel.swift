//
//  CancelItemListModel.swift
//  SWADESH
//
//  Created by apple on 09/03/21.
//

import Foundation

struct CancelItemListModel: Codable {
    let status: Int
    let message: String
    let data: [DataCancelitem]?
}

// MARK: - Datum
struct DataCancelitem: Codable {
    let id, reason, text, cd: String?
}

//MARK:- Cancel Order
struct CancelorderModel: Codable {
    let status: Int
    let message: String
}

