//
//  RepeatorderModel.swift
//  SWADESH
//
//  Created by apple on 30/03/21.
//

import Foundation

struct RepeatorderModel: Codable {
    let status: Int
    let message: String
}
