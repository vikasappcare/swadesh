//
//  ReturnItemFormViewController.swift
//  SWADESH
//
//  Created by Bhasker on 03/02/21.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON

class ReturnItemFormViewController: UIViewController {
    
    @IBOutlet weak var returnsOptionTblVw: UITableView!
    @IBOutlet weak var reasonTF: IQTextView!
    @IBOutlet weak var uploadImgBtn: UIButton!
    @IBOutlet weak var returnOrderBtn: UIButton!
    @IBOutlet weak var uploadImgsCollVw: UICollectionView!
    @IBOutlet weak var chargesDeliveryLbl: UILabel!
    
    var uploadReturnImages = [UIImage]()
    var uploadReturnImagesData = [Data]()
    var isCheckBoxSelected: Int?
    
    var orderID = String()
    var selectedID = [String]()
    var convertedID = [Int]()
    //Models
    var returnItemListModel: ReturnItemListModel?
    
    //MARK:-  View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.orderreturn)
        
        nibsDetails()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        convertedID = selectedID.map{ Int($0)! }
        
        print(isCheckBoxSelected, convertedID)
        if Reachability.isConnectedToNetwork() {
            userReturnList()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        reasonTF.layer.cornerRadius = 5
        reasonTF.layer.borderWidth = 0.5
        reasonTF.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
    }
    
    //Table and collection view info
    func nibsDetails(){
        
        returnsOptionTblVw.estimatedRowHeight = returnsOptionTblVw.rowHeight
        returnsOptionTblVw.rowHeight = UITableView.automaticDimension
        returnsOptionTblVw.separatorStyle = .none
        
        self.returnsOptionTblVw.delegate = self
        self.returnsOptionTblVw.dataSource = self
        
        self.uploadImgsCollVw.delegate = self
        self.uploadImgsCollVw.dataSource = self
        
        returnsOptionTblVw.register(UINib(nibName: CancelOrderTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CancelOrderTableViewCell.identifier)
        uploadImgsCollVw.register(UINib(nibName: CategoriesCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: CategoriesCollectionViewCell.identifier)
        
    }
    
    //MARK:-  IB Actions
    
    @IBAction func onClickUploadImgBtn(_ sender: Any) {
        
        ImagePickerManager().pickImage(self) { (img) in
            
            self.uploadReturnImages.append(img)
            self.uploadImgsCollVw.reloadData()
        }
    }
    
    @IBAction func onClickReturnOrderBtn(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
            
            if isCheckBoxSelected != nil {
                if uploadReturnImages.count > 0 {
                    print(returnItemListModel?.data?[isCheckBoxSelected!].id, reasonTF.text.isEmpty, "reason selected id")
                    
                    let selectedID = returnItemListModel?.data?[isCheckBoxSelected!].id ?? ""
                    let enteredText = reasonTF.text ?? ""
                    
                    if returnItemListModel?.data?[isCheckBoxSelected!].reason == "Others" {
                        
                        if !reasonTF.text.isEmpty {
                            print("success textview data")
                            userItemsReturn(returnID: selectedID, returnText: enteredText)
                        }else{
                            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.returnReasonText)
                        }
                    }else{
                        print("Success check box selected")
                        userItemsReturn(returnID: selectedID, returnText: enteredText)
                    }
                }else{
                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.returnImagesUpload)
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.returnReasonSelection)
            }
            
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    //MARK:-  API
    func userReturnList() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: itemReturnList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(ReturnItemListModel.self, from: response) else{return}
            
            if responseJson.status == 200 {
                self.returnItemListModel = responseJson
                DispatchQueue.main.async {
                    self.returnsOptionTblVw.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //MARK:- Return an Item
    func userItemsReturn(returnID: String, returnText:String)  {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        for i in 0..<uploadReturnImages.count {
            let Profileimage = uploadReturnImages[i]
            let imgData = Profileimage.jpegData(compressionQuality: 0.5)!
            self.uploadReturnImagesData.append(imgData)
        }
        
        let url = itemReturn_URL
        let parameters: [String:Any] = [
            APIParameters.uID: uids,
            APIParameters.order_id: orderID,
            APIParameters.return_id: returnID,
            APIParameters.return_text: returnText,
            APIParameters.product_id:convertedID
        ]
        let headers: HTTPHeaders = [API.key:API.value,API.content:API.type]
        
        UploadDocumentImage(isUser: true, endUrl: url, imageData: uploadReturnImagesData, parameters: parameters,isLoader: true, title: "", description: "Loading...", vc: self, headers: headers)
        print("url",url)
        print("image",uploadReturnImages)
        print(parameters,"parameters")
        
    }
    
    func UploadDocumentImage(isUser:Bool, endUrl: String, imageData: [Data]?,parameters: [String : Any],isLoader: Bool, title: String, description:String, vc:UIViewController,headers:HTTPHeaders, onCompletion: ((_ isSuccess:Bool) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        if isLoader {
            Indicator.shared().showIndicator(withTitle: title, and: description, vc: vc)
        }
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
           
            for imagesData in self.uploadReturnImagesData {
                multipartFormData.append(imagesData, withName: "images[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
        },
        to: endUrl, method: .post,headers: headers )
        .responseJSON(completionHandler: { (response) in
            Indicator.shared().hideIndicator(vc: vc)
            if let err = response.error{
                Indicator.shared().hideIndicator(vc: vc)
                print(err)
                onError?(err)
                return
            }
            let json = response.data
            if (json != nil)
            {
                let jsonObject = JSON(json!)
                print(jsonObject)
                DispatchQueue.main.async {
                    showAlertMessage(vc: self, titleStr: "", messageStr:jsonObject["message"].stringValue)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.1) {
                        self.navigationController?.backToViewController(vc: MyOrdersVC.self)
                    }
                }
            }
        })
    }

    
    
}


//MARK:-  Extension Tableview
extension ReturnItemFormViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return returnItemListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = returnsOptionTblVw.dequeueReusableCell(withIdentifier: CancelOrderTableViewCell.identifier, for: indexPath) as? CancelOrderTableViewCell else{return UITableViewCell()}
        cell.selectionStyle = .none
        
        cell.cancelOrderOptionLbl.text = returnItemListModel?.data?[indexPath.row].reason ?? ""
        cell.selectOptionBtn.tag = indexPath.row
        cell.selectOptionBtn.addTarget(self, action: #selector(onClickOptionsBtn), for: .touchUpInside)
        //bottom charges lbl
        self.chargesDeliveryLbl.text = returnItemListModel?.data?[0].text ?? ""
        
        if isCheckBoxSelected == indexPath.row {
            cell.optionSelcteImgVw.image = UIImage(named: "check_roundRadio_blue")
        }else{
            cell.optionSelcteImgVw.image = UIImage(named: "unCheck_roundRadio_blue")
        }
        
        return cell
    }
    
    @objc func onClickOptionsBtn(_ sender: UIButton) {
        
        isCheckBoxSelected = sender.tag
        
        DispatchQueue.main.async {
            self.returnsOptionTblVw.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

//MARK:-  Extension CollectionView
extension ReturnItemFormViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return uploadReturnImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoriesCollectionViewCell.identifier, for: indexPath) as! CategoriesCollectionViewCell
        
        categoryCell.categoryImgVw.image = uploadReturnImages[indexPath.row]
        categoryCell.categoryNameLbl.text = ""
        return categoryCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width / 5, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
