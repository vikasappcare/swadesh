//
//  ItemsCVCell.swift
//  SWADESH
//
//  Created by Vikas on 17/01/21.
//

import UIKit

class ItemsCVCell: UICollectionViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    class var nib : UINib {
        return UINib(nibName: ItemsCVCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var itemsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        itemsImage.layer.cornerRadius = itemsImage.bounds.size.width / 2.0
        itemsImage.contentMode = .scaleAspectFill
//        itemsImage.layer.borderWidth = 2
//        itemsImage.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }

}
