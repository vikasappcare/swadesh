//
//  CancelOrderTableViewCell.swift
//  SWADESH
//
//  Created by Bhasker on 01/02/21.
//

import UIKit

class CancelOrderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var optionSelcteImgVw: UIImageView!
    
    @IBOutlet weak var selectOptionBtn: UIButton!
    
    @IBOutlet weak var cancelOrderOptionLbl: UILabel!
    
    class var identifier: String {
        return "\(self)"
    }
    
    class var nibName : String {
        return "\(self)"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
