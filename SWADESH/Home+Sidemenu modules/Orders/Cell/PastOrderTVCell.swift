//
//  PastOrderTVCell.swift
//  SWADESH
//
//  Created by apple on 14/04/21.
//

import UIKit

class PastOrderTVCell: UITableViewCell {
    
    class var identifier: String {
        return "\(self)"
    }
    
    class var nib: UINib {
        return UINib(nibName: PastOrderTVCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var orderIDLbl: UILabel!
    @IBOutlet weak var totalItemsDateLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderContainsLbl: UILabel!
    @IBOutlet weak var orderListLbl: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var repeatOrderBtn: UIButton!
    @IBOutlet weak var orderListCV: UICollectionView!
    
    var imageArray = [String]()
    
    var imageData = [UIImage(named: "hector-farahani"), UIImage(named: "alexandra-kikot"),UIImage(named: "roberta-sorge")]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    private func showCollectionView() {
        orderListCV.delegate = self
        orderListCV.dataSource = self
        orderListCV.backgroundColor = .clear
        orderListCV.register(ItemsCVCell.nib, forCellWithReuseIdentifier: ItemsCVCell.identifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func productImagesArray(_ images: [String]) {
        imageArray = images
        showCollectionView()
        print(imageArray, "cell data")
    }
    
}

extension PastOrderTVCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = orderListCV.dequeueReusableCell(withReuseIdentifier: ItemsCVCell.identifier, for: indexPath) as! ItemsCVCell
        
        cell.itemsImage.sd_setImage(with: URL(string: imageArray[indexPath.row]), placeholderImage: UIImage(systemName: "placeholder1"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return -10
    }
    
}
