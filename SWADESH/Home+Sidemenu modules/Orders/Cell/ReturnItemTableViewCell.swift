//
//  ReturnItemTableViewCell.swift
//  SWADESH
//
//  Created by Bhasker on 03/02/21.
//

import UIKit

class ReturnItemTableViewCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    class var nibName : String {
        return "\(self)"
    }
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var selectedProImage: UIImageView!
    @IBOutlet weak var selectedProBtn: UIButton!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productWeightView: UIView!
    @IBOutlet weak var productWeightLbl: UILabel!
    @IBOutlet weak var productDescriptionLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
