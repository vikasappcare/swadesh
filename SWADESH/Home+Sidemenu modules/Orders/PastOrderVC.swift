//
//  PastOrderVC.swift
//  SWADESH
//
//  Created by Vikas on 12/01/21.
//

import UIKit

class PastOrderVC: UIViewController {
    
    @IBOutlet weak var pastOrderTV: UITableView!
    
    //Model
    var pastOrdersModel: PastOrdersModel?
    var repeatorderModel: RepeatorderModel?
    
    var pastImageString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userPastOrders()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    func showTableView() {
        pastOrderTV.delegate = self
        pastOrderTV.dataSource = self
        
        pastOrderTV.estimatedRowHeight = pastOrderTV.rowHeight
        pastOrderTV.rowHeight = UITableView.automaticDimension
        
        pastOrderTV.register(PastOrderTVCell.nib, forCellReuseIdentifier: PastOrderTVCell.identifier)
    }
    
    
    //MARK:- API
    func userPastOrders() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.orderType: "Past"
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: viewOrders_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(PastOrdersModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.pastOrdersModel = responseJson
                DispatchQueue.main.async {
                    self.pastOrderTV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
            
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    func userRepeatOrder(orderID: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.order_id: orderID
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: repeatOrder_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(RepeatorderModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.repeatorderModel = responseJson
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                print("success")
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
}

extension PastOrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pastOrdersModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = pastOrderTV.dequeueReusableCell(withIdentifier: PastOrderTVCell.identifier) as! PastOrderTVCell
        
        cell.orderIDLbl.text = "Id: \(pastOrdersModel?.data?[indexPath.row].orderID ?? "")"
        cell.totalItemsDateLbl.text = "\(pastOrdersModel?.data?[indexPath.row].productDetails?.count ?? 0) Items | \(pastOrdersModel?.data?[indexPath.row].deliveryDate ?? "")"
        cell.orderStatusLbl.text = pastOrdersModel?.data?[indexPath.row].orderStatus ?? ""
        cell.orderListLbl.text = ""//pastOrdersModel?.data?[indexPath.row].productDetails?[indexPath.row].name ?? ""
        cell.totalPrice.text = String(format: "$%@ ", pastOrdersModel?.data?[indexPath.row].orderAmount ?? "")
        cell.repeatOrderBtn.tag = indexPath.row
        cell.repeatOrderBtn.addTarget(self, action: #selector(onTaprepeatOrder(_:)), for: .touchUpInside)
        
        let title = pastOrdersModel?.data?[indexPath.row].productDetails?.count ?? 0
        
        var name1 = String()
        for i in 0..<title {
            print(indexPath.row, i, title)
            print(pastOrdersModel?.data?[indexPath.row].productDetails?[i].name ?? "", "order list")
            
            let value = pastOrdersModel?.data?[indexPath.row].productDetails?[i].name ?? ""
            name1 += value + ","
            print(name1, "vikas")
            
            let imgValue = pastOrdersModel?.data?[indexPath.row].productDetails?[i].image ?? ""
            pastImageString += imgValue + ","
        }
        
        //cell.orderListCV.reloadData()
        let valueString = pastImageString.components(separatedBy: ",")
        print(valueString, "images")
        cell.productImagesArray(valueString)
        
        let name = name1.dropLast()
        cell.orderListLbl.text = String(name)
        name1 = ""
        return cell
    }
    
    @objc func onTaprepeatOrder(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            userRepeatOrder(orderID: pastOrdersModel?.data?[sender.tag].orderID ?? "")
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let vc = storyboard?.instantiateViewController(identifier: "OrderDetailsViewController") as!  OrderDetailsViewController
//        navigationController?.pushViewController(vc, animated: true)

    }
}
