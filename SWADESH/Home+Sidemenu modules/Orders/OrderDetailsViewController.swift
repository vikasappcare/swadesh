//
//  OrderDetailsViewController.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 28/01/21.
//

import UIKit

struct OrderStatus {
    let title: String
    let subtitle: String
}

class OrderDetailsViewController: UIViewController {
    
    @IBOutlet weak var itemsCollVw: UICollectionView!
    @IBOutlet weak var orderStatusTblVw: UITableView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var orderIDLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var totalItemsDateLbl: UILabel!
    @IBOutlet weak var timeSlotLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderStatusView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    
    //Model
    var particularOrderDetailsModel: ParticularOrderDetailsModel?
    //order status
    var orderStatusList = [OrderStatus]()
    var orderID = String()
    var segmentValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.orderDetails)
        tableViewNibs()
        
        orderStatusList.append(OrderStatus(title: "Order Placed", subtitle: "Thanks for placing the order"))
        orderStatusList.append(OrderStatus(title: "Shipped", subtitle: "Your order has been shipped"))
        orderStatusList.append(OrderStatus(title: "On the way", subtitle: "Your order is now out for delivery"))
        orderStatusList.append(OrderStatus(title: "Order Delivered", subtitle: "Your order is now delivered."))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        segmentValue = 1
        
        if Reachability.isConnectedToNetwork() {
            userOrderDetails()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = #colorLiteral(red: 0.1882352941, green: 0.4705882353, blue: 0.6509803922, alpha: 1)
        cancelBtn.layer.cornerRadius = 10
        
    }
    
    func tableViewNibs(){
        
        orderStatusTblVw.estimatedRowHeight = orderStatusTblVw.rowHeight
        orderStatusTblVw.rowHeight = UITableView.automaticDimension
        orderStatusTblVw.separatorStyle = .none
        
        
//        self.itemsCollVw.delegate = self
//        self.itemsCollVw.dataSource = self
        
        self.orderStatusTblVw.delegate = self
        self.orderStatusTblVw.dataSource = self
        
        orderStatusTblVw.register(UINib(nibName: OrderStatusTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: OrderStatusTableViewCell.identifier)
        orderStatusTblVw.register(UINib(nibName: ReturnItemTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ReturnItemTableViewCell.identifier)
//        itemsCollVw.register(UINib(nibName: CategoriesCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: CategoriesCollectionViewCell.identifier)
        
    }
    
    //MARK:-  Selector
    @IBAction func onClickCancelBtn(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "Return Order" {
            let vc = storyboard?.instantiateViewController(identifier: "ReturnItemsViewController") as! ReturnItemsViewController
            vc.orderID = orderID
            vc.particularProductDetail = particularOrderDetailsModel?.data?[0].productDetails
            navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = storyboard?.instantiateViewController(identifier: "CancelOrderViewController") as! CancelOrderViewController
            vc.orderID = orderID
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //Segment changes
    @IBAction func onTapSegmentController(_ sender: Any) {
        
        if segment.selectedSegmentIndex == 0 {
            print("order status")
            segmentValue = 1
            
        }else if segment.selectedSegmentIndex == 1 {
            segmentValue = 2
            print("order details")
            
        }
        
        orderStatusTblVw.reloadData()
        
    }
    
    
    //MARK:- API
    func userOrderDetails() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.order_id: orderID
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: orderDetails_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(ParticularOrderDetailsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.particularOrderDetailsModel = responseJson
                
                DispatchQueue.main.async {
                    self.orderIDLbl.text = "Id: \(responseJson.data?[0].orderID ?? "")"
                    //self.orderStatusLbl.text = responseJson.data?[0].orderStatus ?? ""
                    self.priceLbl.text = String(format: "$%@ ", responseJson.data?[0].orderAmount ?? "")
                    self.totalItemsDateLbl.text = "\(responseJson.data?[0].productDetails?.count ?? 0) items | \(responseJson.data?[0].deliveryDate ?? "")"
                    self.timeSlotLbl.text = responseJson.data?[0].deliveryTime ?? ""
                    self.orderStatusTblVw.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}

//MARK:- Extensions Table view
extension OrderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if segmentValue == 1 {
            return orderStatusList.count
        }else{
            return particularOrderDetailsModel?.data?[0].productDetails?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if segmentValue == 1 {
            
            guard let cell = orderStatusTblVw.dequeueReusableCell(withIdentifier: OrderStatusTableViewCell.identifier, for: indexPath) as? OrderStatusTableViewCell else{return UITableViewCell()}
            cell.selectionStyle = .none
            if indexPath.row == 0 {
                cell.topVw.isHidden = true
            }else if indexPath.row == 3 {
                cell.bottomVw.isHidden = true
            }
            
            cell.ordertitleLbl.text = orderStatusList[indexPath.row].title
            cell.orderSubTitleLbl.text = orderStatusList[indexPath.row].subtitle
            cell.statusImageCheck.image = #imageLiteral(resourceName: "AddrNotSelect")
            
            /*
             'Order_Received','Order_Processing','Order_Packed','Order_Pickuped','Onthe_Way','Order_Delivered','Order_cancelled','Ready_to_pickup'
             */
            
            //let statuss = ["Order_Received","Order_Packed","Onthe_Way","Order_Delivered"]
            
            let status = "Order_Delivered"//self.particularOrderDetailsModel?.data?[0].orderStatus ?? ""
            
            if status == "Order_Received" {
                if indexPath.row == 0 {
                    cell.statusImageCheck.image = #imageLiteral(resourceName: "Radio  Check")
                }else{
                    cell.statusImageCheck.image = #imageLiteral(resourceName: "AddrNotSelect")
                }
                cancelBtn.setTitle("Cancel Order", for: .normal)
                
            }else if status == "Order_Packed" {
                if indexPath.row == 0 || indexPath.row == 1 {
                    cell.statusImageCheck.image = #imageLiteral(resourceName: "Radio  Check")
                }else{
                    cell.statusImageCheck.image = #imageLiteral(resourceName: "AddrNotSelect")
                }
                cancelBtn.setTitle("Cancel Order", for: .normal)
                
            }else if status == "Onthe_Way" {
                if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
                    cell.statusImageCheck.image = #imageLiteral(resourceName: "Radio  Check")
                }else{
                    cell.statusImageCheck.image = #imageLiteral(resourceName: "AddrNotSelect")
                }
                cancelBtn.setTitle("Cancel Order", for: .normal)
                
            }else if status == "Order_Delivered" {
                cell.statusImageCheck.image = #imageLiteral(resourceName: "Radio  Check")
                cancelBtn.setTitle("Return Order", for: .normal)
                
            }else{
                cell.statusImageCheck.image = #imageLiteral(resourceName: "AddrNotSelect")
            }
            
            return cell
            
        }else{
            let cell = orderStatusTblVw.dequeueReusableCell(withIdentifier: ReturnItemTableViewCell.identifier, for: indexPath) as! ReturnItemTableViewCell
            cell.selectedProImage.isHidden = true
            cell.selectedProBtn.isHidden = true
            cell.productWeightView.backgroundColor = #colorLiteral(red: 0.9882352941, green: 0.7215686275, blue: 0, alpha: 0.3)
            cell.productWeightLbl.textColor = #colorLiteral(red: 0.9882352941, green: 0.7215686275, blue: 0, alpha: 1)
            
            cell.productImage.sd_setImage(with: URL(string: particularOrderDetailsModel?.data?[0].productDetails?[indexPath.row].image ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
            cell.productNameLbl.text = particularOrderDetailsModel?.data?[0].productDetails?[indexPath.row].name ?? ""
            cell.productDescriptionLbl.text = "Gits"
            cell.quantityLbl.text = "Quantity - \(particularOrderDetailsModel?.data?[0].productDetails?[indexPath.row].quantity ?? "")"
            cell.productWeightLbl.text = particularOrderDetailsModel?.data?[0].productDetails?[indexPath.row].weight ?? ""
            
            cell.priceLbl.text = String(format: "$%@ / packet", particularOrderDetailsModel?.data?[0].productDetails?[indexPath.row].price ?? "")
            cell.priceLbl.highlight(searchedText: String(format: "$%@", particularOrderDetailsModel?.data?[0].productDetails?[indexPath.row].price ?? ""))
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if segmentValue == 1 {
            return 80
        }else{
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

