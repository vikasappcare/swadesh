//
//  OngoingOrderVC.swift
//  SWADESH
//
//  Created by Vikas on 12/01/21.
//

import UIKit

class OngoingOrderVC: UIViewController {

    @IBOutlet weak var ongoingTV: UITableView!
    
    //Model
    var ongoingOrdersModel: OngoingOrdersModel?
    var repeatorderModel: RepeatorderModel?
    
    var ongoingNamesString = String()
    var ongoingImageString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userOngoingOrders()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    func showTableView() {
        ongoingTV.delegate = self
        ongoingTV.dataSource = self
        
        ongoingTV.estimatedRowHeight = ongoingTV.rowHeight
        ongoingTV.rowHeight = UITableView.automaticDimension
        
        ongoingTV.register(OngoingOrderTVCell.nib, forCellReuseIdentifier: OngoingOrderTVCell.identifier)
    }
    
    //MARK:- API
    func userOngoingOrders() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.orderType: "Active"
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: viewOrders_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(OngoingOrdersModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.ongoingOrdersModel = responseJson
                DispatchQueue.main.async {
                    self.ongoingTV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
            
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    func userRepeatOrder(orderID: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.order_id: orderID
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: repeatOrder_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(RepeatorderModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.repeatorderModel = responseJson
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                print("success")
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
}

extension OngoingOrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ongoingOrdersModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ongoingTV.dequeueReusableCell(withIdentifier: OngoingOrderTVCell.identifier) as! OngoingOrderTVCell
        
        cell.orderIDLbl.text = "Id: \(ongoingOrdersModel?.data?[indexPath.row].orderID ?? "")"
        cell.totalItemsDateLbl.text = "\(ongoingOrdersModel?.data?[indexPath.row].productDetails?.count ?? 0) Items | \(ongoingOrdersModel?.data?[indexPath.row].deliveryDate ?? "")"
        cell.orderStatusLbl.text = ongoingOrdersModel?.data?[indexPath.row].orderStatus ?? ""
        cell.repeatOrderBtn.tag = indexPath.row
        cell.repeatOrderBtn.addTarget(self, action: #selector(onTaprepeatOrder(_:)), for: .touchUpInside)
        
        let title = ongoingOrdersModel?.data?[indexPath.row].productDetails?.count ?? 0
        
        for i in 0..<title {
            print(indexPath.row, i, title)
            //print(ongoingOrdersModel?.data?[indexPath.row].productDetails?[i].name ?? "", "order list")
            
            let value = ongoingOrdersModel?.data?[indexPath.row].productDetails?[i].name ?? ""
            ongoingNamesString += value + ","
            //print(ongoingNamesString, "vikas")
            
            let imgValue = ongoingOrdersModel?.data?[indexPath.row].productDetails?[i].image ?? ""
            ongoingImageString += imgValue + ","
        }
        
        //cell.orderListCV.reloadData()
        let valueString = ongoingImageString.components(separatedBy: ",")
        print(valueString, "images")
        
        cell.productImagesArray(valueString)
        
        let name = ongoingNamesString.dropLast()
        cell.orderListLbl.text = String(name)
        cell.totalPrice.text = String(format: "$%@ ", ongoingOrdersModel?.data?[indexPath.row].orderAmount ?? "")
        ongoingNamesString = ""
        ongoingImageString = ""
        return cell
    }
    
    @objc func onTaprepeatOrder(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            userRepeatOrder(orderID: ongoingOrdersModel?.data?[sender.tag].orderID ?? "")
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "OrderDetailsViewController") as!  OrderDetailsViewController
        vc.orderID = ongoingOrdersModel?.data?[indexPath.row].orderID ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
}
