//
//  MyOrdersVC.swift
//  SWADESH
//
//  Created by Vikas on 12/01/21.
//

import UIKit

class MyOrdersVC: UIViewController {

    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var displayView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let searchImage = UIBarButtonItem(image: UIImage(named: "Mask Group 49")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(onTapSearch(_:)))
        
        navigationItem.rightBarButtonItem = searchImage
        
        navTitle(heading: Title.orderHistory)
        ViewEmbedder.embed(withIdentifier: "OngoingOrderVC", parent: self, container: self.displayView) { (vc) in
            vc.isEditing = true
        }
    }
    
    //MARK:-  Nav bar Item Actions
    @objc func onTapSearch(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "SearchFilterViewController") as! SearchFilterViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Selector
    @IBAction func onSlideSegment(_ sender: Any) {
        
        if segment.selectedSegmentIndex == 0 {
            ViewEmbedder.embed(withIdentifier: "OngoingOrderVC", parent: self, container: self.displayView) { (vc) in
                vc.isEditing = true
            }
        }else{
            ViewEmbedder.embed(withIdentifier: "PastOrderVC", parent: self, container: self.displayView) { (vc) in
                vc.isEditing = true
            }
        }
        
    }
    
}
