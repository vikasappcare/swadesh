//
//  ProfileModel.swift
//  SWADESH
//
//  Created by Vikas on 25/01/21.
//

import Foundation

//MARK:- Profile pic upload
struct ProfilePicModel: Codable {
    let status: Int
    let message: String
}

//MARK:- Update Profile and profile same
struct ProfileModel: Codable {
    let status: Int
    let message: String
    let data: [DataProfile]?
}

// MARK: - Datum
struct DataProfile: Codable {
    let id, firstName, uid, lastName: String?
    let pPic: String?
    let email, mobile, cCode, otpStatus: String?
    let uotp, address2, address1, password: String?
    let userStatus, deviceID, deviceToken, deviceType: String?
    let cd: String?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case uid
        case lastName = "last_name"
        case pPic = "p_pic"
        case email, mobile
        case cCode = "c_code"
        case otpStatus = "otp_status"
        case uotp
        case address2 = "address_2"
        case address1 = "address_1"
        case password
        case userStatus = "user_status"
        case deviceID = "device_id"
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case cd
    }
}



