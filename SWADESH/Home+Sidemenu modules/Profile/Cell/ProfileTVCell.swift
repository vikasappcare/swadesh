//
//  ProfileTVCell.swift
//  SWADESH
//
//  Created by Vikas on 28/01/21.
//

import UIKit

class ProfileTVCell: UITableViewCell {
    
    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var userTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
