//
//  ProfileVC.swift
//  SWADESH
//
//  Created by Vikas on 19/01/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class ProfileVC: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileTV: UITableView!
    @IBOutlet weak var profileTVHeight: NSLayoutConstraint!
    
    let titleArray = ["First Name","Last Name","Mobile Number","Email Id"]
    var allCellsTFValues = ["","","",""]
    
    //Model
    var profileModel: ProfileModel?
    var imageData1 = [Data]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.profile)
        
        profileImage.image = #imageLiteral(resourceName: "user-gray")
        
        //height TV
        profileTVHeight.constant = CGFloat(titleArray.count * 85)
        
        if Reachability.isConnectedToNetwork() {
            userProfileApi()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    //MARK:- selector
    @IBAction func onTapProfilePic(_ sender: Any) {
        
        ImagePickerManager().pickImage(self) { (img) in
            self.profileImage.image = img
            let Profileimage = img
            let imgData = Profileimage.jpegData(compressionQuality: 0.5)!
            if Reachability.isConnectedToNetwork(){
                self.uploadPicDocumentforUSer(data:imgData)
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
                
            }
            print(img, "image print")
        }
        
    }
    
    @IBAction func onTapSaveProfile(_ sender: Any) {
        
        allCellsTFValues.removeAll()
        for i in 0..<4{
            
            let indexpath = IndexPath(row:i, section: 0)
            let cell = profileTV.cellForRow(at: indexpath) as? ProfileTVCell
            self.allCellsTFValues.append(cell?.userTextField.text ?? "")
        }
        
        print(allCellsTFValues, "values submit")
        
        if allCellsTFValues.contains(""){
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.allFields)
        }else{
            
            if Reachability.isConnectedToNetwork() {
                userProfileUpdateApi()
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
            }
        }
    }
    
    //MARK:- API
    //Profile view
    func userProfileApi() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID : uids
        ]
        
        print(params)
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: profile_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(ProfileModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.profileModel = responseJson
                userDefault.setValue(responseJson.data?[0].pPic ?? "", forKey: userDefaults.profilePic)
                userDefault.setValue(responseJson.data?[0].firstName ?? "" + (responseJson.data?[0].lastName ?? ""), forKey: userDefaults.fullName)
                userDefault.setValue(responseJson.data?[0].email ?? "", forKey: userDefaults.email)

                DispatchQueue.main.async {
                    print(responseJson.data?[0].pPic, "ppic")
                    self.profileImage.sd_setImage(with: URL(string: responseJson.data?[0].pPic?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "user-gray"))
                    self.profileTV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    //Profile Update
    func userProfileUpdateApi() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.firstName: allCellsTFValues[0],
            APIParameters.lastName: allCellsTFValues[1],
            APIParameters.mobile: allCellsTFValues[2],
            APIParameters.email: allCellsTFValues[3]
        ]
        print(params, "post profile")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: profileUpdate_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(ProfileModel.self, from: response) else{return}
            
            if responseJson.status == 200 {
                
                DispatchQueue.main.async {
                    self.userProfileApi()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
            
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    //MARK:- ProfilePicUpload
    func uploadPicDocumentforUSer(data:Data)  {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        self.imageData1.append(data)
        let url = profilePic_URL
        let parameters: [String:Any] = [
            APIParameters.uID:uids
        ]
        let headers: HTTPHeaders = [
            API.content:API.type, API.key:API.value
        ]
        
        UploadDocumentImage(isUser: true, endUrl: url, imageData: imageData1, parameters: parameters,isLoader: true, title: "", description: "Loading...", vc: self, headers: headers)
        print("url",url)
        print("image",imageData1)
        print(parameters,"parameters")
        
    }
    
    func UploadDocumentImage(isUser:Bool, endUrl: String, imageData: [Data]?,parameters: [String : Any],isLoader: Bool, title: String, description:String, vc:UIViewController,headers:HTTPHeaders, onCompletion: ((_ isSuccess:Bool) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        if isLoader {
            Indicator.shared().showIndicator(withTitle: title, and: description, vc: vc)
        }
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            //               if let data = [imageData]{
            //                   multipartFormData.append(data, withName: "images[]", fileName: "\(Date.init().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            //                   // multipartFormData.append(data, withName: "profilepic[]", fileName: "imageNew.jpeg", mimeType: "image/jpeg")
            //               }
            for imagesData in self.imageData1 {
                multipartFormData.append(imagesData, withName: "profilepic", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
        },
        to: endUrl, method: .post,headers: headers )
        .responseJSON(completionHandler: { (response) in
            Indicator.shared().hideIndicator(vc: vc)
            if let err = response.error{
                Indicator.shared().hideIndicator(vc: vc)
                print(err)
                onError?(err)
                return
            }
            let json = response.data
            if (json != nil)
            {
                let jsonObject = JSON(json!)
                print(jsonObject)
                DispatchQueue.main.async {
                    showAlertMessage(vc: self, titleStr: "", messageStr:jsonObject["message"].stringValue)
                    self.userProfileApi()
                }
            }
        })
    }
    
}

extension ProfileVC:UITableViewDelegate,UITableViewDataSource,
                    UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = profileTV.dequeueReusableCell(withIdentifier: ProfileTVCell.identifier, for: indexPath) as! ProfileTVCell
        
        cell.titleLbl.text = titleArray[indexPath.row]
        cell.userTextField.delegate = self
        cell.userTextField.tag = indexPath.row
        cell.selectionStyle = .none
        
        if indexPath.row == 0 {
            cell.userTextField.text = profileModel?.data?[0].firstName ?? ""
            self.allCellsTFValues[0] = profileModel?.data?[0].firstName ?? ""
        }else if indexPath.row == 1 {
            cell.userTextField.text = profileModel?.data?[0].lastName ?? ""
            self.allCellsTFValues[1] = profileModel?.data?[0].lastName ?? ""
        }else if indexPath.row == 2 {
            cell.userTextField.text = profileModel?.data?[0].mobile ?? ""
            self.allCellsTFValues[2] = profileModel?.data?[0].mobile ?? ""
        }else if indexPath.row == 3 {
            cell.userTextField.text = profileModel?.data?[0].email ?? ""
            self.allCellsTFValues[3] = profileModel?.data?[0].email ?? ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        print("Sireesha", textField.text)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            self.allCellsTFValues[0] = textField.text!
        }else if textField.tag == 1 {
            self.allCellsTFValues[1] = textField.text!
        }else if textField.tag == 2 {
  
            if textField.text!.isValidPhonenumber() {
                self.allCellsTFValues[2] = textField.text!
            }else{
                //show alert for valid phone number
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.mobileFormat)
                self.allCellsTFValues[2] = ""
            }
            
        }else if textField.tag == 3 {
            if textField.text!.isValidEmail() {
                self.allCellsTFValues[3] = textField.text!
                
            }else{
                ////show alert for valid email
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.emailFormat)
                self.allCellsTFValues[3] = ""

            }
        }
        print("ios")
        print(allCellsTFValues)
        return true
    }
    
}
