//
//  AddressListVC.swift
//  SWADESH
//
//  Created by Vikas on 12/01/21.
//

import UIKit

class AddressListVC: UIViewController {
    
    @IBOutlet weak var addrListTV: UITableView!
    
    var selectedIndex: Int?
    var isFromPaymentSummary = Bool()
    
    
    //Model
    var addressListModel: AddressListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.address)
        tableViewSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Reachability.isConnectedToNetwork() {
            userAddressListAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    func tableViewSetup() {
        
        addrListTV.delegate = self
        addrListTV.dataSource = self
        addrListTV.separatorStyle = .none
        addrListTV.tableFooterView = UIView()
        addrListTV.estimatedRowHeight = addrListTV.rowHeight
        addrListTV.rowHeight = UITableView.automaticDimension
    }
    
    @IBAction func onTapAddNewAddr(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewAddressVC") as! AddNewAddressVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- API
    func userAddressListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addressList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(AddressListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.addressListModel = responseJson
                
                DispatchQueue.main.async {
                    self.addrListTV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    //Delete addr
    func userAddressDeleteAPI(id: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.id: id
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addressDelete_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(AddAddressModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                DispatchQueue.main.async {
                    self.userAddressListAPI()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}

extension AddressListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = addrListTV.dequeueReusableCell(withIdentifier: AddrListTVCell.identifier) as! AddrListTVCell
        
        let details = addressListModel?.data?[indexPath.row]
        
        cell.addrNameLbl.text = details?.addressType ?? ""
        let addr1 = (details?.street1 ?? "")+" "+(details?.street2 ?? "")
        let addr2 = (details?.city ?? "")+" "+(details?.state ?? "")
        let addr3 = (details?.postalcode ?? "")
        
        cell.addressLbl.text = addr1+" "+addr2+" "+addr3
        cell.addrSelectBtn.setImage(nil, for: .normal)
        
        cell.addrEdit.tag = indexPath.row
        cell.addrDelete.tag = indexPath.row
        
        cell.addrEdit.addTarget(self, action: #selector(onTapEdit(_:)), for: .touchUpInside)
        cell.addrDelete.addTarget(self, action: #selector(onTapDelete(_:)), for: .touchUpInside)
        
        if selectedIndex != nil {
            if selectedIndex == indexPath.row {
                cell.addrSelectBtn.setImage(#imageLiteral(resourceName: "AddrSelect"), for: .normal)
            }else{
                cell.addrSelectBtn.setImage(nil, for: .normal)
            }
        }else{
            cell.addrSelectBtn.setImage(nil, for: .normal)
        }
        
        return cell
    }
    
    @objc func onTapEdit(_ sender: UIButton) {
        let editVC = storyboard?.instantiateViewController(identifier: "AddNewAddressVC") as! AddNewAddressVC
        editVC.isComingFrom = "edit"
        editVC.addrid = addressListModel?.data?[sender.tag].id ?? ""
        editVC.street1 = addressListModel?.data?[sender.tag].street1 ?? ""
        editVC.street2 = addressListModel?.data?[sender.tag].street2 ?? ""
        editVC.city = addressListModel?.data?[sender.tag].city ?? ""
        editVC.state = addressListModel?.data?[sender.tag].state ?? ""
        editVC.zipCode = addressListModel?.data?[sender.tag].postalcode ?? ""
        editVC.addrName = addressListModel?.data?[sender.tag].addressType ?? ""
        
        navigationController?.pushViewController(editVC, animated: true)
    }
    
    @objc func onTapDelete(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            userAddressDeleteAPI(id: addressListModel?.data?[sender.tag].id ?? "")
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        addrListTV.deselectRow(at: indexPath, animated: true)
        selectedIndex = indexPath.row
        let details = addressListModel?.data?[indexPath.row]
        
        let addr1 = (details?.street1 ?? "")+" "+(details?.street2 ?? "")
        let addr2 = (details?.city ?? "")+" "+(details?.state ?? "")
        let addr3 = (details?.postalcode ?? "")

        SELECTED_ADDRESS = addr1+" "+addr2+" "+addr3
        SELECTED_ADDRESS_ID = details?.id ?? ""

        if isFromPaymentSummary == true {
            
            self.navigationController?.popViewController(animated: true)
        }
        DispatchQueue.main.async {
            self.addrListTV.reloadData()
        }
        
    }
    
}
