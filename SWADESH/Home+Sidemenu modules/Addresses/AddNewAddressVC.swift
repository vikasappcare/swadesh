//
//  AddNewAddressVC.swift
//  SWADESH
//
//  Created by Vikas on 12/01/21.
//

import UIKit

class AddNewAddressVC: UIViewController {
    
    @IBOutlet weak var street1TF: UITextField!
    @IBOutlet weak var street2TF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var zipcodeTF: UITextField!
    @IBOutlet weak var addrNameTF: UITextField!
    @IBOutlet weak var addNewAddrBtn: UIButton!
    
    //Model
    var addAddrModel: AddAddressModel?
    
    var isComingFrom = String()
    
    var addrid = String()
    var street1 = String()
    var street2 = String()
    var city = String()
    var state = String()
    var zipCode = String()
    var addrName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.newAddress)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if isComingFrom == "edit" {
            
            navTitle(heading: "Edit Address")
            addNewAddrBtn.setTitle("Update Address", for: .normal)
            street1TF.text = street1
            street2TF.text = street2
            cityTF.text = city
            stateTF.text = state
            zipcodeTF.text = zipCode
            addrNameTF.text = addrName
        }
        
    }
    
    //MARK:- Selector
    @IBAction func onTapAddAddress(_ sender: Any) {
        
        if street1TF.text!.isEmpty && street2TF.text!.isEmpty && cityTF.text!.isEmpty && stateTF.text!.isEmpty && zipcodeTF.text!.isEmpty && addrNameTF.text!.isEmpty {
            
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.allFields)
        }else{
            
            if street1TF.text!.isEmpty {
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.street1)
            }else{
                if street2TF.text!.isEmpty {
                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.street2)
                }else{
                    if cityTF.text!.isEmpty {
                        showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.city)
                    }else{
                        if stateTF.text!.isEmpty {
                            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.state)
                        }else{
                            if zipcodeTF.text!.isEmpty {
                                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.zipCode)
                            }else{
                                if addrNameTF.text!.isEmpty {
                                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.addrName)
                                }else{
                                    if Reachability.isConnectedToNetwork() {
                                        
                                        if isComingFrom == "edit" {
                                            userEditAddressAPI()
                                        }else{
                                            userAddAddressAPI()
                                        }
                                    }else{
                                        showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
}

extension AddNewAddressVC {
    
    //MARK:-  Add Address
    func userAddAddressAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.addressType: addrNameTF.text ?? "",
            APIParameters.street1: street1TF.text ?? "",
            APIParameters.street2: street2TF.text ?? "",
            APIParameters.state: stateTF.text ?? "",
            APIParameters.city: cityTF.text ?? "",
            APIParameters.postalcode: zipcodeTF.text ?? ""
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addressAdd_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(AddAddressModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
        
    }
    
    //MARK:-  Edit Address
    func userEditAddressAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.addressType: addrNameTF.text ?? "",
            APIParameters.street1: street1TF.text ?? "",
            APIParameters.street2: street2TF.text ?? "",
            APIParameters.state: stateTF.text ?? "",
            APIParameters.city: cityTF.text ?? "",
            APIParameters.postalcode: zipcodeTF.text ?? "",
            APIParameters.id: addrid
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addressEdit_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(AddAddressModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}
