//
//  CategoryListModel.swift
//  SWADESH
//
//  Created by Vikas on 29/01/21.
//

import Foundation

struct CategoryListModel: Codable {
    let status: Int
    let message: String
    let data: [DataCat]?
}

// MARK: - Datum
struct DataCat: Codable {
    let id, name, status, image: String?
    let discription, cd: String?
}
