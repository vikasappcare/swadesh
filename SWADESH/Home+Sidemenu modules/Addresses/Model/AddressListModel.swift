//
//  AddressListModel.swift
//  SWADESH
//
//  Created by Vikas on 29/01/21.
//

import Foundation

//MARK:- Address List
struct AddressListModel: Codable {
    let status: Int
    let message: String
    let data: [DataAddr]?
}

// MARK: - Datum
struct DataAddr: Codable {
    let id, uid, addressType, state: String?
    let street1, street2, city, postalcode: String?
    let cd, status: String?

    enum CodingKeys: String, CodingKey {
        case id, uid
        case addressType = "address_type"
        case state, street1, street2, city, postalcode, cd, status
    }
}

//MARK:- Add Address
struct AddAddressModel: Codable {
    let status: Int
    let message: String
}
