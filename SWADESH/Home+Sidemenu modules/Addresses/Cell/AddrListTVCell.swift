//
//  AddrListTVCell.swift
//  SWADESH
//
//  Created by Vikas on 12/01/21.
//

import UIKit

class AddrListTVCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var addrNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var addrSelectBtn: UIButton!
    @IBOutlet weak var addrEdit: UIButton!
    @IBOutlet weak var addrDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
