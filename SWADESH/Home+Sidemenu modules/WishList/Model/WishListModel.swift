//
//  WishListModel.swift
//  SWADESH
//
//  Created by Bhasker on 19/02/21.
//

import Foundation

struct WishListModel: Codable {
    let status: Int
    let message: String
    let data: [DataWishList]?
}

// MARK: - Datum
struct DataWishList: Codable {
    let id, categoryID, itemName, datumDescription: String?
    let itemPrice, weight, quantity, expiryDate: String?
    let image: String?
    let status, cd, cartlist, cartQuantity: String?
    let cartGrams, cartPrice: String?
    let priceWeight: [PriceWeight]?
    let wishlist: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case itemName = "item_name"
        case datumDescription = "description"
        case itemPrice = "item_price"
        case weight, quantity
        case expiryDate = "expiry_date"
        case image, status, cd, cartlist
        case cartQuantity = "cart_quantity"
        case cartGrams = "cart_grams"
        case cartPrice = "cart_price"
        case priceWeight = "price_weight"
        case wishlist
    }
}


// MARK: - PriceWeight
struct PriceWeight: Codable {
    let id, weight, price: String?
}

