//
//  productTVCell.swift
//  SWADESH
//
//  Created by Vikas on 18/01/21.
//

import UIKit
import DropDown

class productTVCell: UITableViewCell {
    
    class var identifier: String {
        return "\(self)"
    }
    
    class var nib: UINib {
        return UINib(nibName: productTVCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productWeightBtn: UIButton!
    @IBOutlet weak var productWeightLbl: UILabel!
    @IBOutlet weak var minBtn: UIButton!
    @IBOutlet weak var maxBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var addCartBtn: UIButton!
    @IBOutlet weak var spinnerAnimatedImage: UIImageView!
    @IBOutlet weak var checkMarkImage: UIImageView!
    @IBOutlet weak var animatedViewWidth: NSLayoutConstraint! //40
    
    var animating = false
    let quantityDropDown = DropDown()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        productImage.contentMode = .scaleAspectFit
    }
    
    func loadWishListData(wishList:DataWishList?){
        
        productImage.sd_setImage(with: URL(string: wishList?.image ?? ""), placeholderImage: UIImage(systemName: ""))
        productName.text = wishList?.itemName ?? ""
        productPriceLbl.text = wishList?.itemPrice ?? ""
        productWeightLbl.text = wishList?.weight ?? ""
        quantityLbl.text = wishList?.quantity ?? ""
        
    }
    
    func loadQuantityData(quantityData: [String], priceData: [String]){
        
        print("Drop down called")
        quantityDropDown.anchorView = productWeightBtn
        quantityDropDown.bottomOffset = CGPoint(x: 0, y: productWeightBtn.bounds.height)
        quantityDropDown.dataSource = quantityData
        quantityDropDown.selectionAction = { [weak self] (index, item) in
            print("Drop down printed")

            self?.productWeightLbl.text = item
            self?.productPriceLbl.text = priceData[index]
        }

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: -  Animation
    
    func spin(with options: UIView.AnimationOptions) {
        
        UIView.animate(withDuration: 1.2, delay: 0, options: options, animations: {
            
            self.spinnerAnimatedImage.image = UIImage(named: "Spinner")
            
            self.spinnerAnimatedImage.transform = self.spinnerAnimatedImage.transform.rotated(by: .pi / 2)
            
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                
                self.stopSpin()
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    
                    //self.checkMarkImage.image = UIImage(named: "Radio  Check")
                    
                }
            }
            
        }) { finished in
            
            if finished {
                
                self.checkMarkImage.image = UIImage(named: "Radio  Check")
                
                if self.animating {
                    
                    self.spin(with: .curveLinear)
                    
                } else if options != .curveEaseOut {
                    
                    self.spin(with: .curveEaseOut)
                }
            }
        }
    }
    
    @objc func startSpin() {
        
        if !animating {
            
            animating = true
            spin(with: .curveEaseIn)
        }
    }
    
    func stopSpin() {
        
        animating = false
    }
    
}
