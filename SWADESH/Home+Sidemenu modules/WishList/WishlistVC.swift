//
//  WishlistVC.swift
//  SWADESH
//
//  Created by Vikas on 18/01/21.
//


import UIKit

class WishlistVC: UIViewController {

    @IBOutlet weak var totalItemsLbl: UILabel!
    @IBOutlet weak var productsTV: UITableView!
    
    //  Models
    var wishListModel: WishListModel?
    
    let badgeCount = UILabel()
    
    var quantity = Int()
    
    var quantityArr:[String] = []
    var dummyArr = [Int]()
    var animating = false
    var selectAnimation = Int()
    var isAnimationSelected = Bool()
    var animationSelectedArr = [Int]()
    var productId = String()
    var CatId = String()
    var selectedCatId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.wishlist)
        setupTableAndCollection()
        
        //Barbuttons
        badgeCount.frame = CGRect(x: 15, y: -05, width: 16, height: 16)
        badgeCount.layer.borderColor = UIColor.clear.cgColor
        badgeCount.layer.borderWidth = 2
        badgeCount.layer.cornerRadius = badgeCount.bounds.height / 2
        badgeCount.textAlignment = .center
        badgeCount.layer.masksToBounds = true
        badgeCount.textColor = .white
        badgeCount.font = badgeCount.font.withSize(12)
        badgeCount.backgroundColor = .red
        //badgeCount.text = "4"
        
        
        let cartImg = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        cartImg.setBackgroundImage(UIImage(named: "cart_gray"), for: .normal)
        cartImg.addTarget(self, action: #selector(onTapCart(_:)), for: .touchUpInside)
        cartImg.addSubview(badgeCount)
        
        
        let cartImage = UIBarButtonItem(customView: cartImg)

        let searchImage = UIBarButtonItem(image: UIImage(named: "Mask Group 49")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(onTapSearch(_:)))
        
        navigationItem.rightBarButtonItems = [cartImage,searchImage]
        
        selectAnimation = -1
        isAnimationSelected = false
    }
    
    //MARK:-  Nav bar Item Actions
    @objc func onTapSearch(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "SearchFilterViewController") as! SearchFilterViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onTapCart(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userWishListAPI()
            cartProductsListAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    func setupTableAndCollection() {
        
        productsTV.delegate = self
        productsTV.dataSource = self
        productsTV.tableFooterView = UIView()
        productsTV.register(productTVCell.nib, forCellReuseIdentifier: productTVCell.identifier)
        productsTV.estimatedRowHeight = 140
        productsTV.rowHeight = UITableView.automaticDimension
        
    }
    
    
}


extension WishlistVC {
    
    //MARK:-  API
    //Wishlist List
    func userWishListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.uID : uids,
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: usersWishListURL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(WishListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.wishListModel = responseJson
                
                if self.wishListModel?.data?.count ?? 0 > 0 {
                    for _ in 0..<self.wishListModel!.data!.count {
                        self.dummyArr.append(0)
                    }
                }
                DispatchQueue.main.async {
                    self.totalItemsLbl.text = "\(responseJson.data?.count ?? 0) Items"
                    self.productsTV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    //Remove Wishlist
    func userRemoveWishlistAPI(pID: String, cID: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.product_id : pID,
            APIParameters.category_id : cID,
            APIParameters.uID : uids
        ]
        
        print(params, "remove wishlist")

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: deleteWishList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.userWishListAPI()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Add to cart
    func addToCartAPI(cid:String, pid:String, price:String, quantityValue:String, weight:String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityValue,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("Parameres: \(params)")
        print("UriString: \(addToCart_URL)")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.userRemoveWishlistAPI(pID: pid, cID: cid)
                self.cartProductsListAPI()
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Cart list
    func cartProductsListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: cartProductsList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CartProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                DispatchQueue.main.async {
                    self.badgeCount.text = "\(responseJson.data?.count ?? 0)"
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}


//MARK:-  Extension TableView
extension WishlistVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = productsTV.dequeueReusableCell(withIdentifier: productTVCell.identifier, for: indexPath) as? productTVCell else{return UITableViewCell()}
        
        cell.loadWishListData(wishList: wishListModel?.data?[indexPath.row])
        
        cell.cancelBtn.tag = indexPath.row
        cell.cancelBtn.addTarget(self, action: #selector(deleteItem(_:)), for: .touchUpInside)
        
        cell.minBtn.tag = indexPath.row
        cell.minBtn.addTarget(self, action: #selector(onTapMinBtn(_:)), for: .touchUpInside)
        cell.maxBtn.tag = indexPath.row
        cell.maxBtn.addTarget(self, action: #selector(onTapMaxBtn(_:)), for: .touchUpInside)
        cell.productWeightBtn.tag = indexPath.row
        cell.productWeightBtn.addTarget(self, action: #selector(onTapWeightdropDownBtn(_:)), for: .touchUpInside)
        cell.addCartBtn.tag = indexPath.row
        cell.addCartBtn.addTarget(self, action: #selector(onTapAddToCart(_:)), for: .touchUpInside)
        

        if dummyArr[indexPath.row] == 1 {
            UIView.animate(withDuration: 0.5) {
                cell.animatedViewWidth.constant = 40
                cell.spinnerAnimatedImage.image = UIImage(named: "Radio  Check")
            }
        }else {
            cell.animatedViewWidth.constant = 0
        }
        
        if selectAnimation == indexPath.row {
            
            cell.startSpin()
        }else {
            cell.stopSpin()
        }


        return cell
    }
    
    @objc func deleteItem(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
            userRemoveWishlistAPI(pID: wishListModel?.data?[sender.tag].id ?? "", cID: wishListModel?.data?[sender.tag].categoryID ?? "")
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    @objc func onTapMinBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: productsTV)
        let indexPath = productsTV.indexPathForRow(at:buttonPosition)
        let cell = productsTV.cellForRow(at: indexPath!) as? productTVCell
        quantity = Int(cell?.quantityLbl.text ?? "0") ?? 0
        
        if quantity > 1 {
            quantity -= 1
        }
        cell?.quantityLbl.text = String(quantity)
    }
    
    @objc func onTapMaxBtn(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: productsTV)
        let indexPath = productsTV.indexPathForRow(at:buttonPosition)
        let cell = productsTV.cellForRow(at: indexPath!) as? productTVCell
        quantity = Int(cell?.quantityLbl.text ?? "0") ?? 0
        quantity += 1
        cell?.quantityLbl.text = String(quantity)
    }
    
    @objc func onTapWeightdropDownBtn(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = productsTV.cellForRow(at: indexPath) as! productTVCell
        
        let details = wishListModel?.data?[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
    }
    
    @objc func onTapAddToCart(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: productsTV)
        let indexPath = productsTV.indexPathForRow(at:buttonPosition)
        let cell = productsTV.cellForRow(at: indexPath!) as? productTVCell
        
        print("addto cart")
        isAnimationSelected = true
        selectAnimation = sender.tag
        dummyArr[sender.tag] = 1
        
        if Reachability.isConnectedToNetwork() {
            let details = wishListModel?.data?[sender.tag]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell?.productPriceLbl.text ?? "", quantityValue: String(quantity), weight: cell?.productWeightLbl.text ?? "")
            productsTV.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
