//
//  ReferFriendVC.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class ReferFriendVC: UIViewController {
    
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var dottedView: RectangularDashedView!
    @IBOutlet weak var couponLbl: UILabel!
    @IBOutlet weak var onTapButton: UIButton!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var referNowBtn: UIButton!
    @IBOutlet weak var referalCodeTF: UITextField!
    
    //Model
    var referalCodeModel: ReferalCodeModel?
    var referalPointsModel: ReferalPointsModel?
    
    var referalBalanceLbl: UILabel = {
        var referal = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        referal.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        referal.numberOfLines = 2
        referal.textAlignment = .right
        referal.font = UIFont(name: Fonts.regular, size: 14)
        return referal
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.refer)
        //referalBalanceLbl.text = "Balance 0"
        
        
        if Reachability.isConnectedToNetwork() {
            userReferalCode()
            userReferalPoints()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
        let rightBarbutton = UIBarButtonItem(customView: referalBalanceLbl)
        navigationItem.rightBarButtonItem = rightBarbutton
    }
    
    //MARK:- Selector
    @IBAction func onTapCopyCode(_ sender: Any) {
        UIPasteboard.general.string = couponLbl.text
    }
    
    @IBAction func onTapReferNow(_ sender: Any) {
        
        if referNowBtn.titleLabel?.text == "Refer Now" {
            
            let referPopupVC = storyboard?.instantiateViewController(identifier: "ReferNowVC") as! ReferNowVC
            referPopupVC.modalPresentationStyle = .overCurrentContext
            referPopupVC.referalCode = referalCodeModel?.data?.referalCode ?? ""
            present(referPopupVC, animated: false, completion: nil)
            
            
//            // text to share
//            let text = "This is some text that I want to share."
//
//            // set up activity view controller
//            let textToShare = [ text ]
//            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
//            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//
//            // exclude some activity types from the list (optional)
//            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
//
//            // present the view controller
//            self.present(activityViewController, animated: true, completion: nil)
        }else{
            showAlertMessage(vc: self, titleStr: "", messageStr: "you can submit the code")
        }
    }
    
    //MARK:- API
    //referal code
    func userReferalCode() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID : uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: referalCode_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(ReferalCodeModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.referalCodeModel = responseJson
                
                DispatchQueue.main.async {
                    
                    if responseJson.data?.referalStatus ?? "" == "yes" {
                        self.referalCodeTF.isHidden = true
                        self.couponLbl.text = responseJson.data?.referalCode ?? ""
                        self.descriptionLbl.text = responseJson.data?.referalDescription ?? ""
                        self.referNowBtn.setTitle("Refer Now", for: .normal)
                    }else{
                        self.referalCodeTF.isHidden = false
                        self.referNowBtn.setTitle("Submit", for: .normal)
                    }
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //referal points
    func userReferalPoints() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: referalPoints_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(ReferalPointsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.referalPointsModel = responseJson
                print(responseJson.data?[0].points ?? "", "points credited")
                DispatchQueue.main.async {
                    self.referalBalanceLbl.text = "Bonus: \(responseJson.data?[0].points ?? "")"
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}


