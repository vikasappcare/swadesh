//
//  ReferEmailModel.swift
//  SWADESH
//
//  Created by apple on 08/04/21.
//

import Foundation

//MARK:- Referal Email to others
struct ReferEmailModel: Codable {
    let status: Int
    let message: String
}

//MARK:- Get referal code
struct ReferalCodeModel: Codable {
    let status: Int
    let message: String
    let data: DataReferal?
}

struct DataReferal: Codable {
    let referalCode, referalDescription, referalStatus: String?

    enum CodingKeys: String, CodingKey {
        case referalCode = "referal_code"
        case referalDescription = "referal_description"
        case referalStatus = "referal_status"
    }
}

//MARK:- Referal points
struct ReferalPointsModel: Codable {
    let status: Int
    let message: String
    let data: [DataPoints]?
}

// MARK: - Datum
struct DataPoints: Codable {
    let points: String?
}

