//
//  ReferNowVC.swift
//  SWADESH
//
//  Created by apple on 06/04/21.
//

import UIKit

class ReferNowVC: UIViewController {

    @IBOutlet weak var emailIDTF: UITextField!
    
    //Model
    var referEmailModel: ReferEmailModel?
    
    var referalCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer()
        tap.numberOfTouchesRequired = 1
        tap.addTarget(self, action: #selector(onTapView(_:)))
        view.addGestureRecognizer(tap)
        
    }
    
    @objc func onTapView(_ gesture: UITapGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func onTapSend(_ sender: Any) {
        
        if emailIDTF.text!.isValidEmail() {
            
            if Reachability.isConnectedToNetwork() {
                userReferFriend()
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
            }
        }else{
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.emailFormat)
        }
        
    }
    
    //MARK:- Refer a friend
    func userReferFriend() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.emailID: emailIDTF.text ?? "",
            APIParameters.uID: uids,
            APIParameters.referal_code: referalCode
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: referEmailID_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(ReferEmailModel.self, from: response) else{return}
            
            if responseJson.status == 200 {
                self.referEmailModel = responseJson
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.dismiss(animated: false, completion: nil)
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}
