//
//  ChangePasswordVC.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    @IBOutlet weak var oldPswdTf: UITextField!
    @IBOutlet weak var newPswdTF: UITextField!
    @IBOutlet weak var confirmNewPswdTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.changePassword)
    }
    
    //MARK:- selector
    @IBAction func onTapForgotPswd(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onTapChangePswd(_ sender: Any) {
        
        if oldPswdTf.text!.isEmpty && newPswdTF.text!.isEmpty && confirmNewPswdTF.text!.isEmpty {
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.allFields)
        }else{
            if oldPswdTf.text!.isEmpty{
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.oldPswd)
            }else{
                if newPswdTF.text!.isEmpty{
                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.newPswd)
                }else{
                    if confirmNewPswdTF.text!.isEmpty{
                        showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.confirmNewPswd)
                    }else{
                        if newPswdTF.text != confirmNewPswdTF.text {
                            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.samePswd)
                        }else{
                            
                            if Reachability.isConnectedToNetwork() {
                                userChangePswdAPI()
                            }else{
                                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
                            }
                            print("success")
                        }
                    }
                }
            }
        }
        
    }
    
}

//MARK:- API Call Extension
extension ChangePasswordVC {
    
    func userChangePswdAPI() {
        
        let params = [
            APIParameters.newpassword : oldPswdTf.text ?? "",
            APIParameters.password : newPswdTF.text ?? "",
            APIParameters.cpassword : confirmNewPswdTF.text ?? "",
            APIParameters.email : "",
            APIParameters.uID : ""
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: changePswd_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(ChangePswdModel.self, from: response) else{return}
            
            if responseJson.status == 200 {
                let vc = self.storyboard?.instantiateViewController(identifier: "SuccessPasswordChangeVC") as! SuccessPasswordChangeVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}
