//
//  SettingsVC.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class SettingsVC: UIViewController {

    @IBOutlet weak var settingsTV:UITableView!
    
    var settingsList = ["Change password", "Terms and Conditions", "Privacy Policy", "Faq"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.settings)
        tableViewSetUp()
        
    }
    
    //Setup
    func tableViewSetUp() {
        
        settingsTV.delegate = self
        settingsTV.dataSource = self
        settingsTV.tableFooterView = UIView()
        settingsTV.separatorInset = UIEdgeInsets(top: 30, left: 40, bottom: 30, right: 40)
        settingsTV.separatorColor = UIColor.init(red: 205/255, green: 212/255, blue: 217/255, alpha: 1.0)
        
        settingsTV.estimatedRowHeight = settingsTV.rowHeight
        settingsTV.rowHeight = UITableView.automaticDimension
    }
    
    
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = settingsTV.dequeueReusableCell(withIdentifier: SettingsTVCell.identifier, for: indexPath) as! SettingsTVCell
        cell.nameLbl.text = settingsList[indexPath.row]
        
        if indexPath.row == 3{
            cell.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.row
        
        switch index {
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "TermsAndConditions") as! TermsAndConditions
            navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "Privacypolicy") as! Privacypolicy
            navigationController?.pushViewController(vc, animated: true)
        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
            navigationController?.pushViewController(vc, animated: true)
        default:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
