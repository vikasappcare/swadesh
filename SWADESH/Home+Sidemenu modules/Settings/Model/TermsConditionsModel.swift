//
//  TermsConditionsModel.swift
//  SWADESH
//
//  Created by Vikas on 25/01/21.
//

import Foundation

//MARK:- Change Password
struct ChangePswdModel: Codable {
    let status: Int
    let message: String
}
//MARK:- Change Password
struct SuccesMessageModel: Codable {
    let status: Int
    let message: String
}

//MARK:- T&C
struct TermsConditionsModel: Codable {
    let status: Int
    let message: String
    let data: [DataTerms]?
}

struct DataTerms: Codable {
    let id, discription, status, cd: String?
}

//MARK:- Privacy Policy
struct PrivacyModel: Codable {
    let status: Int
    let message: String
    let data: [DataPrivacy]?
}

struct DataPrivacy: Codable {
    let id, discription, sts, cd: String?
}

//MARK:- FAQ'S
struct FAQModel: Codable {
    let status: Int
    let message: String
    let data: [DataFAQ]?
}

// MARK: - Datum
struct DataFAQ: Codable {
    let id, question, answer, status: String?
    let cd: String?
}
