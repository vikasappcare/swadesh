//
//  Privacypolicy.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class Privacypolicy: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.privacypolicy)
        
        if Reachability.isConnectedToNetwork() {
            privacyAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    func privacyAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: privacy_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(PrivacyModel.self, from: response) else{return}
            
            if responseJson.status == 200 {
                self.textView.text = responseJson.data?[0].discription
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}
