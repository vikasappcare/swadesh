//
//  FAQVC.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class FAQVC: UIViewController {

    @IBOutlet weak var faqTV:UITableView!
    
    private var faqModel: FAQModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.faq)
        tableViewSetup()
        
        if Reachability.isConnectedToNetwork() {
            faqAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    //Setup
    func tableViewSetup() {
        
        faqTV.delegate = self
        faqTV.dataSource = self
        //sections
        faqTV.estimatedSectionHeaderHeight = faqTV.sectionHeaderHeight
        faqTV.sectionHeaderHeight = UITableView.automaticDimension
        //rows of TV
        faqTV.estimatedRowHeight = faqTV.rowHeight
        faqTV.rowHeight = UITableView.automaticDimension
        
    }
    
    func faqAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: faq_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(FAQModel.self, from: response) else{return}
            
            if responseJson.status == 200 {
                self.faqModel = responseJson
                
                DispatchQueue.main.async {
                    self.faqTV.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}

extension FAQVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return faqModel?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = faqTV.dequeueReusableCell(withIdentifier: HeaderTVCell.identifier) as! HeaderTVCell
        cell.titleLbl.text = "\(section+1).  \(faqModel?.data?[section].question ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = faqTV.dequeueReusableCell(withIdentifier: FAQTVCell.identifier) as! FAQTVCell
        cell.subtitleLbl.text = faqModel?.data?[indexPath.row].answer ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
