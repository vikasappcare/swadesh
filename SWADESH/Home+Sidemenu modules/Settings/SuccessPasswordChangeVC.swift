//
//  SuccessPasswordChangeVC.swift
//  SWADESH
//
//  Created by Sidhu on 03/02/21.
//

import UIKit

class SuccessPasswordChangeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+3.0) {
            self.navigationController?.backToViewController(vc: SettingsVC.self)
        }
        
    }

}
