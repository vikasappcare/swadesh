//
//  SettingsTVCell.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class SettingsTVCell: UITableViewCell {

    class var identifier:String {
        return "\(self)"
    }
    
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
