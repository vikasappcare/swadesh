//
//  FAQTVCell.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class FAQTVCell: UITableViewCell {

    class var identifier:String {
        return "\(self)"
    }
    @IBOutlet weak var subtitleLbl:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class HeaderTVCell: UITableViewCell {
    
    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var titleLbl:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
