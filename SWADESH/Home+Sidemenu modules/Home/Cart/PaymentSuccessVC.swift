//
//  PaymentSuccessVC.swift
//  SWADESH
//
//  Created by Sidhu on 03/02/21.
//

import UIKit

class PaymentSuccessVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        navigationItem.backButtonTitle = ""
        DispatchQueue.main.asyncAfter(deadline: .now()+5.0) {
            
            self.navigationController?.backToViewController(vc: HomeVC.self)
        }
    }

}
