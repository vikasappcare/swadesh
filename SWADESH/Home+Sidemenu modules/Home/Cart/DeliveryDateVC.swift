//
//  DeliveryDateVC.swift
//  SWADESH
//
//  Created by Vikas on 22/01/21.
//

import UIKit
import FSCalendar

class DeliveryDateVC: UIViewController {

    //MARK: -  Initializations for Calendar
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate let sampleFormatter: DateFormatter = {
        let sampleFormatter = DateFormatter()
        sampleFormatter.dateFormat = "d MMM"
        return sampleFormatter
    }()
    
    
    fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
    private var currentPageForCalendar: Date?
    
    private lazy var today: Date = {
        return Date()
    }()
    
    private func moveCurrentPage(moveUp: Bool) {
        
//        let calendar = Calendar.current
//        var dateComponents = DateComponents()
//        dateComponents.month = moveUp ? 1 : -1
//
//        self.currentPageForCalendar = calendar.date(byAdding: dateComponents, to: self.currentPageForCalendar ?? self.today)
//        calendarVw.setCurrentPage(self.currentPageForCalendar!, animated: true)
        
        let currentMonth = self.calendarVw.currentPage
        let nextMonth: Date? = gregorian.date(byAdding: .month, value: moveUp ? 1:-1, to: currentMonth, options: [])
        self.calendarVw.setCurrentPage(nextMonth!, animated: true)
        
    }

    
    @IBOutlet weak var calendarVw: FSCalendar!

    var currentDate = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCalendar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        calendarVw.layer.cornerRadius = 10.0
        calendarVw.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        calendarVw.layer.borderWidth = 0.3
//        calendarVw.calendarHeaderView.layer.cornerRadius = 10
        calendarVw.calendarHeaderView.roundCorners(corners: [.topLeft, .topRight], radius: 10)


    }

    func configureCalendar() {
                
        calendarVw.allowsSelection = true
        calendarVw.allowsMultipleSelection = false
        calendarVw.scope = .month
        calendarVw.dataSource = self
        calendarVw.delegate = self
        calendarVw.reloadData()
        calendarVw.calendarHeaderView.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.4705882353, blue: 0.6509803922, alpha: 1)
        calendarVw.calendarWeekdayView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.3)

        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        currentDate = formatter.string(from: Date())
        print("Current Date : \(currentDate)")
        SELECTED_DATE = currentDate
        
    }

    
    @IBAction func onTapCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapConfirm(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "SelectDate"), object: nil)

        SELECTED_DATE = currentDate
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickPreviouMonthBtn(_ sender: UIButton) {
        
        let currentMonth = self.calendarVw.currentPage
        let previousMonth: Date? = gregorian.date(byAdding: .month, value: -1, to: currentMonth, options: [])
        self.calendarVw.setCurrentPage(previousMonth!, animated: true)
        //self.moveCurrentPage(moveUp: false)
    }
    
    @IBAction func onClickNextMonthBtn(_ sender: UIButton) {
        
        let currentMonth = self.calendarVw.currentPage
        let nextMonth: Date? = gregorian.date(byAdding: .month, value: 1, to: currentMonth, options: [])
        self.calendarVw.setCurrentPage(nextMonth!, animated: true)
        
        //self.moveCurrentPage(moveUp: true)

    }
    
}
extension DeliveryDateVC : FSCalendarDataSource, FSCalendarDelegate , FSCalendarDelegateAppearance {
    
    //MARK: -  FSCalendar DataSource & Delegate
    
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
                            return true
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

        print("calendar did select date \(self.formatter.string(from: date))")
            
        currentDate = self.formatter.string(from: date)

            let dateAndMonth = self.sampleFormatter.string(from: date)
        print(dateAndMonth)

    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
    }
 
}
