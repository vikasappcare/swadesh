//
//  TimeSlotsTVCell.swift
//  SWADESH
//
//  Created by Vikas on 22/01/21.
//

import UIKit

class TimeSlotsTVCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var imageDisplay: UIImageView!
    @IBOutlet weak var timesLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
