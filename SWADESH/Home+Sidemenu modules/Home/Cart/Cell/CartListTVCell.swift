//
//  CartListTVCell.swift
//  SWADESH
//
//  Created by Vikas on 22/01/21.
//

import UIKit
import DropDown

class CartListTVCell: UITableViewCell {

    @IBOutlet weak var itemImgVw: UIImageView!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var deleteBtnWidth: NSLayoutConstraint! //20
    
    @IBOutlet weak var itemDescriptionLbl: UILabel!
    
    @IBOutlet weak var minBtn: UIButton!
    @IBOutlet weak var itemQuantityLbl: UILabel!
    @IBOutlet weak var maxBtn: UIButton!
    @IBOutlet weak var itemWeightBtn: UIButton!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemWeightLbl: UILabel!
    
    class var identifier: String {
        return "\(self)"
    }
    
    class var nib: UINib {
        return UINib(nibName: CartListTVCell.identifier, bundle: nil)
    }
    
    let quantityDropDown = DropDown()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadQuantityData(quantityData: [String], priceData: [String]){
        
        print("Drop down called")
        quantityDropDown.anchorView = itemWeightBtn
        quantityDropDown.bottomOffset = CGPoint(x: 0, y: itemWeightBtn.bounds.height)
        quantityDropDown.dataSource = quantityData
        quantityDropDown.selectionAction = { [weak self] (index, item) in
            print("Drop down printed")

            self?.itemWeightLbl.text = item
            self?.itemPriceLbl.text = priceData[index]
        }

    }
    
    func loadCartData(cartData: CartProductsArr?){
        
        itemNameLbl.text = cartData?.itemName ?? ""
        itemPriceLbl.text = cartData?.cartPrice ?? ""
        itemWeightLbl.text = cartData?.cartGrams ?? ""
        itemDescriptionLbl.text = ""//cartData.datumDescription
        itemQuantityLbl.text = cartData?.cartQuantity ?? ""
        let chosenImgUrl = cartData?.image ?? ""
        let chosenImgUrlString = chosenImgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)        
        itemImgVw.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
