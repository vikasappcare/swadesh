//
//  CartVC.swift
//  SWADESH
//
//  Created by Vikas on 22/01/21.
//

import UIKit

class CartVC: UIViewController {

    @IBOutlet weak var cartTV: UITableView!
    @IBOutlet weak var dateTF: UITextField!
    @IBOutlet weak var timeTF: UITextField!
    
    @IBOutlet weak var cartTVHeight: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var cartProductsList:CartProductsModel?
    var cartProductsListArrr = [CartProductsArr]()

    var productId = String()
    
    var quantity = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle(heading: Title.cart)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CartVC.dateSelected(notification:)), name: Notification.Name(rawValue: "SelectDate"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(CartVC.timeSelected(notification:)), name: Notification.Name(rawValue: "SelectTime"), object: nil)

        setUpTF()
        setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            
            cartProductsListAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("called")
    }
    
    func setUpTF() {
        dateTF.setRightView(image: #imageLiteral(resourceName: "down-arrow_black"))
        timeTF.setRightView(image: #imageLiteral(resourceName: "down-arrow_black"))
        dateTF.addTarget(self, action: #selector(myTargetFunction), for: .allEvents)
        timeTF.addTarget(self, action: #selector(myTargetFunction), for: .allEvents)
        
    }
    
    @objc func dateSelected(notification: Notification) {
        
        dateTF.text = SELECTED_DATE

    }
    
    @objc func timeSelected(notification: Notification) {
        
        timeTF.text = SELECTED_TIME

    }

    @objc func myTargetFunction(textField: UITextField) {
        
        switch textField.tag {
        case 10:
            let vc = storyboard?.instantiateViewController(identifier: "DeliveryDateVC") as! DeliveryDateVC
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        case 11:
            let vc = storyboard?.instantiateViewController(identifier: "DeliveryTimeVC") as! DeliveryTimeVC
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        default:
            break
        }
    
        print("myTargetFunction")
    }
    
    func setUpTableView() {
        
        cartTV.delegate = self
        cartTV.dataSource = self
        cartTV.register(CartListTVCell.nib, forCellReuseIdentifier: CartListTVCell.identifier)
        cartTV.estimatedRowHeight = 131
        cartTV.rowHeight = UITableView.automaticDimension
    }
    
    
    //MARK:- Selector
    
    @IBAction func onClickDateBtn(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(identifier: "DeliveryDateVC") as! DeliveryDateVC
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func onClickTimeBtn(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(identifier: "DeliveryTimeVC") as! DeliveryTimeVC
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)

    }
    @IBAction func onTapNext(_ sender: Any) {
        
        var productDict = [String:Any]()
        var productsArr = [[String:Any]]()
        var totalPriceArr = [Float]()
        var total = Float()
        
        for item in 0..<(cartProductsListArrr.count){
            
            productDict.updateValue(cartProductsListArrr[item].pid ?? "", forKey: "product_id")
            productDict.updateValue(cartProductsListArrr[item].cartGrams ?? "", forKey: "weight")
            productDict.updateValue(cartProductsListArrr[item].cartPrice ?? "", forKey: "price")
            productDict.updateValue(cartProductsListArrr[item].cartQuantity ?? "", forKey: "quantity")

            productsArr.append(productDict)

            let price = Float(cartProductsListArrr[item].cartPrice ?? "0") ?? 0
            let quantity = Float(cartProductsListArrr[item].cartQuantity ?? "0") ?? 0
            total = Float(price*quantity)
            totalPriceArr.append(Float(total))
        }
        
        let grandTotal = totalPriceArr.reduce(0, +)
        
        print("Products arr : \(productsArr)")
        print("Grand total : \(grandTotal)")
        
        if dateTF.hasText {
            if timeTF.hasText {
                
                let vc = storyboard?.instantiateViewController(identifier: "PaymentSummaryViewController") as!  PaymentSummaryViewController
                vc.itemTotal = grandTotal
                vc.productsArr = productsArr
                vc.deliveryTime = timeTF.text ?? ""
                vc.deliveryDate = dateTF.text ?? ""
                navigationController?.pushViewController(vc, animated: true)

            }else {
                
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.deliveryTime)
            }
        }else {
            
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.deliveryDate)
        }


    }
    
    @objc func onClickDeleteBtn(_ sender: UIButton){
        
        productId = cartProductsListArrr[sender.tag].pid ?? ""
        userDeleteCartAPI(productId: productId)
    }
    
}

//MARK:- TV extensions
extension CartVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartProductsListArrr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cartTV.dequeueReusableCell(withIdentifier: CartListTVCell.identifier) as! CartListTVCell
        cell.selectionStyle = .none
        
        if cartProductsListArrr.count > 0 {
            
            cell.loadCartData(cartData: cartProductsListArrr[indexPath.row])
        }
        
        cell.minBtn.tag = indexPath.row
        cell.minBtn.addTarget(self, action: #selector(onTapMinBtn(_:)), for: .touchUpInside)
        cell.maxBtn.tag = indexPath.row
        cell.maxBtn.addTarget(self, action: #selector(onTapMaxBtn(_:)), for: .touchUpInside)
        cell.itemWeightBtn.tag = indexPath.row
        cell.itemWeightBtn.addTarget(self, action: #selector(onTapWeightDropDownBtn(_:)), for: .touchUpInside)
        
        cell.deleteBtnWidth.constant = 0
        return cell
    }
    
    @objc func onTapMinBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: cartTV)
        let indexPath = cartTV.indexPathForRow(at: buttonPosition)
        let cell = cartTV.cellForRow(at: indexPath!) as! CartListTVCell
        
        if quantity > 1 {
            
            quantity = Int(cell.itemQuantityLbl.text ?? "") ?? 0
            quantity -= 1
            
            if Reachability.isConnectedToNetwork() {
                let details = cartProductsList?.data?[sender.tag]
                addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell.itemPriceLbl.text ?? "", quantityItem: String(quantity), weight: cell.itemWeightLbl.text ?? "")
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
            }
            
        }else if quantity == 1 {
            if Reachability.isConnectedToNetwork() {
                userDeleteCartAPI(productId: cartProductsListArrr[sender.tag].pid ?? "")
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
            }
        }
        
    }
    
    @objc func onTapMaxBtn(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: cartTV)
        let indexPath = cartTV.indexPathForRow(at: buttonPosition)
        let cell = cartTV.cellForRow(at: indexPath!) as! CartListTVCell
        
        quantity = Int(cell.itemQuantityLbl.text ?? "") ?? 0
        quantity += 1
        cell.itemQuantityLbl.text = String(quantity)
        
        if Reachability.isConnectedToNetwork() {
            let details = cartProductsList?.data?[sender.tag]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell.itemPriceLbl.text ?? "", quantityItem: String(quantity), weight: cell.itemWeightLbl.text ?? "")
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    @objc func onTapWeightDropDownBtn(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = cartTV.cellForRow(at: indexPath) as! CartListTVCell
        
        let details = cartProductsList?.data?[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK:-  API Service
extension CartVC {
    
    //Cart list
    func cartProductsListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: cartProductsList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CartProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.cartProductsList = responseJson
                self.cartProductsListArrr = responseJson.data ?? []
                print("Response: \(String(describing: self.cartProductsList?.data))")
                if self.cartProductsListArrr.count > 0 {
                    
                    self.cartTVHeight.constant = CGFloat(self.cartProductsListArrr.count)*120
                    DispatchQueue.main.async {
                        self.cartTV.reloadData()
                    }

                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Delete Cart
    func userDeleteCartAPI(productId:String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.pid : productId,
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: deleteCartProductURL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                
                DispatchQueue.main.async {
                    self.cartProductsListAPI()
                }

            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //update cart
    //Addto cart
    func addToCartAPI(cid: String, pid: String, price:String, quantityItem: String, weight: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityItem,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("Parameres: \(params)")
        print("UriString: \(addToCart_URL)")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                DispatchQueue.main.async {
                    self.cartProductsListAPI()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}
