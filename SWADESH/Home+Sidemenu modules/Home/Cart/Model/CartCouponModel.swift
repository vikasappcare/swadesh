//
//  CartCouponModel.swift
//  SWADESH
//
//  Created by apple on 19/04/21.
//

import Foundation

struct CartCouponModel:Codable {
    let status: Int
    let message: String
    let total: Int?
}
