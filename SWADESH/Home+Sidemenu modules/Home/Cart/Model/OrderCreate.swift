//
//  OrderCreate.swift
//  SWADESH
//
//  Created by Bhasker on 04/03/21.
//

import Foundation
// MARK: - Welcome
struct OrderCreateModel: Codable {
    let status: Int
    let message, uid, orderID: String
    let netTotal: Int

    enum CodingKeys: String, CodingKey {
        case status, message, uid
        case orderID = "order_id"
        case netTotal = "net_total"
    }
}
