//
//  DeliveryCharges.swift
//  SWADESH
//
//  Created by Bhasker on 03/03/21.
//

import Foundation
// MARK: - Welcome
struct DeliveryChargesModel: Codable {
    let status: Int
    let message: String
    let data: [DeliveryChargesArr]
}

// MARK: - Datum
struct DeliveryChargesArr: Codable{
    let id, deliveryCharges, tax, cd: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case deliveryCharges = "delivery_charges"
        case tax, cd
    }
}


// MARK: - Welcome
struct UserCreditPointsModel: Codable {
    let status: Int
    let message: String
    let data: [UserCreditPointsArr]
}

// MARK: - Datum
struct UserCreditPointsArr: Codable {
    let points: String
}


