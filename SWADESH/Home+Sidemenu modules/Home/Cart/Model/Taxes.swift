//
//  Taxes.swift
//  SWADESH
//
//  Created by Bhasker on 03/03/21.
//

import Foundation
// MARK: - Welcome
struct TaxesModel: Codable {
    let status: Int
    let message: String
    let data: [TaxesArr]
}

// MARK: - Datum
struct TaxesArr: Codable {
    let id, totalTax, cd: String

    enum CodingKeys: String, CodingKey {
        case id
        case totalTax = "total_tax"
        case cd
    }
}
