//
//  DeliveryTimeSlot.swift
//  SWADESH
//
//  Created by Bhasker on 02/03/21.
//

import Foundation
// MARK: - Welcome
struct DeliveryTimeSlotModel: Codable {
    let status: Int
    let message: String
    let data: [DeliveryTimeSlotArr]
}

// MARK: - Datum
struct DeliveryTimeSlotArr: Codable {
    let id: String
    let image: String
    let type, timings, cd: String
}
