//
//  CartProducts.swift
//  SWADESH
//
//  Created by Bhasker on 26/02/21.
//

import Foundation
// MARK: - Welcome
struct CartProductsModel: Codable {
    let status: Int
    let message: String
    let data: [CartProductsArr]?
}

// MARK: - Datum
struct CartProductsArr: Codable {
    let id, categoryID, itemName, datumDescription: String?
    let itemPrice, weight, quantity, expiryDate: String?
    let image: String?
    let status, cd, totalPrice, uid: String?
    let pid, cartlist, cartQuantity, cartGrams: String?
    let cartPrice: String?
    let priceWeight: [PriceWeightCart]?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case itemName = "item_name"
        case datumDescription = "description"
        case itemPrice = "item_price"
        case weight, quantity
        case expiryDate = "expiry_date"
        case image, status, cd
        case totalPrice = "total_price"
        case uid, pid, cartlist
        case cartQuantity = "cart_quantity"
        case cartGrams = "cart_grams"
        case cartPrice = "cart_price"
        case priceWeight = "price_weight"
    }
}

struct PriceWeightCart: Codable {
    let id, weight, price: String?
}
