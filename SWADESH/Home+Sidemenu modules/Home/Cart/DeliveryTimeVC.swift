//
//  DeliveryTimeVC.swift
//  SWADESH
//
//  Created by Vikas on 22/01/21.
//

import UIKit

class DeliveryTimeVC: UIViewController {

    @IBOutlet weak var timeListTV: UITableView!
    
    var selectedIndex: Int?
    var timeSlotModel: DeliveryTimeSlotModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTableView()
        
        if Reachability.isConnectedToNetwork() {
            
            timeSlotAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }

    }
    
    func setUpTableView() {
        timeListTV.delegate = self
        timeListTV.dataSource = self
        timeListTV.tableFooterView = UIView()
    }
    
    //MARK:- selector
    @IBAction func onTapCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapConfirm(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "SelectTime"), object: nil)

        SELECTED_TIME = timeSlotModel?.data[selectedIndex ?? 0].timings ?? ""

        dismiss(animated: true, completion: nil)
    }
}

extension DeliveryTimeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = timeListTV.dequeueReusableCell(withIdentifier: TimeSlotsTVCell.identifier) as! TimeSlotsTVCell
        
        if selectedIndex != nil {
            if selectedIndex == indexPath.row {
                cell.selectBtn.setImage(#imageLiteral(resourceName: "right_yellow"), for: .normal)
            }else{
                cell.selectBtn.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
            }
        }else{
            cell.selectBtn.setImage(#imageLiteral(resourceName: "right_gray"), for: .normal)
        }
        
        cell.timesLbl.text = String(format: "%@ - %@", timeSlotModel?.data[indexPath.row].type ?? "",timeSlotModel?.data[indexPath.row].timings ?? "")
        let chosenImgUrl = timeSlotModel?.data[indexPath.row].image ?? ""
        let chosenImgUrlString = chosenImgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.imageDisplay.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        timeListTV.deselectRow(at: indexPath, animated: true)
        
        selectedIndex = indexPath.row
        DispatchQueue.main.async {
            self.timeListTV.reloadData()
        }
        
    }
    
}
//MARK:-  API Service
extension DeliveryTimeVC {
    
    func timeSlotAPI() {
        
//        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
//
        let params = [
            APIParameters.date : SELECTED_DATE
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: getTimeSlotsURL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(DeliveryTimeSlotModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.timeSlotModel = responseJson
                print("Response: \(String(describing: self.timeSlotModel?.data))")
                if self.timeSlotModel?.data.count ?? 0 > 0 {
                    DispatchQueue.main.async {
                        self.timeListTV.reloadData()
                    }

                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
}
