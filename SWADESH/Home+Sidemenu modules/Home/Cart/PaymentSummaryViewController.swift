//
//  PaymentSummaryViewController.swift
//  SWADESH
//
//  Created by Bhasker on 01/02/21.
//

import UIKit

class PaymentSummaryViewController: UIViewController {

    var taxesModel: TaxesModel?
    var deliveryChargesModel: DeliveryChargesModel?
    var addressListModel: AddressListModel?
    var creditPointsModel:UserCreditPointsModel?
    var orderCreate:OrderCreateModel?
    var cartCouponModel:CartCouponModel?
    
    var productsArr :[[String:Any]] = []
    var itemTotal = Float()
    var deliveryCharges = String()
    var discount = String()
    var taxes = String()

    var deliveryDate = String()
    var deliveryTime = String()
    
    //MARK:-  IB Outlets
    
    @IBOutlet weak var userEmailLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var itemTotalPriceLbl: UILabel!
    @IBOutlet weak var bonusPointsLbl: UILabel!
    @IBOutlet weak var bonusPointsCheckBoxBtn: UIButton!
    @IBOutlet weak var deliveryChargesLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var taxePriceLbl: UILabel!
    @IBOutlet weak var totalAmountPayLbl: UILabel!
    @IBOutlet weak var deliveryAddressBtn: UIButton!
    @IBOutlet weak var couponCodeTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navTitle(heading: Title.paymentSummary)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navTitle(heading: Title.paymentSummary)
        
        if Reachability.isConnectedToNetwork() {
            if SELECTED_ADDRESS == "" {
                
                userAddressListAPI()

            }else {
                
                self.addressLbl.text = SELECTED_ADDRESS
            }
            getDeliveryChargesAPI()
            getUserCreditPointsAPI()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                
                self.itemTotalPriceLbl.text = String(format: "$%0.2f", self.itemTotal)
                let devCharges = Float(self.deliveryCharges) ?? 0
                let tax = Float(self.taxes ?? "") ?? 0
                let discount = Float(self.couponDiscountLbl.text ?? "") ?? 0
                
                let totalAmountPay = self.itemTotal + devCharges + tax - discount
                self.totalAmountPayLbl.text = String(format: "$%0.2f", totalAmountPay)
                
            }
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }

    }

    //MARK:-  IB Actions
    @IBAction func onClickBonusPointsBtn(_ sender: UIButton) {
    }
    
    @IBAction func couponApplyBtn(_ sender: UIButton) {
        
        if couponCodeTF.text!.isEmpty {
            showAlertMessage(vc: self, titleStr: "", messageStr: "Enter Coupon Code.")
        }else{
            if Reachability.isConnectedToNetwork(){
                userCouponApply()
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
            }
        }
    }
    
    @IBAction func onClicckDeliveryAddressBtn(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "AddressListVC") as!  AddressListVC
        vc.isFromPaymentSummary = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func onClickPaymentProceedBtn(_ sender: UIButton) {
        
        if Reachability.isConnectedToNetwork() {
            orderCreateAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
//        let vc = storyboard?.instantiateViewController(identifier: "PaymentSuccessVC") as!  PaymentSuccessVC
//        navigationController?.pushViewController(vc, animated: true)

    }
}
//MARK:-  API Service
extension PaymentSummaryViewController {
    
    //Delivery charges and taxes
    func getDeliveryChargesAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: getDeliveryChargesURL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(DeliveryChargesModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.deliveryChargesModel = responseJson
                print("Response: \(String(describing: self.deliveryChargesModel?.data))")
                if self.deliveryChargesModel?.data.count ?? 0 > 0 {
                    self.deliveryCharges = self.deliveryChargesModel?.data[0].deliveryCharges ?? ""
                    self.deliveryChargesLbl.text = String(format: "$%@", self.deliveryChargesModel?.data[0].deliveryCharges ?? "")
                    self.taxes = self.deliveryChargesModel?.data[0].tax ?? ""
                    self.taxePriceLbl.text = String(format: "$%@", self.deliveryChargesModel?.data[0].tax ?? "")
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //User referal points
    func getUserCreditPointsAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: getUserCreditPointsURL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(UserCreditPointsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.creditPointsModel = responseJson
                self.bonusPointsLbl.text = String(format: "You have %@ credits as a Referral Bonus", self.creditPointsModel?.data[0].points ?? "")
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    //Address List
    func userAddressListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addressList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(AddressListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.addressListModel = responseJson
                let addr1 = (self.addressListModel?.data?[0].street1 ?? "")+" "+(self.addressListModel?.data?[0].street2 ?? "")
                let addr2 = (self.addressListModel?.data?[0].city ?? "")+" "+(self.addressListModel?.data?[0].state ?? "")
                let addr3 = (self.addressListModel?.data?[0].postalcode ?? "")
                guard let fullName = userDefault.string(forKey: userDefaults.fullName) else {return}
                guard let email = userDefault.string(forKey: userDefaults.email) else {return}
                self.userEmailLbl.text = String(format:"%@,%@",fullName,email)
                self.addressLbl.text = addr1+" "+addr2+" "+addr3
                SELECTED_ADDRESS_ID = self.addressListModel?.data?[0].id ?? ""
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    //Coupon Apply
    func userCouponApply() {
        let params = [
            APIParameters.total : itemTotalPriceLbl.text ?? "",
            APIParameters.coupen_code: couponCodeTF.text ?? ""
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: cartCouponURL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(CartCouponModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.couponDiscountLbl.text = "\(Int(self.itemTotalPriceLbl.text ?? "") ?? 0 - (responseJson.total ?? 0))"
                
                DispatchQueue.main.async {
                    
                    self.itemTotalPriceLbl.text = String(format: "$%0.2f", self.itemTotal)
                    let devCharges = Float(self.deliveryCharges) ?? 0
                    let tax = Float(self.taxes ?? "") ?? 0
                    let discount = Float(self.couponDiscountLbl.text ?? "") ?? 0
                    
                    let totalAmountPay = self.itemTotal + devCharges + tax - discount
                    self.totalAmountPayLbl.text = String(format: "$%0.2f", totalAmountPay)
                    
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    
    //Order create
    func orderCreateAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [APIParameters.uID: uids,
                      APIParameters.product_arr: productsArr,
                      APIParameters.payment_type:"cod",
                      APIParameters.payment_status: "pending",
                      APIParameters.order_status: "pending",
                      APIParameters.order_amount:totalAmountPayLbl.text ?? "",
                      APIParameters.discount: couponDiscountLbl.text ?? "",
                      APIParameters.delivery_time : deliveryTime,
                      APIParameters.gst: taxePriceLbl.text ?? "",
                      APIParameters.address_id: SELECTED_ADDRESS_ID,
                      APIParameters.delivery_type:"delivery",
                      APIParameters.delivery_charge:deliveryChargesLbl.text ?? "",
                      APIParameters.delivery_date: deliveryDate,
                      APIParameters.credits_used: "0",
                      APIParameters.coupon_code: couponCodeTF.text ?? ""
        ] as [String : Any]
        
        print(params, "order create")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: orderCreateURL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(OrderCreateModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.orderCreate = responseJson
                print("Order create Response: \(self.orderCreate)")
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1, execute: {
                    let vc = self.storyboard?.instantiateViewController(identifier: "PaymentSuccessVC") as!  PaymentSuccessVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }) 
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
}
