//
//  SearchFilterViewController.swift
//  SWADESH
//
//  Created by Bhasker on 29/01/21.
//

import UIKit

class SearchFilterViewController: UIViewController {
    
    //MARK:-  IB Outlets
    
    @IBOutlet weak var popularSearchesCollVw: UICollectionView!
    @IBOutlet weak var frequentlyOrderderCollVw: UICollectionView!
    @IBOutlet weak var searchByCategoryCollVw: UICollectionView!
    @IBOutlet weak var searchCategoriesBar: UISearchBar!
    @IBOutlet weak var searchFilters: UIButton!
    @IBOutlet weak var searchResultTV: UITableView!
    
    //Models
    var frequentlyOrderModel: FrequentlyOrderModel?
    var categoryListModel: CategoryListModel?
    var searchListModel: SearchListModel?
    
    
    var namesArr = [String]()
    var dummyArr : [[String : Any]] = []
    var storesArr : [[String : Any]] = []
    
    //MARK:-  ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.search)
        
        namesArr = ["Milk","Apple","Bread","Brinjal","French fries","Idli batter","Mushroom","Tofu","Cheese","Butter","Thumbs Up"]
        collectionVwNibs()
        popularSearchesCollVw.delegate = self
        popularSearchesCollVw.dataSource = self
        popularSearchesCollVw.reloadData()
        
        frequentlyOrderderCollVw.delegate = self
        frequentlyOrderderCollVw.dataSource = self
        frequentlyOrderderCollVw.reloadData()
        
        searchByCategoryCollVw.delegate = self
        searchByCategoryCollVw.dataSource = self
        searchByCategoryCollVw.reloadData()
        
        searchCategoriesBar.delegate = self
        searchCategoriesBar.showsCancelButton = false
        
        searchResultTV.isHidden = true
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
        // Do any additional setup after loading the view.
    }
    
    func collectionVwNibs() {
        
        popularSearchesCollVw.register(UINib(nibName: PopularSerachesCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: PopularSerachesCollectionViewCell.identifier)
        frequentlyOrderderCollVw.register(FrequentlyOrderedCVCell.nib, forCellWithReuseIdentifier: FrequentlyOrderedCVCell.identifier)
        searchByCategoryCollVw.register(CategoriesCVCell.nib, forCellWithReuseIdentifier: CategoriesCVCell.identifier)
        
        searchResultTV.register(ItemTableViewCell.nibName, forCellReuseIdentifier: ItemTableViewCell.identifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userCategoryListAPI()
            userFrequentlyOrders()
            
            
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    @IBAction func onTapFilterButton(_ sender: UIButton) {
        
//        let filterVC = storyboard?.instantiateViewController(identifier: "BottomFilterVC") as! BottomFilterVC
//
//        present(filterVC, animated: true, completion: nil)
    }
    
    //MARK:- API
    func userSearchedList(keyName: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID : "210120103649", //uids,
            APIParameters.key : keyName
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: searchedList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            
            guard let responseJson = try? JSONDecoder().decode(SearchListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.searchListModel = responseJson
                DispatchQueue.main.async {
                    self.searchResultTV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    DispatchQueue.main.async {
                        self.searchResultTV.reloadData()
                    }
                }
            }
            
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Frequently ordered
    func userFrequentlyOrders() {
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID : "201009042842" //uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: freqOrderList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(FrequentlyOrderModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.frequentlyOrderModel = responseJson
                
                DispatchQueue.main.async {
                    self.frequentlyOrderderCollVw.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Categories list
    func userCategoryListAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: categoryList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CategoryListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.categoryListModel = responseJson
                
                DispatchQueue.main.async {
                    self.searchByCategoryCollVw.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
   
}

//MARK:-  UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension SearchFilterViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 444 {
            
            return namesArr.count
        }else if collectionView.tag == 555{
            return frequentlyOrderModel?.data?.count ?? 0
        }else {
            return categoryListModel?.data?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 444 {
            
            let popularCell = collectionView.dequeueReusableCell(withReuseIdentifier: PopularSerachesCollectionViewCell.identifier, for: indexPath) as! PopularSerachesCollectionViewCell
            
            popularCell.titlaLbl.text = namesArr[indexPath.row]
            return popularCell
            
        }else if collectionView.tag == 555{
            
            let cell = frequentlyOrderderCollVw.dequeueReusableCell(withReuseIdentifier: FrequentlyOrderedCVCell.identifier, for: indexPath) as! FrequentlyOrderedCVCell
            
            let details = frequentlyOrderModel?.data?[indexPath.row]
            cell.productImage.sd_setImage(with: URL(string: details?.image ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
            cell.productNameLbl.text = details?.itemName ?? ""
            cell.productDescriptionLbl.text = details?.datumDescription ?? ""
            cell.productPriceLbl.text = String(format: "$%@ / packet", details?.itemPrice ?? "")
            cell.productPriceLbl.highlight(searchedText: String(format: "$%@", details?.itemPrice ?? ""))
            cell.animatedViewWidth.constant = 0
            return cell
            
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoriesCVCell.identifier, for: indexPath) as! CategoriesCVCell
            
            let chosenImgUrl = categoryListModel?.data?[indexPath.item].image
            let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            cell.categoryImage.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
            
            cell.categoryName.text = categoryListModel?.data?[indexPath.row].name ?? ""
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 444 {
            
            let size: CGSize = (namesArr[indexPath.row] as AnyObject).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)])
            return CGSize(width: size.width + 30, height: 32)
            
        }else if collectionView.tag == 555{
            
            let width = (collectionView.bounds.size.width - (4 - 1) * 1) / 4
            return CGSize(width: width, height: collectionView.frame.height)
        }else{
            
            return CGSize(width: (collectionView.frame.size.width / 3) - 20, height: 130)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 555 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "FrequentlyOrderedViewController") as! FrequentlyOrderedViewController
            navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView.tag == 444 {
            
        }else{
            let categoryVC = storyboard?.instantiateViewController(identifier: "SingleCategoryVC") as! SingleCategoryVC
            categoryIDString = categoryListModel?.data?[indexPath.row].id ?? ""
            navigationController?.pushViewController(categoryVC, animated: true)
        }
        
    }
}

//MARK:-  Tableview delegates and data source
extension SearchFilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchCell = searchResultTV.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier) as! ItemTableViewCell
        
        searchCell.itemNameLbl.text = searchListModel?.data?[indexPath.row].itemName
        searchCell.itemDescriptionLbl.text = searchListModel?.data?[indexPath.row].itemName
        searchCell.priceLbl.text = searchListModel?.data?[indexPath.row].itemPrice ?? ""
        
        let chosenImgUrl = searchListModel?.data?[indexPath.item].image
        let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        searchCell.itemImgVw.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: ""))
        
        searchCell.animationVwDynamicWidth.constant = 0
        
        return searchCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: -  UISearchBarDelegate Delegate  Methods
extension SearchFilterViewController : UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        searchBar.text = nil
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        self.searchCategoriesBar.resignFirstResponder()
        print("cancel clicked")
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = #colorLiteral(red: 0.1882352941, green: 0.6509803922, blue: 0.6, alpha: 1)
        searchCategoriesBar.showsCancelButton = true
        searchResultTV.isHidden = false
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
        print(searchText, "searchetext")
        
        if Reachability.isConnectedToNetwork() {
            self.userSearchedList(keyName: searchText)
            //self.searchResultTV.reloadData()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        self.searchCategoriesBar.resignFirstResponder()
        searchResultTV.isHidden = true
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        
        searchCategoriesBar.showsCancelButton = false
    }
    
}
