//
//  FrequentlyOrderedViewController.swift
//  SWADESH
//
//  Created by Bhasker on 29/01/21.
//

import UIKit

class FrequentlyOrderedViewController: UIViewController {
    
    @IBOutlet weak var freqOrderListTV: UITableView!
    
    //Model
    var frequentlyOrderModel: FrequentlyOrderModel?
    
    var selectAnimation = Int()
    var quantity = Int()
    var dummyArr = [Int]()
    var isAnimationSelected = Bool()
    
    //MARK:-  ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.frquentlyOrdered)
        
        setupTableview()
        
        selectAnimation = -1
        isAnimationSelected = false
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
        // Do any additional setup after loading the view.
    }
    
    func setupTableview() {
        
        freqOrderListTV.delegate = self
        freqOrderListTV.dataSource = self
        freqOrderListTV.reloadData()
        freqOrderListTV.register(ItemTableViewCell.nibName, forCellReuseIdentifier: ItemTableViewCell.identifier)
        freqOrderListTV.estimatedRowHeight = freqOrderListTV.rowHeight
        freqOrderListTV.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userFrequentlyOrders()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    //MARK:- CV Cell quantity action
    @objc func onTapMinusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: freqOrderListTV)
        let indexPath = freqOrderListTV.indexPathForRow(at: buttonPosition)
        let cell = freqOrderListTV.cellForRow(at: indexPath!) as! ItemTableViewCell
        
        if quantity > 1 {
            
            quantity = Int(cell.quantityLbl.text ?? "") ?? 0
            quantity -= 1
            cell.quantityLbl.text = String(quantity)
        }else if quantity == 1 {
            
        }
        
    }
    
    @objc func onTapPlusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: freqOrderListTV)
        let indexPath = freqOrderListTV.indexPathForRow(at: buttonPosition)
        let cell = freqOrderListTV.cellForRow(at: indexPath!) as! ItemTableViewCell
        quantity = Int(cell.quantityLbl.text ?? "") ?? 0
        quantity += 1
        cell.quantityLbl.text = String(quantity)
        
    }
    
    //MARK:- API
    func userFrequentlyOrders() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID : uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: freqOrderList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(FrequentlyOrderModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.frequentlyOrderModel = responseJson
                
                for _ in 0..<self.frequentlyOrderModel!.data!.count{
                    
                    self.dummyArr.append(0)
                }
                
                DispatchQueue.main.async {
                    self.freqOrderListTV.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Addto cart
    func addToCartAPI(cid: String, pid: String, price:String, quantityItem: String, weight: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityItem,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("Parameres: \(params)")
        print("UriString: \(addToCart_URL)")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.userFrequentlyOrders()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}


//MARK:-  UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension FrequentlyOrderedViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frequentlyOrderModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = freqOrderListTV.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier) as! ItemTableViewCell
        
        let productDetails = frequentlyOrderModel?.data?[indexPath.row]
        
        cell.priceLbl.text = productDetails?.itemPrice ?? ""
        cell.itemImgVw.sd_setImage(with: URL(string: productDetails?.image ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
        cell.itemNameLbl.text = productDetails?.itemName ?? ""
        cell.itemDescriptionLbl.text = productDetails?.datumDescription ?? ""
        cell.quantityLbl.text = productDetails?.itemPrice ?? ""
        cell.itemWeightLbl.text = productDetails?.weight ?? ""
        
        if dummyArr[indexPath.row] == 1 {
            UIView.animate(withDuration: 0.5) {
                cell.animationVwDynamicWidth.constant = 40
                cell.spinnerImgVw.image = UIImage(named: "Radio  Check")
            }
        }else {
            cell.animationVwDynamicWidth.constant = 0
        }
        
        if selectAnimation == indexPath.row {
            
            cell.startSpin()
        }else {
            cell.stopSpin()
        }
        
        if frequentlyOrderModel?.data?[indexPath.row].cartlist == "No" {
            cell.animationVwDynamicWidth.constant = 0
            cell.checkMarkImgVw.image = UIImage(named: "")
            cell.spinnerImgVw.image = UIImage(named: "")
            cell.quantityLbl.text = "1"
            cell.priceLbl.text = frequentlyOrderModel?.data?[indexPath.row].itemPrice ?? ""
            
        }else{
            cell.startSpin()
            cell.stopSpin()
            cell.animationVwDynamicWidth.constant = 40
            cell.quantityLbl.text = frequentlyOrderModel?.data?[indexPath.row].cartQuantity ?? ""
            cell.itemWeightLbl.text = frequentlyOrderModel?.data?[indexPath.row].cartGrams ?? ""
            cell.priceLbl.text = frequentlyOrderModel?.data?[indexPath.row].cartPrice ?? ""
            print("yse cart values", frequentlyOrderModel?.data?[indexPath.row].cartQuantity)
        }
        
        cell.addToCartBtn.tag = indexPath.row
        cell.itemWeightBtn.tag = indexPath.row
        cell.quantityDropDown.tag = indexPath.row
        cell.quantityDecrementBtn.tag = indexPath.row
        cell.quantityIncrementBtn.tag = indexPath.row
        
        
        cell.addToCartBtn.addTarget(self, action: #selector(onTapAddtoCart(_:)), for: .touchUpInside)
        cell.itemWeightBtn.addTarget(self, action: #selector(onTapWeightDropDown(_:)), for: .touchUpInside)
        cell.quantityIncrementBtn.addTarget(self, action: #selector(onTapPlusBtn(_:)), for: .touchUpInside)
        cell.quantityDecrementBtn.addTarget(self, action: #selector(onTapMinusBtn(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    @objc func onTapAddtoCart(_ sender: UIButton) {
        
        dummyArr[sender.tag] = 1
        selectAnimation = sender.tag
        isAnimationSelected = true
        
        if Reachability.isConnectedToNetwork() {
            let buttonPosition = sender.convert(CGPoint.zero, to: freqOrderListTV)
            let indexPath = freqOrderListTV.indexPathForRow(at:buttonPosition)
            let cell = freqOrderListTV.cellForRow(at: indexPath!) as? ItemTableViewCell
            
            let details = frequentlyOrderModel?.data?[sender.tag]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell?.priceLbl.text ?? "", quantityItem: String(quantity), weight: cell?.itemWeightLbl.text ?? "")
            
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    @objc func onTapWeightDropDown(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = freqOrderListTV.cellForRow(at: indexPath) as! ItemTableViewCell
        
        let details = frequentlyOrderModel?.data?[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
