//
//  BottomFilterVC.swift
//  SWADESH
//
//  Created by Sidhu on 03/02/21.
//

import UIKit

class BottomFilterVC: UIViewController {

    @IBOutlet weak var filterCV: UICollectionView!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    
    let namesArr = ["Milk","Apple","Bread","Brinjal","French fries","Idli batter","Mushroom","Tofu","Cheese","Butter","Thumbs Up"]
    
    var selectedIndex = [IndexPath]()
    var selectedData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpCollectionView()
    }
    
    private func setUpCollectionView() {
        
        filterCV.delegate = self
        filterCV.dataSource = self
        
    }
    
    //MARK:- Selector
    //next page
    @IBAction func onTapApplyChanges(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SearchFilterViewController") as! SearchFilterViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //cancel
    @IBAction func onTapCancel(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension BottomFilterVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return namesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = filterCV.dequeueReusableCell(withReuseIdentifier: BottomFilterCVCell.identifier, for: indexPath) as! BottomFilterCVCell
        cell.nameLbl.text = namesArr[indexPath.row]
        
        if selectedIndex.contains(indexPath) {
            cell.displayView.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }
        cell.layoutSubviews()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGSize = (namesArr[indexPath.row] as AnyObject).size(withAttributes: [NSAttributedString.Key.font: UIFont.init(name: Fonts.regular, size: 16)!])
        return CGSize(width: size.width + 10, height: 40)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        filterCV.deselectItem(at: indexPath, animated: true)
        
        print(indexPath.item)
        
        let strValue = namesArr[indexPath.item]
        
        if selectedIndex.contains(indexPath) {
            selectedIndex = selectedIndex.filter {$0 != indexPath}
            selectedData = selectedData.filter {$0 != strValue}
        }else{
            selectedIndex.append(indexPath)
            selectedData.append(strValue)
        }
        
        DispatchQueue.main.async {
            self.filterCV.reloadData()
        }
        
    }
    
}
