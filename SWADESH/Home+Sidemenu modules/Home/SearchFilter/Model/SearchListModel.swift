//
//  SearchListModel.swift
//  SWADESH
//
//  Created by apple on 05/04/21.
//

import Foundation

struct SearchListModel: Codable {
    let status: Int
    let message: String
    let data: [DataSearchList]?
}

// MARK: - Datum
struct DataSearchList: Codable {
    let id, categoryID, itemName, datumDescription: String?
    let itemPrice, weight, quantity, expiryDate: String?
    let image: String?
    let status, cd, wishlist: String?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case itemName = "item_name"
        case datumDescription = "description"
        case itemPrice = "item_price"
        case weight, quantity
        case expiryDate = "expiry_date"
        case image, status, cd, wishlist
    }
}
