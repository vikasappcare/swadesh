//
//  FrequentlyOrderModel.swift
//  SWADESH
//
//  Created by apple on 30/03/21.
//

import Foundation

struct FrequentlyOrderModel: Codable {
    let status: Int
    let message: String
    let data: [DataFrequentlyOrder]?
}

struct DataFrequentlyOrder: Codable {
    let id, categoryID, itemName, datumDescription: String?
    let itemPrice, weight, quantity, expiryDate: String?
    let image: String?
    let status, cd, cartlist, cartQuantity: String?
    let cartGrams, cartPrice: String?
    let priceWeight: [DataOrdersPriceWeight]?
    let wishlist: String?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case itemName = "item_name"
        case datumDescription = "description"
        case itemPrice = "item_price"
        case weight, quantity
        case expiryDate = "expiry_date"
        case image, status, cd, cartlist
        case cartQuantity = "cart_quantity"
        case cartGrams = "cart_grams"
        case cartPrice = "cart_price"
        case priceWeight = "price_weight"
        case wishlist
    }
}

// MARK: - PriceWeight
struct DataOrdersPriceWeight: Codable {
    let id, weight, price: String?
}
