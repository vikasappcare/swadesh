//
//  FrequentlyOrderedCVCell.swift
//  SWADESH
//
//  Created by apple on 15/03/21.
//

import UIKit
import DropDown

class FrequentlyOrderedCVCell: UICollectionViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    class var nib: UINib {
        return UINib(nibName: FrequentlyOrderedCVCell.identifier, bundle: nil)
    }
    
    
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productDescriptionLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var productWeightBtn: UIButton!
    @IBOutlet weak var productWeightLbl: UILabel!
    @IBOutlet weak var minBtn: UIButton!
    @IBOutlet weak var maxBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var addtoCartBtn: UIButton!
    @IBOutlet weak var spinnerImage: UIImageView!
    @IBOutlet weak var checkMarkImage: UIImageView!
    
    @IBOutlet weak var animatedViewWidth: NSLayoutConstraint! //20
    
    var animating = false
    let quantityDropDown = DropDown()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        productImage.contentMode = .scaleAspectFit
    }
    
    func loadQuantityData(quantityData: [String], priceData: [String]){
        
        print("Drop down called")
        quantityDropDown.anchorView = productWeightBtn
        quantityDropDown.bottomOffset = CGPoint(x: 0, y: productWeightBtn.bounds.height)
        quantityDropDown.dataSource = quantityData
        quantityDropDown.selectionAction = { [weak self] (index, item) in
            print("Drop down printed")

            self?.productWeightLbl.text = item
            self?.productPriceLbl.text = priceData[index]
        }

    }
    
    //MARK: -  Animation
    
func spin(with options: UIView.AnimationOptions) {
        
        UIView.animate(withDuration: 1.2, delay: 0, options: options, animations: {
            
            self.spinnerImage.image = UIImage(named: "Spinner")

            self.spinnerImage.transform = self.spinnerImage.transform.rotated(by: .pi / 2)
            
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                
                self.stopSpin()
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    
                    self.checkMarkImage.image = UIImage(named: "Radio  Check")

                }
            }
            
        }) { finished in
            
            if finished {
                print("Stopped images vikas")
                self.checkMarkImage.image = UIImage(named: "Radio Check")

                if self.animating {
                    
                    self.spin(with: .curveLinear)
                    
                } else if options != .curveEaseOut {
                    
                    self.spin(with: .curveEaseOut)
                }
            }
        }
    }
    
    @objc func startSpin() {
        
        if !animating {
            
            animating = true
            spin(with: .curveEaseIn)
        }
    }
    
    func stopSpin() {
        
        animating = false
        print("Stopped images vikas1")
    }

}
