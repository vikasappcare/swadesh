//
//  BottomFilterCVCell.swift
//  SWADESH
//
//  Created by Sidhu on 03/02/21.
//

import UIKit

class BottomFilterCVCell: UICollectionViewCell {
    
    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var displayView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
