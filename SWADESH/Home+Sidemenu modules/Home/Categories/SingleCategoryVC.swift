//
//  SingleCategoryVC.swift
//  SWADESH
//
//  Created by Vikas on 22/01/21.
//

import UIKit
import DropDown

class SingleCategoryVC: UIViewController {
    
    @IBOutlet weak var categoryTV: UITableView!
    @IBOutlet weak var totalSubCatgryLbl: UILabel!
    
    //Model
    var singleCategoryListModel: SingleCategoryListModel?
    
    var navTitle:String?
    
    var selectAnimation = Int()
    var quantity = 1
    var dummyArr = [Int]()
    var isAnimationSelected = Bool()
    
    let dropDown = DropDown()
    
    //barbutton item
    let badgeCount = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: navTitle ?? "Category Details")
        setUpTableView()
        
        badgeCount.frame = CGRect(x: 15, y: -05, width: 16, height: 16)
        badgeCount.layer.borderColor = UIColor.clear.cgColor
        badgeCount.layer.borderWidth = 2
        badgeCount.layer.cornerRadius = badgeCount.bounds.height / 2
        badgeCount.textAlignment = .center
        badgeCount.layer.masksToBounds = true
        badgeCount.textColor = .white
        badgeCount.font = badgeCount.font.withSize(12)
        badgeCount.backgroundColor = .red
        badgeCount.text = "0"
        
        
        let cartImage = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        cartImage.setBackgroundImage(UIImage(named: "cart_gray"), for: .normal)
        cartImage.addTarget(self, action: #selector(onTapCart(_:)), for: .touchUpInside)
        cartImage.addSubview(badgeCount)
        
        
        let cartImgBtn = UIBarButtonItem(customView: cartImage)

        let searchImage = UIBarButtonItem(image: UIImage(named: "Mask Group 49")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(onTapSearch(_:)))
        
        navigationItem.rightBarButtonItems = [cartImgBtn,searchImage]
        
        selectAnimation = -1
        isAnimationSelected = false
        
    }
    
    //MARK:-  Nav bar Item Actions
    @objc func onTapSearch(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "SearchFilterViewController") as! SearchFilterViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onTapCart(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userSingleCategoryListAPI(cid: categoryIDString)
            cartProductsListAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    //MARK:- Selector
    func setUpTableView() {
        
        categoryTV.delegate = self
        categoryTV.dataSource = self
        categoryTV.register(ItemTableViewCell.nibName, forCellReuseIdentifier: ItemTableViewCell.identifier)
        categoryTV.estimatedRowHeight = categoryTV.rowHeight
        categoryTV.rowHeight = UITableView.automaticDimension
        self.categoryTV.reloadData()
        self.categoryTV.tableFooterView = UIView()
        
    }
    
    @objc func onTapMinusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: categoryTV)
        let indexPath = categoryTV.indexPathForRow(at: buttonPosition)
        let cell = categoryTV.cellForRow(at: indexPath!) as! ItemTableViewCell
        
        if quantity > 1 {
            
            quantity = Int(cell.quantityLbl.text ?? "") ?? 0
            quantity -= 1
            cell.quantityLbl.text = String(quantity)
        }else if quantity == 1 {
            
        }
        
    }
    
    @objc func onTapPlusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: categoryTV)
        let indexPath = categoryTV.indexPathForRow(at: buttonPosition)
        let cell = categoryTV.cellForRow(at: indexPath!) as! ItemTableViewCell
        quantity = Int(cell.quantityLbl.text ?? "") ?? 0
        quantity += 1
        cell.quantityLbl.text = String(quantity)
        
    }
    
    
    
    //MARK:- API
    func userSingleCategoryListAPI(cid: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.category_id: cid
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: singleCategoryList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SingleCategoryListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.singleCategoryListModel = responseJson
                
                for _ in 0..<self.singleCategoryListModel!.data!.count{
                    
                    self.dummyArr.append(0)
                }
                
                DispatchQueue.main.async {
                    self.totalSubCatgryLbl.text = "\(responseJson.data?.count ?? 0) Items"
                    self.categoryTV.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    //Addto cart
    func addToCartAPI(cid: String, pid: String, price:String, quantityItem: String, weight: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityItem,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("Parameres: \(params)")
        print("UriString: \(addToCart_URL)")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.userSingleCategoryListAPI(cid: categoryIDString)
                    self.cartProductsListAPI()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Cart list
    func cartProductsListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: cartProductsList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CartProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                DispatchQueue.main.async {
                    self.badgeCount.text = "\(responseJson.data?.count ?? 0)"
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}

extension SingleCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return singleCategoryListModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = categoryTV.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier) as! ItemTableViewCell
        
        let productDetails = singleCategoryListModel?.data?[indexPath.row]
        
        cell.itemImgVw.sd_setImage(with: URL(string: productDetails?.image ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
        cell.itemNameLbl.text = productDetails?.itemName ?? ""
        cell.itemDescriptionLbl.text = productDetails?.datumDescription ?? ""
        cell.priceLbl.text = productDetails?.itemPrice ?? ""
        
        
        
        if dummyArr[indexPath.row] == 1 {
            UIView.animate(withDuration: 0.5) {
                cell.animationVwDynamicWidth.constant = 40
                cell.spinnerImgVw.image = UIImage(named: "Radio  Check")
            }
        }else {
            cell.animationVwDynamicWidth.constant = 0
        }
        
        if selectAnimation == indexPath.row {
            
            cell.startSpin()
        }else {
            cell.stopSpin()
        }
        
        if singleCategoryListModel?.data?[indexPath.row].cartlist == "No" {
            cell.animationVwDynamicWidth.constant = 0
            cell.checkMarkImgVw.image = UIImage(named: "")
            cell.spinnerImgVw.image = UIImage(named: "")
            cell.quantityLbl.text = "1"
            cell.priceLbl.text = singleCategoryListModel?.data?[indexPath.row].itemPrice ?? ""
            
        }else{
            cell.startSpin()
            cell.stopSpin()
            cell.animationVwDynamicWidth.constant = 40
            cell.quantityLbl.text = singleCategoryListModel?.data?[indexPath.row].cartQuantity ?? ""
            cell.itemWeightLbl.text = singleCategoryListModel?.data?[indexPath.row].cartGrams ?? ""
            cell.priceLbl.text = singleCategoryListModel?.data?[indexPath.row].cartPrice ?? ""
            
        }
        
        cell.addToCartBtn.tag = indexPath.row
        cell.itemWeightBtn.tag = indexPath.row
        cell.quantityDropDown.tag = indexPath.row
        cell.quantityDecrementBtn.tag = indexPath.row
        cell.quantityIncrementBtn.tag = indexPath.row
        
        
        cell.addToCartBtn.addTarget(self, action: #selector(onTapAddtoCart(_:)), for: .touchUpInside)
        cell.itemWeightBtn.addTarget(self, action: #selector(onTapWeightDropDown(_:)), for: .touchUpInside)
        cell.quantityIncrementBtn.addTarget(self, action: #selector(onTapPlusBtn(_:)), for: .touchUpInside)
        cell.quantityDecrementBtn.addTarget(self, action: #selector(onTapMinusBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func onTapAddtoCart(_ sender: UIButton) {
        
        dummyArr[sender.tag] = 1
        selectAnimation = sender.tag
        isAnimationSelected = true
        
        if Reachability.isConnectedToNetwork() {
            let buttonPosition = sender.convert(CGPoint.zero, to: categoryTV)
            let indexPath = categoryTV.indexPathForRow(at:buttonPosition)
            let cell = categoryTV.cellForRow(at: indexPath!) as? ItemTableViewCell
            
            let details = singleCategoryListModel?.data?[sender.tag]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell?.priceLbl.text ?? "", quantityItem: String(quantity), weight: cell?.itemWeightLbl.text ?? "")
            
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    @objc func onTapWeightDropDown(_ sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = categoryTV.cellForRow(at: indexPath) as! ItemTableViewCell
        
        let details = singleCategoryListModel?.data?[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        categoryTV.deselectRow(at: indexPath, animated: true)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SingleProductDetailsVC") as! SingleProductDetailsVC
        productIDString = singleCategoryListModel?.data?[indexPath.row].id ?? ""
        print(productIDString, "selected product ID")
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
