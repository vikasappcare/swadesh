//
//  HomeQuantity.swift
//  SWADESH
//
//  Created by Bhasker on 19/02/21.
//

import Foundation
// MARK: - Welcome
struct HomeQuantityModel: Codable {
    let status: Int
    let message: String
    let data: [HomeQuantityArr]
}

// MARK: - Datum
struct HomeQuantityArr: Codable {
    let id, quantity: String
}
