//
//  SingleCategoryListModel.swift
//  SWADESH
//
//  Created by Vikas on 29/01/21.
//

import Foundation

struct SingleCategoryListModel: Codable {
    let status: Int
    let message: String
    let data: [DataSingleCat]?
}

// MARK: - Datum
struct DataSingleCat: Codable {
    let id, categoryID, itemName, datumDescription: String?
    let itemPrice, weight, quantity, expiryDate: String?
    let image: String?
    let status, cd, cartlist, cartQuantity: String?
    let cartGrams, cartPrice, wishlist: String?
    let priceWeight: [PriceWeightSingleCat]?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case itemName = "item_name"
        case datumDescription = "description"
        case itemPrice = "item_price"
        case weight, quantity
        case expiryDate = "expiry_date"
        case image, status, cd, cartlist
        case cartQuantity = "cart_quantity"
        case cartGrams = "cart_grams"
        case cartPrice = "cart_price"
        case wishlist
        case priceWeight = "price_weight"
    }
}

// MARK: - PriceWeight
struct PriceWeightSingleCat: Codable {
    let id, weight, price: String?
}

