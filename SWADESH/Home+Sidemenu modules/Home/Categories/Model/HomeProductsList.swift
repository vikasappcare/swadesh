//
//  HomeProductsList.swift
//  SWADESH
//
//  Created by Bhasker on 23/02/21.
//

import Foundation
// MARK: - Welcome

struct HomeProductsListModel: Codable {
    
    let status: Int
    let message: String
    let data: [HomeProductsListArr]
}

// MARK: - Datum
struct HomeProductsListArr: Codable {
    let id, categoryID, itemName, datumDescription: String?
    let itemPrice, weight, quantity, expiryDate: String?
    let image: String?
    let status, cd, cartlist, cartQuantity: String?
    let cartGrams, wishlist: String?
    let priceWeight: [DataPriceWeight]?
    let cartPrice: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case itemName = "item_name"
        case datumDescription = "description"
        case itemPrice = "item_price"
        case weight, quantity
        case expiryDate = "expiry_date"
        case image, status, cd, cartlist
        case cartQuantity = "cart_quantity"
        case cartGrams = "cart_grams"
        case wishlist
        case priceWeight = "price_weight"
        case cartPrice = "cart_price"
    }
}

// MARK: - PriceWeight
struct DataPriceWeight: Codable {
    let id, weight, price: String?
}
