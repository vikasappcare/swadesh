//
//  HomeBanner.swift
//  SWADESH
//
//  Created by Bhasker on 19/02/21.
//

import Foundation
// MARK: - Welcome
struct HomeBannerModel: Codable {
    let status: Int
    let message: String
    let data: [HomeBannerArr]
}

// MARK: - Datum
struct HomeBannerArr: Codable {
    let id: String
    let image: String
}
