//
//  HomeNewArrivals.swift
//  SWADESH
//
//  Created by Bhasker on 19/02/21.
//

import Foundation
// MARK: - Welcome
struct HomeNewArrivalsModel: Codable {
    let status: Int
    let message: String
    let data: [HomeNewArrivalsArr]
}

// MARK: - Datum
struct HomeNewArrivalsArr: Codable {
    let id, productName: String

    enum CodingKeys: String, CodingKey {
        case id
        case productName = "ProductName"
    }
}
