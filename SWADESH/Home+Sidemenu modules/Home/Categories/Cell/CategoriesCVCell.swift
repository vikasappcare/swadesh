//
//  CategoriesCVCell.swift
//  SWADESH
//
//  Created by Vikas on 20/01/21.
//

import UIKit

class CategoriesCVCell: UICollectionViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    class var nib: UINib {
        return UINib(nibName: CategoriesCVCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        categoryImage.contentMode = .scaleAspectFill
        
        
        // Initialization code
    }
    
    func setUpCell(image: String, name: String) {
        categoryImage.image = UIImage(named: image)
        categoryName.text = name
    }

}
