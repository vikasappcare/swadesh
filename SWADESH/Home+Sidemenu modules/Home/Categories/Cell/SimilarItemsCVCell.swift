//
//  SimilarItemsCVCell.swift
//  SWADESH
//
//  Created by Sidhu on 03/02/21.
//

import UIKit
import DropDown

class SimilarItemsCVCell: UICollectionViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    class var nib: UINib {
        return UINib(nibName: SimilarItemsCVCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var productImage : UIImageView!
    @IBOutlet weak var productNameLbl : UILabel!
    @IBOutlet weak var productDescriptionLbl: UILabel!
    @IBOutlet weak var productPriceLbl : UILabel!
    @IBOutlet weak var minBtn: UIButton!
    @IBOutlet weak var maxBtn: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var weightBtn: UIButton!
    @IBOutlet weak var addtoCartBtn: UIButton!
    @IBOutlet weak var spinnerImage: UIImageView!
    @IBOutlet weak var checkMarkImage: UIImageView!
    
    @IBOutlet weak var animatedViewWidth: NSLayoutConstraint! //20
    
    
    var animating = false
    let quantityDropDown = DropDown()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        productImage.contentMode = .scaleAspectFit
    }

    func loadQuantityData(quantityData: [String], priceData: [String]){
        
        print("Drop down called")
        quantityDropDown.anchorView = weightBtn
        quantityDropDown.bottomOffset = CGPoint(x: 0, y: weightBtn.bounds.height)
        quantityDropDown.dataSource = quantityData
        quantityDropDown.selectionAction = { [weak self] (index, item) in
            print("Drop down printed")

            self?.weightLbl.text = item
            self?.productPriceLbl.text = priceData[index]
        }

    }
    
    //MARK: -  Animation
    
func spin(with options: UIView.AnimationOptions) {
        
        UIView.animate(withDuration: 1.2, delay: 0, options: options, animations: {
            
            self.spinnerImage.image = UIImage(named: "Spinner")

            self.spinnerImage.transform = self.spinnerImage.transform.rotated(by: .pi / 2)
            
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                
                self.stopSpin()
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    
//                    self.checkMarkImgVw.image = UIImage(named: "Radio  Check")

                }
            }
            
        }) { finished in
            
            if finished {
                
                self.checkMarkImage.image = UIImage(named: "Radio  Check")

                if self.animating {
                    
                    self.spin(with: .curveLinear)
                    
                } else if options != .curveEaseOut {
                    
                    self.spin(with: .curveEaseOut)
                }
            }
        }
    }
    
    @objc func startSpin() {
        
        if !animating {
            
            animating = true
            spin(with: .curveEaseIn)
        }
    }
    
    func stopSpin() {
        
        animating = false
    }
    
}
