//
//  SimilarProductListVC.swift
//  SWADESH
//
//  Created by apple on 22/03/21.
//

import UIKit

class SimilarProductListVC: UIViewController {

    @IBOutlet weak var similarProductTV: UITableView!
    
    //Model
    var similarProductsModel: SimilarProductsModel?
    
    var productIDString = ""
    var selectAnimation = Int()
    var quantity = Int()
    var dummyArr = [Int]()
    var isAnimationSelected = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle(heading: Title.similarItems)
        setupTableView()
        
        selectAnimation = -1
        isAnimationSelected = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            similarProductsAPI(productID: productIDString)
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    func setupTableView() {
        similarProductTV.delegate = self
        similarProductTV.dataSource = self
        similarProductTV.register(ItemTableViewCell.nibName, forCellReuseIdentifier: ItemTableViewCell.identifier)
        similarProductTV.estimatedRowHeight = similarProductTV.rowHeight
        similarProductTV.rowHeight = UITableView.automaticDimension
        
    }
    
    //MARK:- Cell Actions
    @objc func onTapMinusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: similarProductTV)
        let indexPath = similarProductTV.indexPathForRow(at: buttonPosition)
        let cell = similarProductTV.cellForRow(at: indexPath!) as! ItemTableViewCell
        
        if quantity > 1 {
            
            quantity = Int(cell.quantityLbl.text ?? "") ?? 0
            quantity -= 1
            cell.quantityLbl.text = String(quantity)
        }else if quantity == 1 {
            
        }
        
    }
    
    @objc func onTapPlusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: similarProductTV)
        let indexPath = similarProductTV.indexPathForRow(at: buttonPosition)
        let cell = similarProductTV.cellForRow(at: indexPath!) as! ItemTableViewCell
        quantity = Int(cell.quantityLbl.text ?? "") ?? 0
        quantity += 1
        cell.quantityLbl.text = String(quantity)
        
    }
    
    @objc func onTapAddtoCart(_ sender: UIButton) {
        
        dummyArr[sender.tag] = 1
        selectAnimation = sender.tag
        isAnimationSelected = true
        
        if Reachability.isConnectedToNetwork() {
            let buttonPosition = sender.convert(CGPoint.zero, to: similarProductTV)
            let indexPath = similarProductTV.indexPathForRow(at:buttonPosition)
            let cell = similarProductTV.cellForRow(at: indexPath!) as? ItemTableViewCell
            
            let details = similarProductsModel?.data?[sender.tag]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell?.priceLbl.text ?? "", quantityItem: String(quantity), weight: cell?.itemWeightLbl.text ?? "")
            
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    @objc func onTapWeightDropDown(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = similarProductTV.cellForRow(at: indexPath) as! ItemTableViewCell
        
        let details = similarProductsModel?.data?[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
    }
    
    
    //MARK:-  Similar products
    func similarProductsAPI(productID:String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.product_id : productID,
            APIParameters.uID: uids
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: similarProduct_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SimilarProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                self.similarProductsModel = responseJson
                
                for _ in 0..<self.similarProductsModel!.data!.count{
                    
                    self.dummyArr.append(0)
                }
                
                DispatchQueue.main.async {
                    self.similarProductTV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    //Addto cart
    func addToCartAPI(cid: String, pid: String, price:String, quantityItem: String, weight: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityItem,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("Parameres: \(params)")
        print("UriString: \(addToCart_URL)")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.similarProductsAPI(productID: self.productIDString)
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}

extension SimilarProductListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return similarProductsModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = similarProductTV.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier) as! ItemTableViewCell
        
        let productDetails = similarProductsModel?.data?[indexPath.row]
        
        cell.itemImgVw.sd_setImage(with: URL(string: productDetails?.image ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
        cell.itemNameLbl.text = productDetails?.itemName ?? ""
        cell.itemDescriptionLbl.text = productDetails?.datumDescription ?? ""
        cell.priceLbl.text = productDetails?.itemPrice ?? ""
        
        if dummyArr[indexPath.row] == 1 {
            UIView.animate(withDuration: 0.5) {
                cell.animationVwDynamicWidth.constant = 40
                cell.spinnerImgVw.image = UIImage(named: "Radio  Check")
            }
        }else {
            cell.animationVwDynamicWidth.constant = 0
        }
        
        if selectAnimation == indexPath.row {
            
            cell.startSpin()
        }else {
            cell.stopSpin()
        }
        
        if similarProductsModel?.data?[indexPath.row].cartlist == "No" {
            cell.animationVwDynamicWidth.constant = 0
            cell.checkMarkImgVw.image = UIImage(named: "")
            cell.spinnerImgVw.image = UIImage(named: "")
            cell.quantityLbl.text = "1"
            cell.priceLbl.text = similarProductsModel?.data?[indexPath.row].itemPrice ?? ""
            
        }else{
            cell.startSpin()
            cell.stopSpin()
            cell.animationVwDynamicWidth.constant = 40
            cell.quantityLbl.text = similarProductsModel?.data?[indexPath.row].cartQuantity ?? ""
            cell.itemWeightLbl.text = similarProductsModel?.data?[indexPath.row].cartGrams ?? ""
            cell.priceLbl.text = similarProductsModel?.data?[indexPath.row].cartPrice ?? ""
            
        }
        
        cell.addToCartBtn.tag = indexPath.row
        cell.itemWeightBtn.tag = indexPath.row
        cell.quantityDropDown.tag = indexPath.row
        cell.quantityDecrementBtn.tag = indexPath.row
        cell.quantityIncrementBtn.tag = indexPath.row
        
        
        cell.addToCartBtn.addTarget(self, action: #selector(onTapAddtoCart(_:)), for: .touchUpInside)
        cell.itemWeightBtn.addTarget(self, action: #selector(onTapWeightDropDown(_:)), for: .touchUpInside)
        cell.quantityIncrementBtn.addTarget(self, action: #selector(onTapPlusBtn(_:)), for: .touchUpInside)
        cell.quantityDecrementBtn.addTarget(self, action: #selector(onTapMinusBtn(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
