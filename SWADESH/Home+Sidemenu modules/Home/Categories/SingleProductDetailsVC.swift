//
//  SingleProductDetailsVC.swift
//  SWADESH
//
//  Created by Vikas on 25/01/21.
//

import UIKit
import DropDown

class SingleProductDetailsVC: UIViewController {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productSubNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var expDateBtn: UIButton!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var weightBtn: UIButton!
    @IBOutlet weak var quantityBtn: UIButton!
    @IBOutlet weak var similarListCV: UICollectionView!
    
    //Models
    var singleProductDetailsModel:SingleProductDetailsModel?
    var similarProductsModel:SimilarProductsModel?
    var similarProductsArr = [SimilarProductsArr]()

    let cartImage = UIBarButtonItem()
    let searchImage = UIBarButtonItem()
    let wishListImage = UIBarButtonItem()
    
    var selectAnimation = Int()
    var quantity = Int()
    var dummyArr = [Int]()
    var isAnimationSelected = Bool()
    
    //BarbuttonItem
    let badgeCount = UILabel()
    let quantityDropDown = DropDown()
    var quantityloop = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle(heading: "Product Details")
        
        productImage.contentMode = .scaleAspectFit
        
        badgeCount.frame = CGRect(x: 15, y: -05, width: 16, height: 16)
        badgeCount.layer.borderColor = UIColor.clear.cgColor
        badgeCount.layer.borderWidth = 2
        badgeCount.layer.cornerRadius = badgeCount.bounds.height / 2
        badgeCount.textAlignment = .center
        badgeCount.layer.masksToBounds = true
        badgeCount.textColor = .white
        badgeCount.font = badgeCount.font.withSize(12)
        badgeCount.backgroundColor = .red
        badgeCount.text = "0"
        
        
        let cartImage = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        cartImage.setBackgroundImage(UIImage(named: "cart_gray"), for: .normal)
        cartImage.addTarget(self, action: #selector(onTapCart(_:)), for: .touchUpInside)
        cartImage.addSubview(badgeCount)
        
        
        let cartImgBtn = UIBarButtonItem(customView: cartImage)
        
        //Wishlist
        wishListImage.image = UIImage(named: "wish_list_gray")?.withRenderingMode(.alwaysOriginal)
        wishListImage.target = self
        wishListImage.action = #selector(onTapWishlist(_:))
        
        navigationItem.rightBarButtonItems = [cartImgBtn,wishListImage]
        setUpCollectionView()
        
        selectAnimation = -1
        isAnimationSelected = false
        
    }
    
    func setUpCollectionView() {
        similarListCV.delegate = self
        similarListCV.dataSource = self
        similarListCV.register(SimilarItemsCVCell.nib, forCellWithReuseIdentifier: SimilarItemsCVCell.identifier)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            cartProductsListAPI()
            userSingleCategoryListAPI(pid: productIDString)
            similarProductsAPI(productID: productIDString)
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    //MARK:-  Nav bar Item Actions
    //wishlist
    @objc func onTapWishlist(_ sender: UIBarButtonItem){
        
        if Reachability.isConnectedToNetwork() {
            addToWishListAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    //search
    @objc func onTapSearch(_ sender: UIBarButtonItem){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SearchFilterViewController") as! SearchFilterViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //cart
    @objc func onTapCart(_ sender: UIBarButtonItem){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onTapQuantity(_ sender: Any) {
        quantityloop.removeAll()
        for i in 1...20 {
            quantityloop.append("\(i)")
        }
        
        quantityDropDown.anchorView = quantityBtn
        quantityDropDown.bottomOffset = CGPoint(x: 0, y: quantityBtn.bounds.height)
        quantityDropDown.heightAnchor.constraint(equalToConstant: 60).isActive = true
        quantityDropDown.dataSource = quantityloop
        quantityDropDown.selectionAction = { [weak self] (index, item) in
            print("Drop down printed")

            self?.quantityLbl.text = item
        }
        quantityDropDown.show()
        view.layoutIfNeeded()
    }
    
    @IBAction func onTapWeight(_ sender: Any) {
        
        let details = singleProductDetailsModel?.data?[0]
        
        loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
        
        quantityDropDown.show()
    }
    
    
    //MARK:-  IB Actions
    @IBAction func onTapViewAll(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SimilarProductListVC") as! SimilarProductListVC
        vc.productIDString = productIDString
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onTapConfirm(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            let details = singleProductDetailsModel?.data?[0]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: priceLbl.text ?? "", quantityItem: quantityLbl.text ?? "", weight: weightLbl.text ?? "")
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    //MARK:- Cell Actions
    @objc func onTapMinusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: similarListCV)
        let indexPath = similarListCV.indexPathForItem(at: buttonPosition)
        let cell = similarListCV.cellForItem(at: indexPath!) as! SimilarItemsCVCell
        
        if quantity > 1 {
            
            quantity = Int(cell.quantityLbl.text ?? "") ?? 0
            quantity -= 1
            cell.quantityLbl.text = String(quantity)
        }else if quantity == 1 {
            
        }
        
    }
    
    @objc func onTapPlusBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: similarListCV)
        let indexPath = similarListCV.indexPathForItem(at: buttonPosition)
        let cell = similarListCV.cellForItem(at: indexPath!) as! SimilarItemsCVCell
        
        quantity = Int(cell.quantityLbl.text ?? "") ?? 0
        quantity += 1
        cell.quantityLbl.text = String(quantity)
        
    }
    
    @objc func onTapAddtoCart(_ sender: UIButton) {
        
        dummyArr[sender.tag] = 1
        selectAnimation = sender.tag
        isAnimationSelected = true
        
        if Reachability.isConnectedToNetwork() {
            let buttonPosition = sender.convert(CGPoint.zero, to: similarListCV)
            let indexPath = similarListCV.indexPathForItem(at: buttonPosition)
            let cell = similarListCV.cellForItem(at: indexPath!) as! SimilarItemsCVCell
            
            let details = similarProductsModel?.data?[sender.tag]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell.productPriceLbl.text ?? "", quantityItem: String(quantity), weight: cell.weightLbl.text ?? "")
            
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    @objc func onTapWeightDropDown(_ sender: UIButton) {
        let indexPath = IndexPath(item: sender.tag, section: 0)
        let cell = similarListCV.cellForItem(at: indexPath) as! SimilarItemsCVCell
        
        let details = similarProductsModel?.data?[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
    }
    
    func loadQuantityData(quantityData: [String], priceData: [String]){
        
        print("Drop down called")
        quantityDropDown.anchorView = weightBtn
        quantityDropDown.bottomOffset = CGPoint(x: 0, y: weightBtn.bounds.height)
        quantityDropDown.dataSource = quantityData
        quantityDropDown.selectionAction = { [weak self] (index, item) in
            print("Drop down printed")

            self?.weightLbl.text = item
            self?.priceLbl.text = priceData[index]
        }
        quantityDropDown.show()
    }
    
    //MARK:-  API
    //categorylist
    func userSingleCategoryListAPI(pid: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.productid: pid
        ]
        print(params)
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: singleProductDetails_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SingleProductDetailsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.singleProductDetailsModel = responseJson
                
                self.productImage.sd_setImage(with: URL(string: responseJson.data?[0].image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: UIImage(systemName: ""))
                self.productNameLbl.text = self.singleProductDetailsModel?.data?[0].itemName ?? ""
                self.productSubNameLbl.text = ""
                self.descriptionLbl.text = self.singleProductDetailsModel?.data?[0].datumDescription ?? ""
                self.priceLbl.text = String(format: "$%@", self.singleProductDetailsModel?.data?[0].itemPrice ?? "") 
                self.expDateBtn.setTitle(self.singleProductDetailsModel?.data?[0].expiryDate ?? "", for: .normal)
                self.weightLbl.text = self.singleProductDetailsModel?.data?[0].weight ?? ""
                self.quantityLbl.text = self.singleProductDetailsModel?.data?[0].quantity ?? ""
                
                if self.singleProductDetailsModel?.data?[0].cartlist ?? "" == "yes" {
                    self.priceLbl.text = String(format: "$%@", self.singleProductDetailsModel?.data?[0].cartPrice ?? "")
                    self.weightLbl.text = self.singleProductDetailsModel?.data?[0].cartGrams ?? ""
                    self.quantityLbl.text = self.singleProductDetailsModel?.data?[0].cartQuantity ?? ""
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    // Add to wishilist
    func addToWishListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.product_id : self.singleProductDetailsModel?.data?[0].id ?? "",
            APIParameters.category_id : self.singleProductDetailsModel?.data?[0].categoryID ?? "",
            APIParameters.uID : uids
        ]
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addProductToWishList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)

            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    //MARK:-  Similar products
    func similarProductsAPI(productID:String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.product_id : productID,
            APIParameters.uID: uids
        ]
        
        print(params, "similar")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: similarProduct_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SimilarProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                self.similarProductsModel = responseJson
                
                if self.similarProductsModel?.data?.count ?? 0 > 0 {
                    for _ in 0..<self.similarProductsModel!.data!.count {
                        self.dummyArr.append(0)
                    }
                }
                DispatchQueue.main.async {
                    self.similarListCV.reloadData()
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    //Addto cart
    func addToCartAPI(cid: String, pid: String, price:String, quantityItem: String, weight: String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityItem,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("Parameres: \(params)")
        print("UriString: \(addToCart_URL)")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.similarProductsAPI(productID: productIDString)
                    self.cartProductsListAPI()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Cart list
    func cartProductsListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: cartProductsList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CartProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                DispatchQueue.main.async {
                    self.badgeCount.text = "\(responseJson.data?.count ?? 0)"
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    //Delete Cart
    func userDeleteCartAPI(productId:String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.pid : productId,
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: deleteCartProductURL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                
                DispatchQueue.main.async {
                    self.cartProductsListAPI()
                    self.similarProductsAPI(productID: productId)
                }

            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}

extension SingleProductDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count = similarProductsModel?.data?.count {
            if count > 3 {
                return 3
            }else{
                return count
            }
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = similarListCV.dequeueReusableCell(withReuseIdentifier: SimilarItemsCVCell.identifier, for: indexPath) as! SimilarItemsCVCell
        
        let details = similarProductsModel?.data?[indexPath.row]
        
        cell.productNameLbl.text = details?.itemName ?? ""
        cell.quantityLbl.text = details?.weight ?? ""
        
        let chosenImgUrl = details?.image
        let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.productImage.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
        
        cell.productPriceLbl.text = String(format: "$%@ / packet", details?.itemPrice ?? "")
        cell.productPriceLbl.highlight(searchedText: String(format: "$%@", details?.itemPrice ?? ""))
        
        if dummyArr[indexPath.row] == 1 {
            UIView.animate(withDuration: 0.5) {
                cell.animatedViewWidth.constant = 20
                cell.spinnerImage.image = UIImage(named: "Radio  Check")
            }
        }else {
            cell.animatedViewWidth.constant = 0
        }
        
        if selectAnimation == indexPath.row {
            
            cell.startSpin()
        }else {
            cell.stopSpin()
        }
        
        if similarProductsModel?.data?[indexPath.row].cartlist == "No" {
            cell.animatedViewWidth.constant = 0
            cell.checkMarkImage.image = UIImage(named: "")
            cell.spinnerImage.image = UIImage(named: "")
            cell.quantityLbl.text = "1"
            cell.productPriceLbl.text = similarProductsModel?.data?[indexPath.row].itemPrice ?? ""
            
        }else{
            cell.startSpin()
            cell.stopSpin()
            cell.animatedViewWidth.constant = 20
            cell.quantityLbl.text = similarProductsModel?.data?[indexPath.row].cartQuantity ?? ""
            cell.weightLbl.text = similarProductsModel?.data?[indexPath.row].cartGrams ?? ""
            cell.productPriceLbl.text = similarProductsModel?.data?[indexPath.row].cartPrice ?? ""
            
        }
        
        cell.addtoCartBtn.tag = indexPath.row
        cell.weightBtn.tag = indexPath.row
        cell.quantityDropDown.tag = indexPath.row
        cell.minBtn.tag = indexPath.row
        cell.maxBtn.tag = indexPath.row
        
        
        cell.addtoCartBtn.addTarget(self, action: #selector(onTapAddtoCart(_:)), for: .touchUpInside)
        cell.weightBtn.addTarget(self, action: #selector(onTapWeightDropDown(_:)), for: .touchUpInside)
        cell.maxBtn.addTarget(self, action: #selector(onTapPlusBtn(_:)), for: .touchUpInside)
        cell.minBtn.addTarget(self, action: #selector(onTapMinusBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (similarListCV.bounds.width - (3-1) * 1) / 3
        
        return CGSize(width: width, height: 163)
    }
    
}

