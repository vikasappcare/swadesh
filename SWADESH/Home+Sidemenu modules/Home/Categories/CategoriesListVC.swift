//
//  CategoriesListVC.swift
//  SWADESH
//
//  Created by Vikas on 20/01/21.
//

import UIKit

class CategoriesListVC: UIViewController {
    
    @IBOutlet weak var categoryCV: UICollectionView!
    @IBOutlet weak var totalCategoriesLbl: UILabel!
    
    //Model
    var categoryListModel: CategoryListModel?
    
    let badgeCount = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.categories)
        setUpCollectionView()
        
        badgeCount.frame = CGRect(x: 15, y: -05, width: 16, height: 16)
        badgeCount.layer.borderColor = UIColor.clear.cgColor
        badgeCount.layer.borderWidth = 2
        badgeCount.layer.cornerRadius = badgeCount.bounds.height / 2
        badgeCount.textAlignment = .center
        badgeCount.layer.masksToBounds = true
        badgeCount.textColor = .white
        badgeCount.font = badgeCount.font.withSize(12)
        badgeCount.backgroundColor = .red
        badgeCount.text = "0"
        
        
        let cartImage = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        cartImage.setBackgroundImage(UIImage(named: "cart_gray"), for: .normal)
        cartImage.addTarget(self, action: #selector(onTapCart(_:)), for: .touchUpInside)
        cartImage.addSubview(badgeCount)
        
        
        let cartImgBtn = UIBarButtonItem(customView: cartImage)

        let searchImage = UIBarButtonItem(image: UIImage(named: "Mask Group 49")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(onTapSearch(_:)))
        
        navigationItem.rightBarButtonItems = [cartImgBtn,searchImage]
        
    }
    
    //MARK:-  Nav bar Item Actions
    @objc func onTapSearch(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "SearchFilterViewController") as! SearchFilterViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onTapCart(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability.isConnectedToNetwork() {
            userCategoryListAPI()
            cartProductsListAPI()
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    //MARK:- Selector
    //CV Setup
    func setUpCollectionView() {
        
        categoryCV.delegate = self
        categoryCV.dataSource = self
        
        categoryCV.register(CategoriesCVCell.nib, forCellWithReuseIdentifier: CategoriesCVCell.identifier)
    }
    
    //MARK:- API
    func userCategoryListAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: categoryList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CategoryListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.categoryListModel = responseJson
                
                DispatchQueue.main.async {
                    self.totalCategoriesLbl.text = "\(responseJson.data?.count ?? 0) Categories"
                    self.categoryCV.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

    }
    
    //Cart list
    func cartProductsListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: cartProductsList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CartProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                DispatchQueue.main.async {
                    self.badgeCount.text = "\(responseJson.data?.count ?? 0)"
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}

extension CategoriesListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryListModel?.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoryCV.dequeueReusableCell(withReuseIdentifier: CategoriesCVCell.identifier, for: indexPath) as! CategoriesCVCell
        
        let chosenImgUrl = categoryListModel?.data?[indexPath.item].image
        let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.categoryImage.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
        
        cell.categoryName.text = categoryListModel?.data?[indexPath.row].name ?? ""
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (categoryCV.frame.size.width / 3) - 20, height: 134)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(identifier: "SingleCategoryVC") as! SingleCategoryVC
        vc.navTitle = categoryListModel?.data?[indexPath.row].name ?? ""
        categoryIDString = categoryListModel?.data?[indexPath.row].id ?? ""
        print(categoryIDString, "selected ID")
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
