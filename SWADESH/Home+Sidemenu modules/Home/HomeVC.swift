//
//  HomeVC.swift
//  SWADESH
//
//  Created by Vikas on 19/01/21.
//

import UIKit
import DropDown

class HomeVC: BaseVC {
    
    @IBOutlet weak var menuImage: UIBarButtonItem!
    @IBOutlet weak var userNameLbl: UIBarButtonItem!
    @IBOutlet weak var homeTblVw: UITableView!
    
    var namesArr = [String]()
    var categoryNamesArr = [String]()
    var categoryImgArr = [String]()
    var itemImgArr = [String]()
    var isSelectedMenu = Int()
    var animating = false
    var selectAnimation = Int()
    var isAnimationSelected = Bool()
    var animationSelectedArr = [Int]()
    var dummyArr = [Int]()
    
    //Models
    var bannersArr : HomeBannerModel?
    var newArrivalsModel: HomeNewArrivalsModel?
    var quantityModelArr = [HomeQuantityArr]()
    var productsListArr : HomeProductsListModel?
    var categoryListModel: CategoryListModel?
    var frequentlyOrderModel: FrequentlyOrderModel?
    var quantityArr:[String] = []
    let paymentDetailsDropDown = DropDown()
    var arrivalId = String()
    var quantiy = Int()
    var pid = String()
    var cid = String()
    var itemPrice = String()
    var itemQuantity = String()
    var itemWeight = String()
    
    //BarbuttonItem
    let badgeCount = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        badgeCount.frame = CGRect(x: 15, y: -05, width: 16, height: 16)
        badgeCount.layer.borderColor = UIColor.clear.cgColor
        badgeCount.layer.borderWidth = 2
        badgeCount.layer.cornerRadius = badgeCount.bounds.height / 2
        badgeCount.textAlignment = .center
        badgeCount.layer.masksToBounds = true
        badgeCount.textColor = .white
        badgeCount.font = badgeCount.font.withSize(12)
        badgeCount.backgroundColor = .red
        //badgeCount.text = "4"
        
        
        let cartImg = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        cartImg.setBackgroundImage(UIImage(named: "cart_gray"), for: .normal)
        cartImg.addTarget(self, action: #selector(onTapCart(_:)), for: .touchUpInside)
        cartImg.addSubview(badgeCount)
        
        
        let cartImgBtn = UIBarButtonItem(customView: cartImg)
        
        let searchImage = UIBarButtonItem(image: UIImage(named: "Mask Group 49")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(onTapSearch(_:)))
        
        navigationItem.rightBarButtonItems = [cartImgBtn,searchImage]
        
        ontapMenu(pImage: #imageLiteral(resourceName: "user-gray"))
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        
        isSelectedMenu = 0
        selectAnimation = -1
        isAnimationSelected = false
        animationSelectedArr = []
        tableViewNibs()
        
        namesArr = ["New arrivals","Hot Sale","Only Vegan","Diwali Sale","Christamas sale"]
        categoryNamesArr = ["Vegetables","Fruits","Snacks","Milk","Oils"]
        categoryImgArr = ["marisol-benitez","nica-cn","charles-deluvio","gabi-miranda","roberta-sorge"]
        itemImgArr = ["biscukets","buttony","burger","alexandra-kikot","nathalia-rosa","buisckets","buttony","burger","alexandra-kikot","nathalia-rosa"]
        
        if Reachability.isConnectedToNetwork() {
            
            userProfileApi()
            cartProductsListAPI()
            userBannersListAPI()
            userCategoryListAPI()
            userNewArrivalsListAPI()
           
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
        
    }
    
    //menu icon
    func ontapMenu(pImage: UIImage) {
        
        let imageView = UIImageView()
        
        imageView.image = pImage
        imageView.contentMode = .scaleToFill
        
        let button2 = UIButton(type: .custom)
        button2.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
        button2.setImage(imageView.image, for: .normal)
        button2.imageView?.contentMode = .scaleToFill
        let currWidth = button2.widthAnchor.constraint(equalToConstant: 30)
        currWidth.isActive = true
        let currHeight = button2.heightAnchor.constraint(equalToConstant: 30)
        currHeight.isActive = true
        button2.addTarget(self, action: #selector(BaseVC.onSlideMenuButtonPressed(_:)), for: .touchUpInside)
        button2.layer.cornerRadius = button2.frame.width/2
        button2.layer.masksToBounds = true
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                          target: nil, action: nil)
        spaceButton.width = 20
        let barButtonItem2 = UIBarButtonItem(customView: button2)
        
        navigationItem.leftBarButtonItem = barButtonItem2
    }
    
    @objc func onTapSearch(_ sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewController(identifier: "SearchFilterViewController") as! SearchFilterViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.endEditing(true)
        
        if Reachability.isConnectedToNetwork() {
            
            userProfileApi()
            cartProductsListAPI()
            userBannersListAPI()
            userCategoryListAPI()
            userNewArrivalsListAPI()
 
        }else{
            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(changeImage), userInfo: nil, repeats: true)
    }
    
    func tableViewNibs(){
        homeTblVw.estimatedRowHeight = homeTblVw.rowHeight
        homeTblVw.rowHeight = UITableView.automaticDimension
        homeTblVw.separatorStyle = .none
        homeTblVw.delegate = self
        homeTblVw.dataSource = self
        homeTblVw.reloadData()
        
        homeTblVw.register(UINib(nibName: BannersTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: BannersTableViewCell.identifier)
        homeTblVw.register(UINib(nibName: CategoriesTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CategoriesTableViewCell.identifier)
        homeTblVw.register(UINib(nibName: SubCategoriesTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: SubCategoriesTableViewCell.identifier)
        homeTblVw.register(FrequentlyOrderedTVCell.nib, forCellReuseIdentifier: FrequentlyOrderedTVCell.identifier)
        //        homeTblVw.register(UINib(nibName: ProductsTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ProductsTableViewCell.identifier)
        homeTblVw.register(ItemTableViewCell.nibName, forCellReuseIdentifier: ItemTableViewCell.identifier)
        
    }
    
    //MARK:-  IB Actions
    @objc func onClickAddToCartBtn(_ sender: UIButton){
        
        isAnimationSelected = true
        selectAnimation  = sender.tag
        dummyArr[sender.tag] = 1
        
        print(dummyArr)
        
        let buttonPosition = sender.convert(CGPoint.zero, to: homeTblVw)
        let indexPath = homeTblVw.indexPathForRow(at:buttonPosition)
        let cell = homeTblVw.cellForRow(at: indexPath!) as? ItemTableViewCell
        
        homeTblVw.reloadRows(at: [IndexPath(row: sender.tag, section: 4)], with: .fade)
        
//        pid = productsListArr?.data[sender.tag].id ?? ""
//        cid = productsListArr?.data[sender.tag].categoryID ?? ""
//        itemPrice = productsListArr?.data[sender.tag].itemPrice ?? ""
//        itemQuantity = cell?.quantityLbl.text ?? ""
//        itemWeight = cell?.itemWeightLbl.text ?? ""
        
        let details = productsListArr?.data[sender.tag]
        addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell?.priceLbl.text ?? "", quantityValue: cell?.quantityLbl.text ?? "", weight: cell?.itemWeightLbl.text ?? "")
    }
    
    @objc func onClickItemWeightBtn(_ sender: UIButton){
        
        let indexPath = IndexPath(row: sender.tag, section: 4)
        let cell = homeTblVw.cellForRow(at: indexPath) as! ItemTableViewCell
        
        let details = productsListArr?.data[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
        
    } 
    
    @objc func onClickQuantityDecrementBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: homeTblVw)
        let indexPath = homeTblVw.indexPathForRow(at:buttonPosition)
        let cell = homeTblVw.cellForRow(at: indexPath!) as? ItemTableViewCell
        quantiy = Int(cell?.quantityLbl.text ?? "") ?? 0
        
        if quantiy > 1 {
            quantiy = quantiy - 1
            cell?.quantityLbl.text = String(quantiy)
        }
    }
    
    @objc func onClickQuantityIncrementBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: homeTblVw)
        let indexPath = homeTblVw.indexPathForRow(at:buttonPosition)
        let cell = homeTblVw.cellForRow(at: indexPath!) as? ItemTableViewCell
        quantiy = Int(cell?.quantityLbl.text ?? "") ?? 0
        quantiy = quantiy + 1
        cell?.quantityLbl.text = String(quantiy)
        
    }
    
    @objc func onClickViewAllBtn(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CategoriesListVC") as! CategoriesListVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onTapCart(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickSearchBtn(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SearchFilterViewController") as! SearchFilterViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:-  UITableViewDelegate, UITableViewDataSource
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if section == 4 {
            
            return productsListArr?.data.count ?? 0
            
        }else {
            
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            guard let cell = homeTblVw.dequeueReusableCell(withIdentifier: BannersTableViewCell.identifier, for: indexPath) as? BannersTableViewCell else{return UITableViewCell()}
            cell.selectionStyle = .none
            return cell
            
        }else if indexPath.section == 1 {
            guard let cell = homeTblVw.dequeueReusableCell(withIdentifier: CategoriesTableViewCell.identifier, for: indexPath) as? CategoriesTableViewCell else{return UITableViewCell()}
            cell.selectionStyle = .none
            cell.categoryCOllVw.register(UINib(nibName: CategoriesCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: CategoriesCollectionViewCell.identifier)
            
            cell.categoryCOllVw.delegate = self
            cell.categoryCOllVw.dataSource = self
            cell.categoryCOllVw.reloadData()
            
            cell.viewAllBtn.tag = indexPath.row
            cell.viewAllBtn.addTarget(self, action: #selector(onClickViewAllBtn), for: .touchUpInside)
            
            return cell
            
        }else if indexPath.section == 2 {
            
            guard let frequentlyCell = homeTblVw.dequeueReusableCell(withIdentifier: FrequentlyOrderedTVCell.identifier, for: indexPath) as? FrequentlyOrderedTVCell else {return UITableViewCell()}
            frequentlyCell.loadVC(viewCotroller: self)
            frequentlyCell.viewAllButton.addTarget(self, action: #selector(onTapViewAll(_:)), for: .touchUpInside)
        
            return frequentlyCell
            
        }else if indexPath.section == 3 {
            
            guard let cell = homeTblVw.dequeueReusableCell(withIdentifier: SubCategoriesTableViewCell.identifier, for: indexPath) as? SubCategoriesTableViewCell else{return UITableViewCell()}
            cell.selectionStyle = .none
            cell.subCategoryCollVw.register(UINib(nibName: SubCategoryNameCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: SubCategoryNameCollectionViewCell.identifier)
            
            cell.subCategoryCollVw.delegate = self
            cell.subCategoryCollVw.dataSource = self
            cell.subCategoryCollVw.reloadData()
            return cell
            
        }else {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier, for: indexPath) as? ItemTableViewCell else{return UITableViewCell()}
            cell.selectionStyle = .none
            
            if productsListArr?.data.count  ?? 0 > 0 {
                cell.itemNameLbl.text = productsListArr?.data[indexPath.row].itemName
                cell.itemDescriptionLbl.text = productsListArr?.data[indexPath.row].itemName
                cell.priceLbl.text = productsListArr?.data[indexPath.row].itemPrice ?? ""
                cell.quantityLbl.text = productsListArr?.data[indexPath.row].quantity ?? ""
                cell.itemWeightLbl.text = productsListArr?.data[indexPath.row].weight ?? ""
                
                let chosenImgUrl = productsListArr?.data[indexPath.item].image
                let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                cell.itemImgVw.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: ""))
                
                
            }
            
            if dummyArr[indexPath.row] == 1 {
                UIView.animate(withDuration: 0.5) {
                    cell.animationVwDynamicWidth.constant = 40
                    cell.spinnerImgVw.image = UIImage(named: "Radio  Check")
                }
            }else {
                cell.animationVwDynamicWidth.constant = 0
            }
            
            if selectAnimation == indexPath.row {

                cell.startSpin()
            }else {
                cell.stopSpin()
            }
            
            if productsListArr?.data[indexPath.row].cartlist == "No" {
                cell.animationVwDynamicWidth.constant = 0
                cell.checkMarkImgVw.image = UIImage(named: "")
                cell.spinnerImgVw.image = UIImage(named: "")
                cell.quantityLbl.text = "1"
                
            }else{
                cell.startSpin()
                cell.stopSpin()
                cell.animationVwDynamicWidth.constant = 40
                cell.quantityLbl.text = productsListArr?.data[indexPath.row].cartQuantity ?? ""
                cell.itemWeightLbl.text = productsListArr?.data[indexPath.row].cartGrams ?? ""
                cell.priceLbl.text = productsListArr?.data[indexPath.row].cartPrice ?? ""
                
            }
            
            cell.addToCartBtn.tag = indexPath.row
            cell.itemWeightBtn.tag = indexPath.row
            cell.quantityDropDown.tag = indexPath.row
            cell.quantityDecrementBtn.tag = indexPath.row
            cell.quantityIncrementBtn.tag = indexPath.row
            
            
            cell.addToCartBtn.addTarget(self, action: #selector(onClickAddToCartBtn), for: .touchUpInside)
            cell.itemWeightBtn.addTarget(self, action: #selector(onClickItemWeightBtn), for: .touchUpInside)
            cell.quantityIncrementBtn.addTarget(self, action: #selector(onClickQuantityIncrementBtn), for: .touchUpInside)
            cell.quantityDecrementBtn.addTarget(self, action: #selector(onClickQuantityDecrementBtn), for: .touchUpInside)
            
            return cell
            
        }
        
    }
    
    @objc func onTapViewAll(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "FrequentlyOrderedViewController") as! FrequentlyOrderedViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 200
        }else if indexPath.section == 1 {
            return 120
        }else if indexPath.section == 2{
            return 190
        }else if indexPath.section == 3 {
            return 35
        }else {
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 333 {
            
            //            let vc = storyboard?.instantiateViewController(identifier: "SingleProductDetailsVC") as!  SingleProductDetailsVC
            //            //self.SingleProductDetailsVC =
            //            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
}

//MAR:- Extension Collection view
extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 111 {
            
            if self.categoryListModel?.data?.count ?? 0 > 5 {
                
                return 5
            }else {
                
                return self.categoryListModel?.data?.count ?? 0
            }
        }else{
            return newArrivalsModel?.data.count ?? 0
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 111 {
            
            let categoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoriesCollectionViewCell.identifier, for: indexPath) as! CategoriesCollectionViewCell
            
            let chosenImgUrl = categoryListModel?.data?[indexPath.item].image
            let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            categoryCell.categoryImgVw.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
            categoryCell.categoryNameLbl.text = categoryListModel?.data?[indexPath.row].name
            return categoryCell
            
        }else {
            
            let subCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: SubCategoryNameCollectionViewCell.identifier, for: indexPath) as! SubCategoryNameCollectionViewCell
            
            if indexPath.item == isSelectedMenu {
                
                subCategoryCell.subCategoryNameLbl.textColor = #colorLiteral(red: 0.9882352941, green: 0.7215686275, blue: 0, alpha: 1)
            }else {
                
                subCategoryCell.subCategoryNameLbl.textColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.6)
                
            }
            
            subCategoryCell.subCategoryNameLbl.text = newArrivalsModel?.data[indexPath.row].productName
            
            return subCategoryCell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 111 {
            
            let width = (collectionView.frame.width - (5 - 1) * 1) / 5
            
            return CGSize(width: width, height: 80)
            
        }else{
            let size: CGSize = (newArrivalsModel?.data[indexPath.row].productName as AnyObject).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)])
            return CGSize(width: size.width + 30, height: 32)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView.tag == 111 {
            return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -5)
        }else{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 111{
            
        }else {
            isSelectedMenu = indexPath.row
            self.arrivalId = self.newArrivalsModel?.data[indexPath.item].id ?? ""
            collectionView.scrollToItem(at:indexPath, at: .centeredHorizontally, animated: true)
            collectionView.reloadData()
            userProductsListAPI()
        }
    }
    
}

extension HomeVC {
    
    //MARK:- API
    func userProfileApi() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID : uids
        ]
        
        print(params)
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: profile_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(ProfileModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                
                 self.userNameLbl.title = String(format: "Hey %@ %@", responseJson.data?[0].firstName ?? "", (responseJson.data?[0].lastName ?? ""))
                let fname = responseJson.data?[0].firstName ?? ""
                let lname = (responseJson.data?[0].lastName ?? "")
                userDefault.setValue(fname+" "+lname, forKey: userDefaults.fullName)
                userDefault.setValue(responseJson.data?[0].email ?? "", forKey: userDefaults.email)
                
                userDefault.setValue(responseJson.data?[0].pPic ?? "", forKey: userDefaults.profilePic)
                
                guard let url = URL(string:responseJson.data?[0].pPic ?? "") else {return}
                if let data = try? Data(contentsOf: url)
                {
                    let image: UIImage = UIImage(data: data)!
                    self.ontapMenu(pImage: image)
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    func userBannersListAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: bannerList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(HomeBannerModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.bannersArr = responseJson
                
                DispatchQueue.main.async {
                    self.homeTblVw.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    
    func userCategoryListAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: categoryList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CategoryListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.categoryListModel = responseJson
                print("Reponse:\(self.categoryListModel?.data)")
                DispatchQueue.main.async {
                    self.homeTblVw.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
    func userNewArrivalsListAPI() {
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: newArrivalsList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(HomeNewArrivalsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.newArrivalsModel = responseJson
                self.arrivalId = self.newArrivalsModel?.data[0].id ?? ""
                self.userProductsListAPI()
                DispatchQueue.main.async {
                    self.homeTblVw.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
        
    }
    
    func userProductsListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID: uids,
            APIParameters.arrival_id : arrivalId
        ]
        
        print(params, "arrival")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: productsList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(HomeProductsListModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.productsListArr = responseJson
                print("Response: \(String(describing: self.productsListArr?.data))")
                if self.productsListArr?.data.count ?? 0 > 0 {
                    //                    self.dummyArr = []
                    for _ in 0..<self.productsListArr!.data.count{
                        
                        self.dummyArr.append(0)
                    }
                }
                DispatchQueue.main.async {
                    self.homeTblVw.reloadData()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
        
    }
    
    func addToCartAPI(cid:String, pid:String, price:String, quantityValue:String, weight:String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityValue,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("Parameres: \(params)")
        print("UriString: \(addToCart_URL)")
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+2.1) {
                    self.cartProductsListAPI()
                    self.userProductsListAPI()
                }
                
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    func cartProductsListAPI() {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}

        let params = [
            APIParameters.uID : uids
        ]

        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: cartProductsList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(CartProductsModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                DispatchQueue.main.async {
                    self.badgeCount.text = "\(responseJson.data?.count ?? 0)"
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}
