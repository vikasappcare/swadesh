//
//  BannersTableViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit

class BannersTableViewCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    class var nibName: String {
        return "\(self)"
    }

    @IBOutlet weak var bannerCollVw: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //Model
    var bannersArr : HomeBannerModel?
    
    var timer = Timer()
    var counter = 0
    var imgArr = [Int]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bannerCollVw.delegate = self
        bannerCollVw.dataSource = self
        bannerCollVw.register(BannerCollectionViewCell.nib, forCellWithReuseIdentifier: BannerCollectionViewCell.identifier)
        
        if Reachability.isConnectedToNetwork() {
            userBannersListAPI()
        }else{
            print("Network error")
        }
        
    }
    
    @objc func changeImage() {
        
        bannerCollVw.isPagingEnabled = false
        
        if counter < imgArr.count {
            let index = IndexPath(item: counter, section: 0)
            self.bannerCollVw.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControl.currentPage = counter
            counter += 1
            
        } else {
            counter = 0
            
            let index = IndexPath(item: counter, section: 0)
            self.bannerCollVw.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageControl.currentPage = counter
            counter = 1
        }
        
        bannerCollVw.isPagingEnabled = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- API
    func userBannersListAPI() {
        
        //Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: bannerList_URL, method: .get, parameter: nil) {[weak self] (response) in
            guard let self = self else {return}
            //Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(HomeBannerModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.bannersArr = responseJson
                
                for _ in 0..<responseJson.data.count {
                    self.imgArr.append(0)
                }
                
                DispatchQueue.main.async {
                    self.pageControl.currentPage = 0
                    self.pageControl.numberOfPages = self.bannersArr?.data.count ?? 0
                    self.bannerCollVw.reloadData()
                    self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
                }
                
            }else{
                print(responseJson.message, "cell1")
                //showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            print(error.localizedDescription, "cell")
            //showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}

extension BannersTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannersArr?.data.count ?? 0
    }
    
    //scroll delegate for scroll images
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        counter = Int(bannerCollVw.contentOffset.x/bannerCollVw.frame.size.width)
        pageControl.currentPage = counter
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = bannerCollVw.dequeueReusableCell(withReuseIdentifier: BannerCollectionViewCell.identifier, for: indexPath) as! BannerCollectionViewCell
        
        let chosenImgUrl = bannersArr?.data[indexPath.row].image
        let chosenImgUrlString = chosenImgUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.bannerImgVw.sd_setImage(with: URL(string: chosenImgUrlString ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: bannerCollVw.bounds.width/1, height: bannerCollVw.bounds.height)
    }
}
