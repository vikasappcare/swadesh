//
//  ProductsTableViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var productsTblVw: UITableView!
    
    class var identifier: String {
        return "\(self)"
    }
    class var nibName: String {
        return "\(self)"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
