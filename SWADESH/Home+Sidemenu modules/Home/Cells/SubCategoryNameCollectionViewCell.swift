//
//  SubCategoryNameCollectionViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit

class SubCategoryNameCollectionViewCell: UICollectionViewCell {

    class var identifier: String {
        return "\(self)"
    }
    class var nibName: String {
        return "\(self)"
    }
    @IBOutlet weak var subCategoryNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
