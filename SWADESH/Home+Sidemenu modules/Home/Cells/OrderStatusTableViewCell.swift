//
//  OrderStatusTableViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 28/01/21.
//

import UIKit

class OrderStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var statusImageCheck: UIImageView!
    @IBOutlet weak var topVw: UIView!
    @IBOutlet weak var bottomVw: UIView!
    @IBOutlet weak var ordertitleLbl: UILabel!
    @IBOutlet weak var orderSubTitleLbl: UILabel!
    
    class var identifier: String {
        return "\(self)"
    }
    class var nibName: String {
        return "\(self)"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        drawDottedLine(start: CGPoint(x: topVw.bounds.minY, y: topVw.bounds.minX), end: CGPoint(x: topVw.bounds.minY, y: topVw.bounds.maxY), view: topVw)

        drawDottedLine(start: CGPoint(x: bottomVw.bounds.minY, y: bottomVw.bounds.minX), end: CGPoint(x: bottomVw.bounds.minY, y: bottomVw.bounds.maxY), view: bottomVw)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

