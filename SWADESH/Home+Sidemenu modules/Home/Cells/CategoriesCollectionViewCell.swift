//
//  CategoriesCollectionViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {

    class var identifier: String {
        return "\(self)"
    }
    class var nibName: String {
        return "\(self)"
    }

    @IBOutlet weak var categoryImgVw: UIImageView!
    @IBOutlet weak var categoryNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
