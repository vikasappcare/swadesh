//
//  FrequentlyOrderedTVCell.swift
//  SWADESH
//
//  Created by apple on 30/03/21.
//

import UIKit

class FrequentlyOrderedTVCell: UITableViewCell {
    
    class var identifier: String {
        return "\(self)"
    }
    
    class var nib: UINib {
        return UINib(nibName: FrequentlyOrderedTVCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var frequentlyOrderedCV: UICollectionView!
    @IBOutlet weak var viewAllButton: UIButton!
    
    //Model
    var frequentlyOrderModel: FrequentlyOrderModel?
    
    //Animation Addto cart
    var selectedAnimation = Int()
    var isAnimationSelected = Bool()
    var dummyArr = [Int]()
    var quantity = Int()
    
    var vc: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCollectionView()
        
        if Reachability.isConnectedToNetwork() {
            userFrequentlyOrders()
        }
        
        selectedAnimation = -1
        isAnimationSelected = false
    }
    
    func loadVC(viewCotroller: UIViewController){
        self.vc = viewCotroller
    }
    
    func setupCollectionView() {
        frequentlyOrderedCV.delegate = self
        frequentlyOrderedCV.dataSource = self
        frequentlyOrderedCV.register(FrequentlyOrderedCVCell.nib, forCellWithReuseIdentifier: FrequentlyOrderedCVCell.identifier)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK:- Cell selector
    @objc func onTapAddToCart(_ sender: UIButton) {
        print("cart button \(sender.tag)")
        selectedAnimation = sender.tag
        isAnimationSelected = true
        dummyArr[sender.tag] = 1
        
        
        if Reachability.isConnectedToNetwork() {
            let buttonPosition = sender.convert(CGPoint.zero, to: frequentlyOrderedCV)
            let indexPath = frequentlyOrderedCV.indexPathForItem(at: buttonPosition)
            let cell = frequentlyOrderedCV.cellForItem(at: indexPath!) as? FrequentlyOrderedCVCell
            
            print(cell?.quantityLbl.text ?? "", "quantity cell")
            let details =  frequentlyOrderModel?.data?[sender.tag]
            addToCartAPI(cid: details?.categoryID ?? "", pid: details?.id ?? "", price: cell?.productPriceLbl.text ?? "", quantityValue: cell?.quantityLbl.text ?? "", weight: cell?.productWeightLbl.text ?? "")
            
            print(cell?.quantityLbl.text ?? "", "quantity cell")
        }
        
    }
    
    @objc func onTapMinBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: frequentlyOrderedCV)
        let indexPath = frequentlyOrderedCV.indexPathForItem(at: buttonPosition)
        let cell = frequentlyOrderedCV.cellForItem(at: indexPath!) as? FrequentlyOrderedCVCell
        quantity = Int(cell?.quantityLbl.text ?? "") ?? 0
        
        if quantity > 1 {
            quantity -= 1
        }else if quantity == 1 {
            
        }
        
        cell?.quantityLbl.text = "\(quantity)"
    }
    
    @objc func onTapMaxBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: frequentlyOrderedCV)
        let indexPath = frequentlyOrderedCV.indexPathForItem(at: buttonPosition)
        let cell = frequentlyOrderedCV.cellForItem(at: indexPath!) as? FrequentlyOrderedCVCell
        
        quantity = Int(cell?.quantityLbl.text ?? "") ?? 0
        quantity += 1
        cell?.quantityLbl.text = "\(quantity)"
    }
    
    @objc func onTapWeightDropDownBtn(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = frequentlyOrderedCV.cellForItem(at: indexPath) as! FrequentlyOrderedCVCell
        
        let details = frequentlyOrderModel?.data?[sender.tag]
        
        cell.loadQuantityData(quantityData: details?.priceWeight?.map{($0.weight ?? "")} ?? [], priceData: details?.priceWeight?.map{($0.price ?? "")} ?? [])
            
        cell.quantityDropDown.show()
    }
    
    
    //MARK:- API
    func userFrequentlyOrders() {
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.uID : uids
        ]
        
        //Indicator.shared().showIndicator(withTitle: "", and: "Loading...", vc: self)
        RestService.serviceCall(url: freqOrderList_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            //  Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(FrequentlyOrderModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.frequentlyOrderModel = responseJson
                
                for _ in 0..<self.frequentlyOrderModel!.data!.count{
                    
                    self.dummyArr.append(0)
                }
                
                DispatchQueue.main.async {
                    self.frequentlyOrderedCV.reloadData()
                }
                
            }else{
                //showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            //showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
    func addToCartAPI(cid:String, pid:String, price:String, quantityValue:String, weight:String) {
        
        guard let uids = userDefault.string(forKey: userDefaults.userID) else {return}
        
        let params = [
            APIParameters.pid : pid,
            APIParameters.cid: cid,
            APIParameters.price:price,
            APIParameters.quantity: quantityValue,
            APIParameters.uID: uids,
            APIParameters.weight: weight
        ]
        
        print("vikas params: \(params)")
        print("UriString: \(addToCart_URL)")
        
        RestService.serviceCall(url: addToCart_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            guard let responseJson = try? JSONDecoder().decode(SuccesMessageModel.self, from: response) else {return}
            
            if responseJson.status == 200 {
                self.userFrequentlyOrders()
            }else{
                print(responseJson.message)
                //showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            print(error.localizedDescription)
            //showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
    }
    
}

//MARK:- Extension
extension FrequentlyOrderedTVCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return frequentlyOrderModel?.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = frequentlyOrderedCV.dequeueReusableCell(withReuseIdentifier: FrequentlyOrderedCVCell.identifier, for: indexPath) as! FrequentlyOrderedCVCell
        
        let details = frequentlyOrderModel?.data?[indexPath.row]
        cell.productImage.sd_setImage(with: URL(string: details?.image ?? ""), placeholderImage: UIImage(systemName: "placeholder1"))
        cell.productNameLbl.text = details?.itemName ?? ""
        cell.productDescriptionLbl.text = details?.datumDescription ?? ""
        cell.productPriceLbl.text = details?.itemPrice ?? ""
        cell.productWeightLbl.text = details?.weight ?? ""
        cell.quantityLbl.text = details?.quantity ?? ""
        
        if dummyArr[indexPath.row] == 1 {
            UIView.animate(withDuration: 0.5) {
                cell.animatedViewWidth.constant = 20
                cell.spinnerImage.image = UIImage(named: "Radio  Check")
            }
        }else {
            cell.animatedViewWidth.constant = 0
        }
        
        if selectedAnimation == indexPath.row {
            cell.startSpin()
        }else {
            cell.stopSpin()
        }
        
        if details?.cartlist ?? "" == "No" {
            
            cell.animatedViewWidth.constant = 0
            cell.checkMarkImage.image = UIImage(named: "")
            cell.spinnerImage.image = UIImage(named: "")
            cell.quantityLbl.text = "1"
            quantity = 1
        }else{
            cell.animatedViewWidth.constant = 20
            cell.startSpin()
            cell.stopSpin()
            cell.quantityLbl.text = details?.cartQuantity ?? ""
            cell.productWeightLbl.text = details?.cartGrams ?? ""
            cell.productPriceLbl.text = details?.cartPrice ?? ""
            quantity = Int(details?.cartQuantity ?? "0") ?? 0
        }
        
        
        cell.addtoCartBtn.tag = indexPath.row
        cell.addtoCartBtn.addTarget(self, action: #selector(onTapAddToCart(_:)), for: .touchUpInside)
        
        cell.minBtn.tag = indexPath.row
        cell.minBtn.addTarget(self, action: #selector(onTapMinBtn(_:)), for: .touchUpInside)
        
        cell.maxBtn.tag = indexPath.row
        cell.maxBtn.addTarget(self, action: #selector(onTapMaxBtn(_:)), for: .touchUpInside)
        
        cell.productWeightBtn.tag = indexPath.row
        cell.productWeightBtn.addTarget(self, action: #selector(onTapWeightDropDownBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (frequentlyOrderedCV.frame.width - (4-1) * 5) / 4
        return CGSize(width: width, height: frequentlyOrderedCV.frame.height)
    }
    
}
