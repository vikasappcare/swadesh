//
//  CategoriesTableViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    class var nibName: String {
        return "\(self)"
    }
    @IBOutlet weak var categoryCOllVw: UICollectionView!
    
    @IBOutlet weak var viewAllBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
