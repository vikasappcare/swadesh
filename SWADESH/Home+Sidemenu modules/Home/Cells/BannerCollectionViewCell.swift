//
//  BannerCollectionViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bannerImgVw: UIImageView!
   
    class var identifier: String {
        return "\(self)"
    }
    class var nib: UINib {
        return UINib(nibName: BannerCollectionViewCell.identifier, bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        bannerImgVw.contentMode = .scaleAspectFill
        // Initialization code
    }

}
