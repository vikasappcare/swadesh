//
//  ItemTableViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit
import DropDown


class ItemTableViewCell: UITableViewCell {
    
    class var identifier: String {
        return "\(self)"
    }
    
    class var nibName: UINib {
        return UINib(nibName: ItemTableViewCell.identifier, bundle: nil)
    }
    
    @IBOutlet weak var itemDescriptionLbl: UILabel!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemImgVw: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var animationVwDynamicWidth: NSLayoutConstraint!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var spinnerImgVw: UIImageView!
    @IBOutlet weak var itemWeightLbl: UILabel!
    @IBOutlet weak var itemWeightBtn: UIButton!
    @IBOutlet weak var quantityDecrementBtn: UIButton!
    @IBOutlet weak var quantityIncrementBtn: UIButton!
    @IBOutlet weak var checkMarkImgVw: UIImageView!
    @IBOutlet weak var quantityLbl: UILabel!
    
    
    var animating = false
    let quantityDropDown = DropDown()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        itemImgVw.contentMode = .scaleAspectFit
    
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadQuantityData(quantityData: [String], priceData: [String]){
        
        print("Drop down called")
        quantityDropDown.anchorView = itemWeightBtn
        quantityDropDown.bottomOffset = CGPoint(x: 0, y: itemWeightBtn.bounds.height)
        quantityDropDown.dataSource = quantityData
        quantityDropDown.selectionAction = { [weak self] (index, item) in
            print("Drop down printed")

            self?.itemWeightLbl.text = item
            self?.priceLbl.text = priceData[index]
        }

    }
    
    //MARK: -  Animation
    
func spin(with options: UIView.AnimationOptions) {
        
        UIView.animate(withDuration: 1.2, delay: 0, options: options, animations: {
            
            self.spinnerImgVw.image = UIImage(named: "Spinner")

            self.spinnerImgVw.transform = self.spinnerImgVw.transform.rotated(by: .pi / 2)
            
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                
                self.stopSpin()
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    
//                    self.checkMarkImgVw.image = UIImage(named: "Radio  Check")

                }
            }
            
        }) { finished in
            
            if finished {
                
                self.checkMarkImgVw.image = UIImage(named: "Radio  Check")

                if self.animating {
                    
                    self.spin(with: .curveLinear)
                    
                } else if options != .curveEaseOut {
                    
                    self.spin(with: .curveEaseOut)
                }
            }
        }
    }
    
    @objc func startSpin() {
        
        if !animating {
            
            animating = true
            spin(with: .curveEaseIn)
        }
    }
    
    func stopSpin() {
        
        animating = false
    }
    
}
