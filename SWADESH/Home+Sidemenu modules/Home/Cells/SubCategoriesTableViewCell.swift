//
//  SubCategoriesTableViewCell.swift
//  IflexHomeScreenSample
//
//  Created by Bhasker on 27/01/21.
//

import UIKit

class SubCategoriesTableViewCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    class var nibName: String {
        return "\(self)"
    }
    @IBOutlet weak var subCategoryCollVw: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
