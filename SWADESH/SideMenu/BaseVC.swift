//
//  BaseVC.swift
//  SWADESH
//
//  Created by Vikas on 19/01/21.
//

import UIKit
import FirebaseInstanceID
import FBSDKLoginKit
import FBSDKCoreKit


class BaseVC: UIViewController, SlideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        
        switch(index){
        case 0:
            //Home
            let vc = storyboard?.instantiateViewController(identifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
        case 1:
            //Profile
            if userDefault.string(forKey: userDefaults.userID) != nil {
                let vc = storyboard?.instantiateViewController(identifier: "ProfileVC") as! ProfileVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = storyboard?.instantiateViewController(identifier: "ReferFriendVC") as! ReferFriendVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case 2:
            //Orders
            if userDefault.string(forKey: userDefaults.userID) != nil {
                let vc = storyboard?.instantiateViewController(identifier: "MyOrdersVC") as! MyOrdersVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = storyboard?.instantiateViewController(identifier: "SettingsVC") as! SettingsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case 3:
            //Address
            if userDefault.string(forKey: userDefaults.userID) != nil {
                let vc = storyboard?.instantiateViewController(identifier: "AddressListVC") as! AddressListVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{}
            
            break
        case 4:
            //Refer a friend
            if userDefault.string(forKey: userDefaults.userID) != nil {
                let vc = storyboard?.instantiateViewController(identifier: "ReferFriendVC") as! ReferFriendVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
                let nav = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .fullScreen
                nav.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(nav, animated: true)
            }
            
            break
        case 5:
            //Wishlist
            if userDefault.string(forKey: userDefaults.userID) != nil {
                let vc = storyboard?.instantiateViewController(identifier: "WishlistVC") as! WishlistVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{}
            
            break
        case 6:
            //Settings
            if userDefault.string(forKey: userDefaults.userID) != nil {
                let vc = storyboard?.instantiateViewController(identifier: "SettingsVC") as! SettingsVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{}
            
            break
        case 8:
            //Logout
            if userDefault.string(forKey: userDefaults.userID) != nil {
                let alert = UIAlertController(title: "Alert", message: "Would you like to Logout your Account?", preferredStyle: UIAlertController.Style.alert)
                
                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.modalPresentationStyle = .fullScreen
                    nav.modalTransitionStyle = .crossDissolve
                    
                    UserDefaults.standard.set(nil, forKey: userDefaults.userID)
                    
                    self.navigationController?.pushViewController(vc, animated: false)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
                    action in
                    
                    self.navigationController?.popViewController(animated: true)
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }else{}
            
            break
        default:
            print("default\n", terminator: "")
            
        }
    }
    
    func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButton.ButtonType.system)
        btnShowMenu.addTarget(self, action: #selector(BaseVC.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //navigationController?.navigationBar.isHidden = false
    }
    
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        
        //navigationController?.navigationBar.isHidden = true
        
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuVC = self.storyboard!.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        menuVC.btnMenu = sender
        menuVC.delegate = self
        
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
        
    }
    
    // MARK: -  Reset UserDefaults
    
    func resetUserDefaults() {
        
        print("BEGIN:Swadesh ======> userDefaults successfully removed from the app")
        
        LoginManager().logOut()
        
        DispatchQueue.main.async {
            
            let instance = InstanceID.instanceID()
            instance.deleteID { (error) in
                
                if error != nil
                {
                    let ERROR_MESSAGE = error?.localizedDescription ?? ""
                    
                    print("Instance ID Error : \(ERROR_MESSAGE)")
                }
                else
                {
                    print("FCM TOKEN Deleted Successfully");
                }
            }
        }

        let userDefaults = UserDefaults.standard
        let dict = userDefaults.dictionaryRepresentation()
        
        dict.keys.forEach { key in
            
            userDefaults.removeObject(forKey: key)
        }
    }
    
}

