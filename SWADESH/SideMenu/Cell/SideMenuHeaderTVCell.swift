//
//  SideMenuHeaderTVCell.swift
//  SWADESH
//
//  Created by Vikas on 19/01/21.
//

import UIKit

class SideMenuHeaderTVCell: UITableViewCell {

    class var identifier: String {
        return "\(self)"
    }
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var backView: CardView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        print(backView.frame.size.height, backView.frame.size.width)
        backView.layer.cornerRadius = backView.frame.width / 2
        userImage.layer.cornerRadius = userImage.frame.width / 2
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
