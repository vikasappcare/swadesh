//
//  MenuVC.swift
//  SWADESH
//
//  Created by Vikas on 19/01/21.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}


class MenuVC: UIViewController {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var btnCloseMenuOverlay : UIButton!
    
    var arrayMenuOptions = [Dictionary<String,String>]()
    var arrayMenuOptions1 = [Dictionary<String,String>]()
    var btnMenu : UIButton!
    
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
        updateArrayMenu()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //navigationController?.isNavigationBarHidden = false

    }
    
    func updateArrayMenu() {
        
        arrayMenuOptions.append(["title":"Home", "icon":"Home"])
        arrayMenuOptions.append(["title":"Profile", "icon":"Profile"])
        arrayMenuOptions.append(["title":"Orders", "icon":"Orders"])
        arrayMenuOptions.append(["title":"Address", "icon":"Address"])
        arrayMenuOptions.append(["title":"Refer a friend", "icon":"Refer"])
        arrayMenuOptions.append(["title":"Wish list", "icon":"Wishlist"])
        arrayMenuOptions.append(["title":"Settings", "icon":"Settings"])
        arrayMenuOptions.append(["title":"", "icon":""])
        arrayMenuOptions.append(["title":"Log Out", "icon":"Logout"])
        arrayMenuOptions.append(["title":"", "icon":""])
    }
    
    func updateArrayMenu1() {
        //before login
        arrayMenuOptions1.append(["title":"Home", "icon":"Home"])
        arrayMenuOptions1.append(["title":"Refer a friend", "icon":"Refer"])
        arrayMenuOptions1.append(["title":"Settings", "icon":"Settings"])
        arrayMenuOptions1.append(["title":"", "icon":""])
        arrayMenuOptions1.append(["title":"Log In", "icon":"Logout"])
        arrayMenuOptions.append(["title":"", "icon":""])
    }
    
    @IBAction func onCloseMenuClick(_ button: UIButton!){
        
        btnMenu.tag = 0
        //navigationController?.navigationBar.isHidden = false
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
        
    }
    
    
}

//MARK:- Extension
extension MenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else {
            if userDefault.string(forKey: userDefaults.userID) != nil {
                return arrayMenuOptions.count
            }else{
                return arrayMenuOptions1.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = table.dequeueReusableCell(withIdentifier: SideMenuHeaderTVCell.identifier, for: indexPath) as! SideMenuHeaderTVCell
            cell.userImage.sd_setImage(with: URL(string: userDefault.string(forKey: userDefaults.profilePic) ?? ""), placeholderImage: #imageLiteral(resourceName: "user-gray"))
            cell.userName.text = userDefault.string(forKey: userDefaults.fullName) ?? ""
            return cell
            
        }else {
            let cell = table.dequeueReusableCell(withIdentifier: SideMenuListTVCell.identifier, for: indexPath) as! SideMenuListTVCell
            
            cell.listName.text = arrayMenuOptions[indexPath.row]["title"]
            cell.listImage.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
            
            if userDefault.string(forKey: userDefaults.userID) != nil {
                if arrayMenuOptions[indexPath.row]["icon"] == "" {
                    if indexPath.row == 7 {
                        cell.bottomView.isHidden = false
                    }else{
                        cell.bottomView.isHidden = true
                    }
                }else{
                    cell.bottomView.isHidden = true
                }
            }else{
                if arrayMenuOptions[indexPath.row]["icon"] == "" {
                    if indexPath.row == 3 {
                        cell.bottomView.isHidden = false
                    }else{
                        cell.bottomView.isHidden = true
                    }
                }else{
                    cell.bottomView.isHidden = true
                }
            }
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
        
    }
    
}
