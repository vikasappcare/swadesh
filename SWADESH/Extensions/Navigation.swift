//
//  Navigation.swift
//  SWADESH
//
//  Created by Vikas on 12/01/21.
//

import UIKit

extension UINavigationController {
    
    func backToViewController(vc: Any) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}

extension UIViewController {
    //MARK:- Navigation title name
    public func navTitle(heading:String) {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        titleLabel.textColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        titleLabel.font = UIFont(name: Fonts.regular, size: 18)
        titleLabel.text = heading
        navigationItem.titleView = titleLabel
    }
}


