//
//  Alerts.swift
//  SWADESH
//
//  Created by Vikas on 08/01/21.
//

import UIKit

public func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
    let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
    vc.present(alert, animated: true, completion: nil)
    
    DispatchQueue.main.asyncAfter(deadline: .now()+2) {
        alert.dismiss(animated: true, completion: nil)
    }
}

