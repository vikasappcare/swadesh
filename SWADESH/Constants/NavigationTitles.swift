//
//  NavigationTitles.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import Foundation

struct Title {
    static let registration = "Registration"
    static let forgotPassword = "Forgot Password"
    static let otp = "Otp Screen"
    static let NewPassword = "New Password"
    static let changePassword = ""
    static let categories = "Categories"
    static let cart = "Cart"
    static let profile = "General Profile"
    static let orderHistory = "Order History"
    static let orderDetails = "Order Details"
    static let orderCancel = "Cancel Order"
    static let orderreturn = "Return an Item"
    static let address = "Address"
    static let newAddress = "Add New Address"
    static let refer = "Refer a friend"
    static let wishlist = "Wish list"
    static let search = "Search"
    static let settings = "Settings"
    static let tandC = "Terms and conditions"
    static let privacypolicy = "Privacy Policy"
    static let faq = "Faq"
    static let paymentSummary = "Payment Summary"
    static let frquentlyOrdered = "Frequently Ordered"
    static let similarItems = "Similar Items"
}
