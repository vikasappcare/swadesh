//
//  AppColorFontName.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import Foundation

struct Fonts {
    
    static let black = "RobotoSlab-Black"
    static let bold = "RobotoSlab-Bold"
    static let extraBold = "RobotoSlab-ExtraBold"
    static let extraLight = "RobotoSlab-ExtraLight"
    static let light = "RobotoSlab-Light"
    static let medium = "RobotoSlab-Medium"
    static let regular = "RobotoSlab-Regular"
    static let semiBold = "RobotoSlab-SemiBold"
    static let thin = "RobotoSlab-Thin"
    
}
