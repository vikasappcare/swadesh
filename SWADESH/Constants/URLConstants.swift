//
//  URLConstants.swift
//  SWADESH
//
//  Created by Vikas on 05/01/21.
//

import Foundation

struct API {
    static let content:String = "Content-Type"
    static let type:String = "application/json"
    static let key:String = "API-KEY"
    static let value:String = "827ccb0eea8a706c4c34a16891f84e7b"
}


//MARK:- Base URL
let BaseURL = "http://159.65.145.170/iflex/index.php/"

//Login, Register, New Pswd & Forgot Password
let login_URL = BaseURL+"GeneralServices/login"
let register_URL = BaseURL+"GeneralServices/register"
let forgotPswd_URL = BaseURL+"GeneralServices/Forget_password"
let otp_URL = BaseURL+"GeneralServices/check_otp"
let newPswd_URL = BaseURL+"GeneralServices/password_update"
let changePswd_URL = BaseURL+"GeneralServices/change_password"
//Profile
let profile_URL = BaseURL+"GeneralServices/userProfile"
let profileUpdate_URL = BaseURL+"GeneralServices/profile_update"
let profilePic_URL = BaseURL+"GeneralServices/profile_photo_update"
//Address
let addressList_URL = BaseURL+"GeneralServices/getuserAddress"
let addressAdd_URL = BaseURL+"GeneralServices/addressAdd"
let addressEdit_URL = BaseURL+"GeneralServices/addressEdit"
let addressDelete_URL = BaseURL+"GeneralServices/userAddressDelete"

//Home
let bannerList_URL = BaseURL+"GeneralServices/banners"
let quantityList_URL = BaseURL+"GeneralServices/quantity"
let newArrivalsList_URL = BaseURL+"GeneralServices/arrival"
let productsList_URL = BaseURL+"GeneralServices/fetcharrivalItems"
let addToCart_URL = BaseURL+"GeneralServices/addtoCart"

//Frequently ordered items
let freqOrderList_URL = BaseURL+"GeneralServices/frequentlyOrderItems"
//Search
let searchedList_URL = BaseURL+"GeneralServices/searchproduct"

//Categories & id
let categoryList_URL = BaseURL+"GeneralServices/fetchCategories"
var categoryIDString = String()
let singleCategoryList_URL = BaseURL+"GeneralServices/fetchCategoriesItems"
var productIDString = String()
let singleProductDetails_URL = BaseURL+"GeneralServices/fetchItemsDetails"
let addProductToWishList_URL = BaseURL+"GeneralServices/whishlist"
let similarProduct_URL = BaseURL+"GeneralServices/getsimilarproducts"
let usersWishListURL = BaseURL+"GeneralServices/getwishlist"
let deleteWishList_URL = BaseURL+"GeneralServices/deletewishList"

//Cart
let cartProductsList_URL = BaseURL+"GeneralServices/getCartProducts"
let deleteCartProductURL = BaseURL+"GeneralServices/deleteCart"
let getTaxesURL = BaseURL+"GeneralServices/taxes"
let getDeliveryChargesURL = BaseURL+"GeneralServices/deliverycharges"
let getTimeSlotsURL = BaseURL+"GeneralServices/timeslots"
let getUserCreditPointsURL = BaseURL+"GeneralServices/userpoints"
let orderCreateURL = BaseURL+"Orders/OrderCreate"
let cartCouponURL = BaseURL+"GeneralServices/totalaftercoupenapply"
//Services
let tandC_URL = BaseURL+"GeneralServices/terms_and_conditions"
let privacy_URL = BaseURL+"GeneralServices/privicypolicy"
let aboutUs_URL = BaseURL+"GeneralServices/about_us"
let faq_URL = BaseURL+"GeneralServices/faqs"

//Orders return
let itemReturnList_URL = BaseURL+"GeneralServices/return_reasons"
let itemReturn_URL = BaseURL+"GeneralServices/return_product_data"

//Order Cancellation
let orderCancelList_URL = BaseURL+"GeneralServices/cancel_reasons"
let orderCancel_URL = BaseURL+"GeneralServices/cancel_order"

//Order Details
let orderDetails_URL = BaseURL+"Orders/orderDetails"
let viewOrders_URL = BaseURL+"Orders/viewOrders"
let repeatOrder_URL = BaseURL+"GeneralServices/repeatOrder"

//Refer a friend with emailID
let referEmailID_URL = BaseURL+"GeneralServices/referFriend"
let referalCode_URL = BaseURL+"GeneralServices/getreferalcode"
let referalPoints_URL = BaseURL+"GeneralServices/userpoints"
