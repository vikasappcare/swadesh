//
//  AlertMessages.swift
//  SWADESH
//
//  Created by Vikas on 08/01/21.
//

import Foundation

//MARK:- UserDefaults
struct userDefaults {
    static let userID = "userid"
    static let profilePic = "profilepic"
    static let fullName = "fullName"
    static let email = "email"

}

//MARK:- Network
struct Network {
    static let title = "Network"
    static let message = "Please check your connection!"
}

//MARK:- Alerts
struct Alerts {
    
    //All fields empty
    static let allFields = "Fields are Empty"
    
    //Login
    static let emailID = "Email-ID field is Empty"
    static let emailFormat = "Email-ID format is incorrect"
    static let mobileFormat = "Mobile Number format is incorrect"
    static let mobileNoCount = "Mobile Number must be 10"
    static let password = "Password field is Empty"
    
    //Signup
    static let firstName = "FirstName field is Empty"
    static let lastName = "LastName field is Empty"
    static let mobileNo = "Mobile number field is Empty"
    static let referral = "Referral Code field is Empty"
    
    //New Password
    static let newPswd = "New password field is Empty"
    static let confirmNewPswd = "Re-enter new password field is Empty"
    
    //Change Password
    static let oldPswd = "Old password field is Empty"
    static let samePswd = "Passwords do not match"
    
    //New Address
    static let street1 = "Street1 field is Empty"
    static let street2 = "Street2 field is Empty"
    static let city = "City field is Empty"
    static let state = "State field is Empty"
    static let zipCode = "ZipCode field is Empty"
    static let addrName = "Address name field is Empty"
    
    //CART
    static let deliveryDate = "Please select delivery date"
    static let deliveryTime = "Please select delivery Time"

    //Return an Item
    static let returnReasonSelection = "Select reason for returning item"
    static let returnReasonText = "Enter the reason for return"
    static let returnImagesUpload = "Upload images of return Items"
    
    //Cancel an Item
    static let cancelReasonSelection = "Select reason for Cancellation of order"
    static let cancelReasonText = "Enter the reason for cancellation"
    
}



