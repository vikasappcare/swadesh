//
//  APIParameters.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import Foundation

struct APIParameters {
    
    static let uID:String = "uid"
    static let userName:String = "username"
    static let password:String = "password"
    static let firstName:String = "first_name"
    static let lastName:String = "last_name"
    static let emailID:String = "email_id"
    static let email:String = "email"
    static let mobile:String = "mobile"
    static let carrierCode:String = "c_code"
    static let referal_code: String = "referal_code"
    static let otp:String = "otp"
    static let cpassword:String = "cpassword"
    static let newpassword:String = "curentpassword"
    static let loginType:String = "LoginType"
    
    static let key:String = "key"
    static let id:String = "id"
    static let addressType:String = "address_type"
    static let address:String = "address"
    static let street1:String = "street1"
    static let street2:String = "street2"
    static let city:String = "city"
    static let state:String = "state"
    static let latitude:String = "ltd"
    static let longitude:String = "lng"
    static let postalcode:String = "postalcode"
    
    static let productid:String = "productid"
    static let product_id:String = "product_id"
    static let category_id: String = "category_id"
    static let arrival_id: String = "arrival_id"
    static let pid: String = "pid"
    static let cid: String = "cid"
    static let price: String = "price"
    static let quantity: String = "quantity"
    static let weight: String = "weight"
    static let date: String = "date"
    static let product_arr: String = "product_arr"
    static let payment_type: String = "payment_type"
    static let payment_status: String = "payment_status"
    static let order_status: String = "order_status"
    static let order_amount: String = "order_amount"
    static let discount: String = "discount"
    static let delivery_time: String = "delivery_time"
    static let gst: String = "gst"
    static let address_id: String = "address_id"
    static let delivery_type: String = "delivery_type"
    static let delivery_charge: String = "delivery_charge"
    static let delivery_date: String = "delivery_date"
    static let credits_used: String = "credits_used"
    static let order_id:String = "order_id"
    static let reason_id:String = "reason_id"
    static let reason_text:String = "reason_text"
    static let return_id:String = "return_id"
    static let return_text:String = "return_text"
    static let orderType:String = "orderType"
    static let coupon_code: String = "coupon_code"
    static let total:String = "total"
    static let coupen_code:String = "coupen_code"
    
    //common
    static let deviceToken:String = "devicetoken"
    static let deviceType:String = "devicetype"
    static let deviceId:String = "deviceid"
    //response
    static let tokenD = "11jhbjjxddwkjwq dkcjde"
    static let device:String = "ios"
    static let idDev = "12345678"
}

