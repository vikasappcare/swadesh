//
//  LoginVC.swift
//  SWADESH
//
//  Created by Vikas on 08/01/21.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class LoginVC: UIViewController, GIDSignInDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var emailIDTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var passwordEyeBtn: UIButton!
    
    //MARK:- Models
    var loginModel:LoginModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        emailIDTF.text = "vikas@appcare.co.in"
//        passwordTF.text = "vikas123"
        passwordEyeBtn.setImage(#imageLiteral(resourceName: "EyeClosed"), for: .normal)
        passwordTF.isSecureTextEntry = true
         
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navTitle(heading: "")
        navigationItem.hidesBackButton = true
    }
    
    //MARK:- Selector
    //Password security
    @IBAction func onTapHideUnHide(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            passwordEyeBtn.setImage(#imageLiteral(resourceName: "Eye"), for: .normal)
            passwordTF.isSecureTextEntry = false
        }else{
            passwordEyeBtn.setImage(#imageLiteral(resourceName: "EyeClosed"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
        
    }
    
    @IBAction func onTapForgotPassword(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        vc.comingFrom = "Login"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onTapLogin(_ sender: Any) {
        
        if emailIDTF.text!.isEmpty && passwordTF.text!.isEmpty {
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.allFields)
        }else{
            if emailIDTF.text!.isEmpty {
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.emailID)
            }else{
                if emailIDTF.text!.isValidEmail() {
                    if passwordTF.text!.isEmpty {
                        showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.password)
                    }else{

                        if Reachability.isConnectedToNetwork() {
                            userLogin(emailID: emailIDTF.text ?? "", password: passwordTF.text ?? "", loginType: "Native")
                        }else{
                            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
                        }
                    }
                }else{
                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.emailFormat)
                }
            }
        }
    }
    
    
    @IBAction func onTapRegister(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- Social Logins
    @IBAction func onTapFacebook(_ sender: Any) {
        
        let loginManager = LoginManager()
        
        if let _ = AccessToken.current {
            // Access token available -- user already logged in
            // Perform log out
            
            loginManager.logOut()
            //updateButton(isLoggedIn: false)
           // updateMessage(with: nil)
            
        } else {
            // Access token not available -- user already logged out
            // Perform log in
            
            loginManager.logIn(permissions: [], from: self) { [weak self] (result, error) in
                
                // Check for error
                guard error == nil else {
                    // Error occurred
                    print(error!.localizedDescription)
                    return
                }
                
                // Check for cancel
                guard let result = result, !result.isCancelled else {
                    print("User cancelled login")
                    return
                }
                // Successfully logged in
                //self?.updateButton(isLoggedIn: true)
                
                Profile.loadCurrentProfile { (profile, error) in
                    print(profile)
                   // self?.updateMessage(with: Profile.current?.name)
                }
               
                guard let accessToken = FBSDKLoginKit.AccessToken.current else { return }
                let graphRequest = FBSDKLoginKit.GraphRequest(graphPath: "me",
                                                              parameters: ["fields": "email, name"],
                                                              tokenString: accessToken.tokenString,
                                                              version: nil,
                                                              httpMethod: .get)
                
                graphRequest.start { (connection, result, error) -> Void in
                    if error == nil {
                        print("result \(result)")
                        let name: [String:String] = result as! [String : String]
                        print("result \(name["email"])")
//                        if Reachability.isConnectedToNetwork() {
//                            self?.loginVMVar.vc = self
//                            self?.showHud("")
//                            self!.loginVMVar.getAllUsreDataAF(email: name["email"] ?? "", name: name["name"] ?? "", password: "", deviceId: UserDefaults.standard.string(forKey: "deviceID") ?? "", deviceType: "iOS", loginType: "Facebook", profilePic: "", mobile: "")
//                         }else {
//                            self?.showAlert(title: Network.title, message: Network.message)
//                         }
                    }
                    else {
                        print("error \(error)")
                    }
                }
            }
        }
        
    }
    
    @IBAction func onTapGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK - Google Sign In
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          print("The user has not signed in before or they have since signed out.")
        } else {
          print("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
//      let userId = user.userID                  // For client-side use only!
//      let idToken = user.authentication.idToken // Safe to send to the server
      let fullName = user.profile.name
//      let givenName = user.profile.givenName
//      let familyName = user.profile.familyName
        let email = user.profile.email
//        if user.profile.hasImage
//        {
//            let pic = user.profile.imageURL(withDimension: 100)
//            print(pic)
//        }
    
        
        //API
//        if Reachability.isConnectedToNetwork() {
//           loginVMVar.vc = self
//           self.showHud("")
//           loginVMVar.getAllUsreDataAF(email: email!, name: fullName!, password: "", deviceId: UserDefaults.standard.string(forKey: "deviceID") ?? "", deviceType: "iOS", loginType: "Google", profilePic: "", mobile: "")
//       }else {
//           self.showAlert(title: NSLocalizedString("Network", comment: ""), message: NSLocalizedString("Networkmessage", comment: ""))
//       }

      // ...
 
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
    }
    
}

//MARK:- API Call
extension LoginVC {
    
    func userLogin(emailID:String, password:String, loginType:String) {
        
        let params = [
            APIParameters.userName : emailID,
            APIParameters.password : password,
            APIParameters.deviceToken : APIParameters.tokenD,
            APIParameters.deviceType : APIParameters.device,
            APIParameters.deviceId : APIParameters.idDev,
            APIParameters.loginType : loginType // Native or Google or Facebook
        ]
        
        print(params)
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: login_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else{return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(LoginModel.self, from: response) else{return}
            
            
            if responseJson.status == 200 {
                print(responseJson.data?[0].id)
                
                var userDetails = [String:String]()
                userDetails.updateValue(responseJson.data?[0].id ?? "", forKey: "uid")
                userDetails.updateValue(responseJson.data?[0].email ?? "", forKey: "emailID")
                userDetails.updateValue(responseJson.data?[0].firstName ?? "" + (responseJson.data?[0].lastName)!, forKey: "name")
                
                userDefault.setValue(userDetails, forKey: Details.userKey)
                userDefault.setValue(responseJson.data?[0].uid ?? "", forKey: userDefaults.userID)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
            
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }
        
    }
    
}
