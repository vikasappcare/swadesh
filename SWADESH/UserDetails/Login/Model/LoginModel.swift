//
//  LoginModel.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import Foundation

//Login Model
struct LoginModel: Codable {
    let status: Int
    let message: String
    let data: [DataLogin]?
}

// MARK: - Datum
struct DataLogin: Codable {
    let id, firstName, uid, lastName: String?
    let pPic, email, referalEmail, mobile: String?
    let cCode, otpStatus, uotp, password: String?
    let userStatus, deviceID, deviceToken, deviceType: String?
    let referalCode, points, cd: String?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case uid
        case lastName = "last_name"
        case pPic = "p_pic"
        case email
        case referalEmail = "referal_email"
        case mobile
        case cCode = "c_code"
        case otpStatus = "otp_status"
        case uotp, password
        case userStatus = "user_status"
        case deviceID = "device_id"
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case referalCode = "referal_code"
        case points, cd, image
    }
}
