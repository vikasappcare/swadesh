//
//  OTPVC.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class OTPVC: UIViewController {
    
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var tf4: UITextField!
    @IBOutlet weak var timerLbl: UILabel!
    
    //Model
    var otpModel:OTPModel?
    
    //timer count
    var counter = 30
    var comingFrom = String()
    var emailID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.otp)
        textFieldSetUp()
    }
    
    func textFieldSetUp() {
        
        tf1.addTarget(self, action: #selector(textdidChange(_:)), for: .editingChanged)
        tf2.addTarget(self, action: #selector(textdidChange(_:)), for: .editingChanged)
        tf3.addTarget(self, action: #selector(textdidChange(_:)), for: .editingChanged)
        tf4.addTarget(self, action: #selector(textdidChange(_:)), for: .editingChanged)
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tf1.becomeFirstResponder()
    }
    
    
    
    //MARK:- selector
    //timer
    @objc func timerAction() {
        if counter > 0 {
            timerLbl.text = "\(counter) sec"
            counter -= 1
        }else if counter == 0 {
            
        }
    }
    
    //textfields
    @objc func textdidChange(_ textField: UITextField) {
        let text = textField.text
        if text?.utf16.count == 1 {
            switch textField {
            case tf1:
                tf2.becomeFirstResponder()
            case tf2:
                tf3.becomeFirstResponder()
            case tf3:
                tf4.becomeFirstResponder()
            case tf4:
                tf4.resignFirstResponder()
            default:
                break
            }
        }
    }
    
    @IBAction func onTapSendOTP(_ sender: Any) {
        
        if tf1.text!.isEmpty || tf2.text!.isEmpty || tf3.text!.isEmpty || tf4.text!.isEmpty {
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.allFields)
        }else {
            if Reachability.isConnectedToNetwork() {
                let otp0 = tf1.text! + tf2.text!
                let otp1 = tf3.text! + tf4.text!
                let otps = otp0+otp1
                print(otps)
                userOTP(otp: "\(otps)")
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
            }
        }
        
    }
    
}

extension OTPVC {
    
    func userOTP(otp: String) {
        
        let params = [
            APIParameters.email: emailID,
            APIParameters.otp: otp
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: otp_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(OTPModel.self, from: response) else{return}
            
            print(responseJson)
            if responseJson.status == 200 {
                print("success")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewPasswordVC") as! NewPasswordVC
                vc.comingFrom = self.comingFrom
                vc.emailID = self.emailID
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

        
    }
    
}
