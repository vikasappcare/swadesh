//
//  NewPasswordVC.swift
//  SWADESH
//
//  Created by Vikas on 11/01/21.
//

import UIKit

class NewPasswordVC: UIViewController {

    @IBOutlet weak var newPswdTF: UITextField!
    @IBOutlet weak var confirmPswdTF: UITextField!
    
    var comingFrom = String()
    var emailID = String()
    //Model
    var newPasswordModel:NewPasswordModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.NewPassword)
    }
    
    //MARK:- selector
    @IBAction func onTapChangePswd(_ sender: Any) {
        
        if newPswdTF.text!.isEmpty && confirmPswdTF.text!.isEmpty {
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.allFields)
        }else{
            
            if newPswdTF.text!.isEmpty{
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.newPswd)
            }else{
                if confirmPswdTF.text!.isEmpty{
                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.confirmNewPswd)
                }else{
                    if newPswdTF.text != confirmPswdTF.text {
                        showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.samePswd)
                    }else{
                        if Reachability.isConnectedToNetwork(){
                            userNewPswd()
                        }else{
                            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
                        }
                    }
                }
            }
            
        }
        
    }
    
}

extension NewPasswordVC {
    
    func userNewPswd() {
        let params = [
            APIParameters.email: emailID,
            APIParameters.password: newPswdTF.text ?? "",
            APIParameters.cpassword: confirmPswdTF.text ?? ""
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: newPswd_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(NewPasswordModel.self, from: response) else{return}
            
            
            if responseJson.status == 200 {
                if self.comingFrom == "Login" {
                    self.navigationController?.backToViewController(vc: LoginVC.self)
                }else{
                    self.navigationController?.backToViewController(vc: SettingsVC.self)
                }
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

        
    }
    
}
