//
//  ForgotPasswordVC.swift
//  SWADESH
//
//  Created by Vikas on 08/01/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    var comingFrom = String()
    
    @IBOutlet weak var emailIDTF: UITextField!
    
    //Model
    var forgotPswdModel:ForgotPswdModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.forgotPassword)
    }
    
    @IBAction func onTapSendOTP(_ sender: Any) {
        
        if emailIDTF.text!.isEmpty {
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.emailID)
        }else{
            if Reachability.isConnectedToNetwork() {
                userForgotPswd()
            }else{
                showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
            }
        }
        
    }
    
}

extension ForgotPasswordVC {
    
    func userForgotPswd() {
        let params = [
            APIParameters.email: emailIDTF.text ?? ""
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: forgotPswd_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(ForgotPswdModel.self, from: response) else{return}
            
            
            if responseJson.status == 200 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                vc.comingFrom = self.comingFrom
                vc.emailID = self.emailIDTF.text ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

        
    }
    
}
