//
//  ForgotPswdModel.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import Foundation

struct ForgotPswdModel: Codable {
    let status: Int
    let message: String
    let data: [DataForgot]?
}

// MARK: - Datum
struct DataForgot: Codable {
    let id, firstName, uid, lastName: String?
    let pPic, email, mobile, cCode: String?
    let otpStatus, uotp, address2, address1: String?
    let password, userStatus, deviceID, deviceToken: String?
    let deviceType, cd: String?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case uid
        case lastName = "last_name"
        case pPic = "p_pic"
        case email, mobile
        case cCode = "c_code"
        case otpStatus = "otp_status"
        case uotp
        case address2 = "address_2"
        case address1 = "address_1"
        case password
        case userStatus = "user_status"
        case deviceID = "device_id"
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case cd
    }
}
