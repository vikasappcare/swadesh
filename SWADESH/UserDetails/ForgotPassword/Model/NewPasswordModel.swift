//
//  NewPasswordModel.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import Foundation

struct NewPasswordModel: Codable {
    let status: Int
    let message: String
}
