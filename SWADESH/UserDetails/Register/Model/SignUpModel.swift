//
//  SignUpModel.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import Foundation

struct SignUpModel: Codable {
    let status: Int
    let message: String
    let date: DataSignUp?
}

// MARK: - DateClass
struct DataSignUp: Codable {
    let firstName, lastName, password, email: String?
    let mobile, cCode, deviceType, deviceToken: String?
    let deviceID, uid, userStatus, referalCode: String?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case password, email, mobile
        case cCode = "c_code"
        case deviceType = "device_type"
        case deviceToken = "device_token"
        case deviceID = "device_id"
        case uid
        case referalCode = "referal_code"
        case userStatus = "user_status"
    }
}
