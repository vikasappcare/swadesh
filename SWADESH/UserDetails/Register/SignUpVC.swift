//
//  SignUpVC.swift
//  SWADESH
//
//  Created by Vikas on 08/01/21.
//

import UIKit

class SignUpVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var emailIDTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var referralTF: UITextField!
    @IBOutlet weak var pswdEyeBtn: UIButton!
    
    //MARK:- Models
    var signUpModel:SignUpModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle(heading: Title.registration)
        pswdEyeBtn.setImage(#imageLiteral(resourceName: "EyeClosed"), for: .normal)
        passwordTF.isSecureTextEntry = true
    }
    
    //MARK:- Selector
    @IBAction func onTapPswdEye(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            pswdEyeBtn.setImage(#imageLiteral(resourceName: "Eye"), for: .normal)
            passwordTF.isSecureTextEntry = false
        }else{
            pswdEyeBtn.setImage(#imageLiteral(resourceName: "EyeClosed"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
        
    }
    
    @IBAction func onTapLogin(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onTapRegister(_ sender: Any) {
        
        if firstNameTF.text!.isEmpty && lastNameTF.text!.isEmpty && mobileTF.text!.isEmpty && emailIDTF.text!.isEmpty && passwordTF.text!.isEmpty && referralTF.text!.isEmpty {
            
            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.allFields)
        }else{
            if firstNameTF.text!.isEmpty {
                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.firstName)
            }else{
                if lastNameTF.text!.isEmpty {
                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.lastName)
                }else{
                    if mobileTF.text!.isEmpty {
                        showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.mobileNo)
                    }else{
                        if mobileTF.text!.count > 9 && mobileTF.text!.count < 11  {
                            if emailIDTF.text!.isEmpty {
                                showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.emailID)
                            }else{
                                if emailIDTF.text!.isValidEmail() {
                                    if passwordTF.text!.isEmpty {
                                        showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.password)
                                    }else{
                                        if Reachability.isConnectedToNetwork() {
                                            userRegistration()
                                        }else{
                                            showAlertMessage(vc: self, titleStr: Network.title, messageStr: Network.message)
                                        }
                                    }
                                }else{
                                    showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.emailFormat)
                                }
                            }
                        }else{
                            showAlertMessage(vc: self, titleStr: "", messageStr: Alerts.mobileNoCount)
                        }
                    }
                }
            }
        }
        
    }
    
}

//MARK:- API
extension SignUpVC {
    
    func userRegistration() {
        
        let params = [
            APIParameters.firstName: firstNameTF.text ?? "",
            APIParameters.lastName: lastNameTF.text ?? "",
            APIParameters.email: emailIDTF.text ?? "",
            APIParameters.mobile: mobileTF.text ?? "",
            APIParameters.deviceToken: APIParameters.tokenD,
            APIParameters.deviceType: APIParameters.device,
            APIParameters.deviceId: APIParameters.idDev,
            APIParameters.password: passwordTF.text ?? "",
            APIParameters.carrierCode: "91",
            APIParameters.referal_code: referralTF.text ?? ""
        ]
        
        Indicator.shared().showIndicator(withTitle: "", and: "Loading..", vc: self)
        RestService.serviceCall(url: register_URL, method: .post, parameter: params) {[weak self] (response) in
            guard let self = self else {return}
            Indicator.shared().hideIndicator(vc: self)
            guard let responseJson = try? JSONDecoder().decode(SignUpModel.self, from: response) else{return}
            
            if responseJson.status == 200 {
                
                var userDetails = [String:String]()
                userDetails.updateValue(responseJson.date?.uid ?? "", forKey: "uid")
                userDetails.updateValue(responseJson.date?.email ?? "", forKey: "emailID")
                userDetails.updateValue(responseJson.date?.firstName ?? "" + (responseJson.date?.lastName)!, forKey: "name")
                
                userDefault.setValue(userDetails, forKey: Details.userKey)
                userDefault.setValue(responseJson.date?.uid ?? "", forKey: userDefaults.userID)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                showAlertMessage(vc: self, titleStr: "", messageStr: responseJson.message)
            }
            
        } failure: { (error) in
            Indicator.shared().hideIndicator(vc: self)
            showAlertMessage(vc: self, titleStr: "", messageStr: error.localizedDescription)
        }

        
    }
    
}
