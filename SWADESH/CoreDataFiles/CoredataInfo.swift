//
//  CoredataInfo.swift
//  SWADESH
//
//  Created by Vikas on 08/02/21.
//

import Foundation
import UIKit
import CoreData

struct CoredataInfo {
    
    var productListArray:[NSManagedObject] = []
    var indexList:[NSManagedObject] = []
    var totalArr = [Int]()
    
    //MARK:- Save Data to Coredata
    func saveProductDetailsDB(pname: String,prdtId:String,cids:String,qty:String,image:String,prefe:String, prefName: String,price:String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Products", in: managedContext)!
        let userName = NSManagedObject(entity: entity, insertInto: managedContext)
        userName.setValue(pname, forKeyPath: "productName")
        userName.setValue(prdtId, forKeyPath: "productId")
        userName.setValue(cids, forKeyPath: "categoryID")
        userName.setValue(qty, forKeyPath: "quantity")
        userName.setValue(image, forKeyPath: "productImage")
        userName.setValue(price, forKeyPath: "productPrice")
        var userArray: [NSManagedObject] = []
        
        do {
            try managedContext.save()
            //            dismiss(animated: false, completion: nil)
            //            self.delegate?.didTapCart(tagValue: self.cids)
            userArray.append(userName)
            print(userArray)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //MARK:- Fetch entire details from CoreData
    mutating func fetchProductListResult() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Product")
        
        do {
            productListArray = try managedContext.fetch(fetchRequest)
            print(productListArray)
            
            if productListArray.count > 0 {
                totalArr.removeAll()
                for i in 0..<productListArray.count {
                    
                    let price = (productListArray[i].value(forKey: "price") as? String as! NSString).integerValue * (productListArray[i].value(forKey: "qty") as? String as! NSString).integerValue
                    print(price, "price")
                    totalArr.append(price)
                }
                var total = 0
                if totalArr.count > 0 {
                    var values = 0
                    for i in 0..<totalArr.count {
                        values = values + totalArr[i]
                    }
                    total = values
                    //self.totalPriceLbl.text = "\(total)"
                }
                
            }
            
            //Added:-
            //self.productCollectionview.reloadData()
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
    //MARK:- Fetch(details based on productID and CategoryID) from Coredata
    
    mutating func fetchProductDetails(prdtId :String,cateId :String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Products")
        if !prdtId.isEmpty {
            fetchRequest.predicate = NSPredicate(format: "productId = %@ && categoryID = %@", prdtId ,cateId )
        }
        do {
            indexList = try managedContext.fetch(fetchRequest)
            print(indexList)
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    
    //MARK:- Update existing product details to Coredata
    mutating func updateProductDetails(prdtId :String,cateId :String,qty:String, priceQty:String)  {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
        
        fetchRequest.predicate = NSPredicate(format: "productId = %@ && categoryId = %@", prdtId ,cateId )
        
        do {
            
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            if results?.count != 0 {
                results![0].setValue(qty, forKeyPath: "qty")
                results![0].setValue(priceQty, forKeyPath: "price")
                //self.productCollectionview.reloadData()
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
        do {
            try context.save()
            fetchProductListResult()
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
    }
    
}
