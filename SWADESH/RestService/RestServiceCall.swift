//
//  RestServiceCall.swift
//  SWADESH
//
//  Created by Vikas on 21/01/21.
//

import UIKit
import Alamofire

struct RestService {

    public static func serviceCall(url:String, method: HTTPMethod, parameter:Parameters?, success: @escaping(Data)->Void, failure: @escaping(Error)->Void) {
        
        let header:HTTPHeaders = [API.key:API.value,API.content:API.type]
        
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON { (responseData) in
                                                            
            switch responseData.result{
            case .success(_):
                success(responseData.data!)
            case .failure(let error):
                failure(error)
            }
        }

    }

}
