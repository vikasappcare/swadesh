//
//  AppDelegate.swift
//  SWADESH
//
//  Created by Vikas on 05/01/21.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKCoreKit
import FirebaseCore
import Firebase
import FirebaseMessaging
import UserNotifications
import FirebaseInstanceID


@main
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Thread.sleep(forTimeInterval: 2)
        //NAVIGATIOn
        appearence()
        //Keyboard
        IQKeyboardManager.shared.enable = true
        
        /* Facebook-Login */
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions:
            launchOptions
        )
        /* Google Login */
        GIDSignIn.sharedInstance().clientID = "828024296140-6plr17o951ude0fs57rsf59h3q7alo8s.apps.googleusercontent.com"
                
        // Notifications
        
//        FirebaseApp.configure()
//        Messaging.messaging()
//        
//        
//        registerAppForNotifications()
//        UNUserNotificationCenter.current().delegate = self
//        Messaging.messaging().delegate = self

        //let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        // Override point for customization after application launch.
        return true
    }

    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
      return GIDSignIn.sharedInstance().handle(url)
    }
    
    //MARK:- Custom Nav functions
    func appearence() {
        //Making navigation bar to transaparent withpout color
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = .clear
        UINavigationBar.appearance().isTranslucent = true
        
        //setting back button to custom image
        let yourBackImage = UIImage(named: "Back")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = yourBackImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = yourBackImage
    }

    // MARK: -  FIRFireBase Messaging Delegate
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        // If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        #if DEBUG
        
        print("FCM TOKEN ====> \(fcmToken)")
        #endif
        
        // UserDefaults.standard.setValue(fcmToken, forKey: FCM_TOKEN)
//        UserDefaults.standard.set(fcmToken, forKey: FCM_TOKEN)
//        UserDefaults.standard.synchronize()
    }
    
    
    // MARK: - NOTIFICATION DELEGATES(For APNS)
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let strDeviceToken = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        Messaging.messaging().apnsToken = deviceToken
        
        #if DEBUG
        print("DEVICE TOKEN ====> \(strDeviceToken)\n")
        #endif
        
        // UserDefaults.standard.setValue(strDeviceToken, forKey: "device_token")
        // UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        #if DEBUG
        print("\(error)")
        #endif
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "SWADESH")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    //MARK: -  Register / UnRegister User Notifications
    
    func registerAppForNotifications() {
        
        //Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func UnRegisterAppForNotifications() {
        
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    //MARK: -  UNUserNotificationCenter Delegate
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        let str = "\(userInfo)"
        /*
         let alertController = UIAlertController(title: "News Tested", message: str, preferredStyle: .actionSheet)
         let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
         UIAlertAction in
         NSLog("OK Pressed")
         
         }
         let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
         UIAlertAction in
         NSLog("Cancel Pressed")
         }
         alertController.addAction(okAction)
         alertController.addAction(cancelAction)
         self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
         */
        print("\(str)")
    }
    
    // called when a notification is delivered when app is in foreground.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        _ = center
        _ = notification
        //completionHandler([.badge, .alert, .sound])
        
        print("Notifications Will Present=====>:\(notification)")
        
        
        let userInfo = notification.request.content.userInfo
        //  let identity = notification.request.identifier
        print("User Info : \(userInfo)")
        //completionHandler([.sound, .alert, .badge])
        
        let text = userInfo["text"] as? String ?? ""
        print(text)
        
        if userInfo["type"] as? String == "login" {
            
            completionHandler([])
            //  UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [identity])
            //  print("Removed Pending Notifications")
            
        }else if userInfo["type"] as? String == "logout" {
            
            completionHandler([])
            //  UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [identity])
            // print("Removed Pending Notifications")
            
        }else {
            
            completionHandler([.badge, .alert, .sound])
        }
    }
    
    // called to let your app know which action was selected by the user for a given notification.
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        _ = center
        _ = response
        completionHandler()
        
        let userInfo = response.notification.request.content.userInfo
        print("User Info : \(userInfo)")
        let text = userInfo["text"] as? String ?? ""
        let title = userInfo["title"] as? String ?? ""
        let type = userInfo["type"] as? String ?? ""
        
        print("Did Receive Notifications Response===> \(response)")
        print("Text:\(text)")
        print("Title:\(title)")
        print("Type:\(type)")
        
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification notification: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        #if DEBUG
        print("Received push notification: \(notification), identifier: \(identifier ?? "")")
        // iOS 8
        #endif
        completionHandler()
    }
    
}
extension AppDelegate {
    
    func willPresentUserSessionExpired() {
        
        let defaults = UserDefaults.standard
        
//        defaults.set(false, forKey: LOGGED_IN)
//        defaults.synchronize()
        
        resetUserDefaults()
        
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//        appDelegate?.window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
    }
    
    
    func didReceiveUserSessionExpired() {
        
        let defaults = UserDefaults.standard
        
//        defaults.set(false, forKey: LOGGED_IN)
//        defaults.synchronize()
        
        resetUserDefaults()
    }
    
    func resetUserDefaults() {
        
        
        print("BEGIN: ======> userDefaults successfully removed from the app")
        
        
        DispatchQueue.main.async {
            
            let instance = InstanceID.instanceID()
            instance.deleteID { (error) in
                
                if error != nil {
                    
                    let errorMessage = error?.localizedDescription ?? ""
                    print("Instance ID Error : \(errorMessage)")
                    
                } else {
                    
                    print("FCM TOKEN Deleted Successfully");
                }
            }
        }
        
        let userDefaults = UserDefaults.standard
        let dict = userDefaults.dictionaryRepresentation()
        
        dict.keys.forEach { key in
            
            userDefaults.removeObject(forKey: key)
        }
        
        userDefaults .synchronize()
        
    }
}
